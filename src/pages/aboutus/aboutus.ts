import { Settings } from './../../providers/settings/settings';
import { User } from './../../providers/user/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController, AlertController } from 'ionic-angular';
import { GiftPage } from '../gift/gift';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';

/**
 * Generated class for the AboutusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-aboutus',
  templateUrl: 'aboutus.html',
})
export class AboutusPage {
  public username="";
  public countcart:any=0;
  public loggeduser='';
  public currency:any={};
  public currencies:any=[];
  public currency_id=1;
  constructor(public homeprovider:HomeproviderProvider, public navCtrl: NavController, public navParams: NavParams,public user:User,public event:Events,public settings:Settings,public modalCtrl: ModalController ,public alertCtrl:AlertController ) {
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutusPage');
    this.user.loggedinuser.subscribe((data)=>{
      console.log('data login',data); 
      if(data && data.length>0){ 
        var user=data[0]; 
        this.loggeduser=user;
      } else{
        this.loggeduser='';
      }
    })
    this.user.cart.subscribe((data)=>{
      if(data && data.length>0){
        this.countcart=data.length;
      } else{
        this.countcart=0;
      }
    })
  }
  ionViewWillEnter(){ 
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
  }
  Search($event){
    var val="";
    if($event.target) 
      val = $event.target.value; 
    this.navCtrl.push('products', {'src':'search','value':val});
    // if(this.user._user)
    // { 
    //   var val="";
    //     if($event.target) 
    //       val = $event.target.value; 
    //   this.navCtrl.push('products', {'src':'search','value':val});
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  GotoCart(){ 
    this.event.publish('selecttab:menu', 3); 
    // if(this.user._user)
    // { 
    //   this.event.publish('selecttab:menu', 3);  
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  OpenModal(){
    let giftCardModal = this.modalCtrl.create(GiftPage);
    giftCardModal.present();
    // if(this.user._user)
    // { 
    //   let giftCardModal = this.modalCtrl.create(GiftPage);
    //   giftCardModal.present();
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  
  setCurrency(){
    this.homeprovider.currency = this.currencies.find(currency=>currency.id==this.currency_id);
    this.currency = this.homeprovider.currency;
  }
}
