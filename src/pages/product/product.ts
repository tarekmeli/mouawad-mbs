import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events, LoadingController, ModalController, AlertController } from 'ionic-angular';
import { Api, Settings, User } from '../../providers';
import { GiftPage } from '../gift/gift';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';

/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name:'products'
})
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {
  public products:any=[];
  public productorginale:any=[];
  public loading:any;
  public countcart:any=0;
  public username="";
  public selectedcat="";
  public pagesrc="";
  public currency:any={};
  public currencies:any=[];
  public currency_id=1;
  public categories:Array<any>=[{"categoryName":"All categories","newcategoryId":0}];
  constructor(public homeprovider:HomeproviderProvider,public navCtrl: NavController, public navParams: NavParams,public api:Api,public event:Events,public loadingCtrl:LoadingController,public toastCtrl:ToastController,public modalCtrl: ModalController,public settings:Settings,public user:User,public alertCtrl:AlertController) { 
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
                       
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    }); 
    this.loading.present();
    this.pagesrc=this.navParams.get('src');
    if(this.navParams.get('src')=='promotions'){ 
      this.api.get('getAllPromotions').then((res)=>{
        console.log('Search',res)
        if(res["success"])
        {
          if(this.loading!=""){
            this.loading.dismiss();
          } 
          if(res["products"].length>0){
            this.products=res["products"].sort(function(a, b){return a.productOrder - b.productOrder})
          }
          else
          {
            let toast = this.toastCtrl.create({
              message:  "No products matching you search were found", 
              position: 'middle',
              showCloseButton:true,
              closeButtonText:'Close'
            });
            toast.present();
          }
        } 
        else{
          if(this.loading!=""){
            this.loading.dismiss();
          } 
           let toast = this.toastCtrl.create({
            message:  "No products matching you search were found", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
        }
      }).catch((er)=>{
        if(this.loading!=""){
          this.loading.dismiss();
        } 
         let toast = this.toastCtrl.create({
          message:  "Data not retrieved.Please try again later.", 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close'
        });
        toast.present();
      })
    }
    if(this.navParams.get('src')=='bestsellers'){ 
      this.api.get('getbestsellers').then((res)=>{
        console.log('Search',res)
        if(res["success"])
        {
          if(this.loading!=""){
            this.loading.dismiss();
          } 
          if(res["products"].length>0){
            this.products=res["products"].sort(function(a, b){return a.productOrder - b.productOrder})
          }
          else
          {
            let toast = this.toastCtrl.create({
              message:  "No products matching you search were found", 
              position: 'middle',
              showCloseButton:true,
              closeButtonText:'Close'
            });
            toast.present();
          }
        } 
        else{
          if(this.loading!=""){
            this.loading.dismiss();
          } 
           let toast = this.toastCtrl.create({
            message:  "No products matching you search were found", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
        }
      }).catch((er)=>{
        if(this.loading!=""){
          this.loading.dismiss();
        } 
         let toast = this.toastCtrl.create({
          message:  "Data not retrieved.Please try again later.", 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close'
        });
        toast.present();
      })
    }
    if(this.navParams.get('src')=='newarrival'){ 
      this.api.get('getProductsbynewarrival').then((res)=>{
        console.log('Search',res)
        if(res["success"])
        {
          if(this.loading!=""){
            this.loading.dismiss();
          } 
          if(res["products"].length>0){
            this.products=res["products"].sort(function(a, b){return a.productOrder - b.productOrder})
          }
          else
          {
            let toast = this.toastCtrl.create({
              message:  "No products matching you search were found", 
              position: 'middle',
              showCloseButton:true,
              closeButtonText:'Close'
            });
            toast.present();
          }
        } 
        else{
          if(this.loading!=""){
            this.loading.dismiss();
          } 
           let toast = this.toastCtrl.create({
            message:  "No products matching you search were found", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
        }
      }).catch((er)=>{
        if(this.loading!=""){
          this.loading.dismiss();
        } 
         let toast = this.toastCtrl.create({
          message:  "Data not retrieved.Please try again later.", 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close'
        });
        toast.present();
      })
    }
    if(this.navParams.get('src')=='brand'){ 
      this.api.get('searchProductsbybrandid/'+this.navParams.get('brandid')).then((res)=>{
        console.log('Search',res)
        if(res["success"])
        {
          if(this.loading!=""){
            this.loading.dismiss();
          } 
          console.log('product',res["products"])
          if(res["products"].length>0){
            var resproducts=res["products"].sort(function(a, b){return a.productOrder - b.productOrder});
            //--Group Product and categories
            for(var i=0;i<resproducts.length;i++){
              const found = this.products.some(el => el.productId === resproducts[i]["productId"]);
              if (!found){
                resproducts[i]["categoryarray"]=[];
                resproducts[i]["categoryarray"].push({"categoryName":resproducts[i]["categoryName"],"newcategoryId":resproducts[i]["newcategoryId"]});
                this.products.push(resproducts[i]);
                //--check categories
                const categoryfound=this.categories.some(el => el.newcategoryId === resproducts[i]["newcategoryId"]);
                if(!categoryfound)
                  this.categories.push({"categoryName":resproducts[i]["categoryName"],"newcategoryId":resproducts[i]["newcategoryId"]});
                
              } 
              else {
                var elementPos = this.products.map(function(x) {return x.productId; }).indexOf(resproducts[i]["productId"]);
                // console.log('elementPos',elementPos)
                this.products[elementPos]["categoryarray"].push({"categoryName":resproducts[i]["categoryName"],"newcategoryId":resproducts[i]["newcategoryId"]});
                //--check categories
                const categoryfound=this.categories.some(el => el.newcategoryId === resproducts[i]["newcategoryId"]);
                if(!categoryfound)
                  this.categories.push({"categoryName":resproducts[i]["categoryName"],"newcategoryId":resproducts[i]["newcategoryId"]});
              }
            }
            this.productorginale=this.products; 
            console.log('products',this.products)
          }
          else
          {
            let toast = this.toastCtrl.create({
              message:  "No products matching you search were found", 
              position: 'middle',
              showCloseButton:true,
              closeButtonText:'Close'
            });
            toast.present();
          }
        } 
        else{
          if(this.loading!=""){
            this.loading.dismiss();
          } 
           let toast = this.toastCtrl.create({
            message:  "No products matching you search were found", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
        }
      }).catch((er)=>{
        if(this.loading!=""){
          this.loading.dismiss();
        } 
         let toast = this.toastCtrl.create({
          message:  "Data not retrieved.Please try again later.", 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close'
        });
        toast.present();
      })
  }//--end if
    if(this.navParams.get('src')=='category'){ 
      this.api.get('searchproductsbycategoyid/'+this.navParams.get('categoryid')).then((res)=>{
        console.log('Search',res)
        if(res["success"])
        {
          if(this.loading!=""){
            this.loading.dismiss();
          } 
          if(res["products"].length>0){
            this.products=res["products"].sort(function(a, b){return a.productOrder - b.productOrder})
          }
          else
          {
            let toast = this.toastCtrl.create({
              message:  "No products matching you search were found", 
              position: 'middle',
              showCloseButton:true,
              closeButtonText:'Close'
            });
            toast.present();
          }
        } 
        else{
          if(this.loading!=""){
            this.loading.dismiss();
          } 
           let toast = this.toastCtrl.create({
            message:  "No products matching you search were found", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
        }
      }).catch((er)=>{
        if(this.loading!=""){
          this.loading.dismiss();
        } 
         let toast = this.toastCtrl.create({
          message:  "Data not retrieved.Please try again later.", 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close'
        });
        toast.present();
      })
  }//--end if
    if(this.navParams.get('src')=='search'){ 
        this.api.get('searchproducts/'+this.navParams.get('value')).then((res)=>{
          console.log('Search',res)
          if(res["success"])
          {
            if(this.loading!=""){
              this.loading.dismiss();
            } 
            if(res["products"].length>0){
              this.products=res["products"].sort(function(a, b){return a.productOrder - b.productOrder})
            }
            else
            {
              let toast = this.toastCtrl.create({
                message:  "No products matching you search were found", 
                position: 'middle',
                showCloseButton:true,
                closeButtonText:'Close'
              });
              toast.present();
            }
          } 
          else{
            if(this.loading!=""){
              this.loading.dismiss();
            } 
             let toast = this.toastCtrl.create({
              message:  "No products matching you search were found", 
              position: 'middle',
              showCloseButton:true,
              closeButtonText:'Close'
            });
            toast.present();
          }
        }).catch((er)=>{
          if(this.loading!=""){
            this.loading.dismiss();
          } 
           let toast = this.toastCtrl.create({
            message:  "Data not retrieved.Please try again later.", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
        })
    }//--end if
  }

  ionViewWillEnter(){ 
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductPage');
  }
  GotoProductDetials(productid){
    this.navCtrl.push('productsdetials', {'productid':productid});
  }
  ionViewDidLeave(){
    console.log('ionViewDidLeave')
    if(this.loading!=""){
      this.loading.dismiss();
    }
  } 
  ionViewDidEnter(){
    console.log('ionViewDidEnter product')
    this.user.cart.subscribe((data)=>{
      console.log('data',data)
      if(data && data.length>0){
        this.countcart=data.length;
      } else{
        this.countcart=0;
      }
    })
  }
  GotoBrands(){
    this.navCtrl.push('brands');
  }
  GotoProductnew(){
    this.navCtrl.push('products', {'src':'newarrival'});
  }
  GotoProductpro(){
    this.navCtrl.push('products', {'src':'promotions'});
  }
  GotoProductbest(){
    this.navCtrl.push('products', {'src':'bestsellers'});
  }
  OpenModal(){
    let giftCardModal = this.modalCtrl.create(GiftPage);
    giftCardModal.present();
  }
  RemoveWishList(product){
    let alert = this.alertCtrl.create({ 
      subTitle: 'Remove product from wishlist?',
      buttons: [
       
        {
          text: 'Remove',
          handler: () => {
            console.log('Remove clicked',product,product["wishlistid"]);
            this.api.delete('deletewishlist/'+product["wishlistid"]).subscribe((res)=>{
              console.log('deletewishlist',res)
              product["inwishlist"]=false;
            })
          }
        },
         {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }
  AddToWishList(product){
    console.log(product,product["productId"])
    if(this.user._user){
      this.api.postpromise("AddWish",{
        "productId":product["productId"],   
        "userId":this.user._user["userId"]
      }).then((res)=>{
        console.log(res)
        product["inwishlist"]=true;
        product["wishlistid"]=res["wish"].id;
        let toast = this.toastCtrl.create({
          message:  res["message"], 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close',
          duration: 1500
        });
        toast.present(); 

      })
    }else{
      let alert = this.alertCtrl.create({ 
        subTitle: 'Please login first',
        buttons: [{
          text: 'Go to Login',
          handler: () => { 
            this.navCtrl.parent.select(2);
          }
        }]
      });
      alert.present();
    }
  }
  GotoCart(){ 
    this.event.publish('selecttab:menu', 3);  
  }
  Search($event){
    var val="";
      if($event.target) 
        val = $event.target.value; 
    this.navCtrl.push('products', {'src':'search','value':val});
  }
  
  SearchType(type){ 
    console.log('SearchType',type)
    var filteredproducts=[];
    if(type==0){
      this.products=[];
      this.products=this.productorginale;
    }
    else{
      filteredproducts=[];
      
      for(var p=0;p<this.productorginale.length;p++){
        for(var c=0;c<this.productorginale[p]["categoryarray"].length;c++){
          if(this.productorginale[p]["categoryarray"][c]["newcategoryId"]==type){
            filteredproducts.push(this.productorginale[p]);
          }
        }
      }
      console.log('filtered',filteredproducts)
      this.products=[];
      this.products=filteredproducts;
    }
  }
  setCurrency(){
    this.homeprovider.currency = this.currencies.find(currency=>currency.id==this.currency_id);
    this.currency = this.homeprovider.currency;
  }
}
