import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, ToastController, ModalController } from 'ionic-angular';
import { Api, Settings, User } from '../../providers';
import { GiftPage } from '../gift/gift';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';

/**
 * Generated class for the BrandsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name:'brands'
})
@Component({
  selector: 'page-brands',
  templateUrl: 'brands.html',
})
export class BrandsPage {
  public brands:any=[];
  public loading:any;
  public countcart:any=0;
  public username="";
  public currency:any={};
  public currencies:any=[];
  public currency_id=1;
  constructor(public homeprovider:HomeproviderProvider,public navCtrl: NavController, public navParams: NavParams,public api:Api,public event:Events,public loadingCtrl:LoadingController,public toastCtrl:ToastController,public modalCtrl: ModalController,public settings:Settings,public user:User) { 
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    }); 
    this.loading.present(); 
    this.api.get("brands").then((res)=>{
      console.log('brands',res)
      if(res["success"])
      {
        if(this.loading!=""){
          this.loading.dismiss();
        } 
        this.brands=res["brands"].sort(function(a, b){return a.brandOrder - b.brandOrder})
      } 
      else{
        if(this.loading!=""){
          this.loading.dismiss();
        } 
         let toast = this.toastCtrl.create({
          message:  "Data not retrieved.Please try again later.", 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close'
        });
        toast.present();
      }
    }).catch((er)=>{
      if(this.loading!=""){
        this.loading.dismiss();
      } 
       let toast = this.toastCtrl.create({
        message:  "Data not retrieved.Please try again later.", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
    })
      //--Get Cart
      this.settings.getcart().then((res)=>{
        console.log('GetCart',res)
        if(res && res.length>0)
          this.countcart=res.length;
      })
      if(this.user._user){
        this.username=this.user._user["fullName"]
      }
      this.event.unsubscribe('CartEdit');
      this.event.subscribe('CartEdit',() => {
          //--Get Cart
        this.settings.getcart().then((res)=>{
          console.log('GetCart',res)
          if(res && res.length>0)
            this.countcart=res.length;
        })
      });
  }
  ionViewWillEnter(){  
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
  
    this.event.unsubscribe('CartEdit');
    this.event.subscribe('CartEdit',() => {
      console.log('CartEdit Home') 
      //--Get Cart
      this.settings.getcart().then((res)=>{
        console.log('GetCart',res)
        if(res && res.length>0)
          this.countcart=res.length;
      })
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SubcatagoriesPage');
  }
  ionViewDidLeave(){
    console.log('ionViewDidLeave')
    if(this.loading!=""){
      this.loading.dismiss();
    }
  }  
  GotoProduct(brandid){
    this.navCtrl.push('products', {'src':'brand','brandid':brandid});
  }
  GotoBrands(){
    this.navCtrl.push('brands');
  }
  GotoProductnew(){
    this.navCtrl.push('products', {'src':'newarrival'});
  }
  GotoProductpro(){
    this.navCtrl.push('products', {'src':'promotions'});
  }
  GotoProductbest(){
    this.navCtrl.push('products', {'src':'bestsellers'});
  }
  OpenModal(){
    let giftCardModal = this.modalCtrl.create(GiftPage);
    giftCardModal.present();
  }
  GotoCart(){ 
    this.event.publish('selecttab:menu', 3);  
  }
  Search($event){
    var val="";
      if($event.target) 
        val = $event.target.value; 
    this.navCtrl.push('products', {'src':'search','value':val});
  }
  
  setCurrency(){
    this.homeprovider.currency = this.currencies.find(currency=>currency.id==this.currency_id);
    this.currency = this.homeprovider.currency;
  }
}
