import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrandsPage } from './brands';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    BrandsPage,
  ],
  imports: [
    IonicPageModule.forChild(BrandsPage),
    PipesModule
  ],
})
export class BrandsPageModule {}
