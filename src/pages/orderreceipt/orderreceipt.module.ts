import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderreceiptPage } from './orderreceipt';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    OrderreceiptPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderreceiptPage),
    PipesModule

  ],
})
export class OrderreceiptPageModule {}
