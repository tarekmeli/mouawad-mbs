import { Api } from './../../providers/api/api';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Platform, AlertController } from 'ionic-angular';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';

/**
 * Generated class for the OrderreceiptPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name:'orderreceipt'
})
@Component({
  selector: 'page-orderreceipt',
  templateUrl: 'orderreceipt.html',
})
export class OrderreceiptPage {
  public orders:any=[];
  public products:any=[];
  public colors:any=[];
  public sizes:any=[];
  public discount:boolean=false;
  public purchase:any='';
  public currency:any={};
  public deliveryCharge=2;
  public currencies:any=[]; 
  public loyaltyProgramPoints:any={};
  constructor(public homeprovider:HomeproviderProvider,public platform:Platform,public navCtrl: NavController, public navParams: NavParams,public api:Api,public event:Events,public alertCtrl:AlertController) {
    this.currency = this.homeprovider.currency;
    this.currencies = this.homeprovider.currencies;
    this.deliveryCharge = this.homeprovider.deliveryCharge;

    this.loyaltyProgramPoints = this.homeprovider.loyaltyProgramPoints;
    
    console.log('Purchase Id',this.navParams.get('purchaseid'),this.navParams.get('code'))
    if(this.navParams.get('code') && this.navParams.get('code')>0){
      this.discount=true;
    }else{
      this.discount=false;
    }
    this.purchase=this.navParams.get('purchase');
    
    console.log('this.purchase',this.purchase) 
  }
  ionViewWillEnter(){  
    this.currency = this.homeprovider.currency;  
    this.homeprovider.currentPage = 'other'; 
  }

  ionViewDidLoad() {
    this.currency = this.homeprovider.currency;
    this.currencies = this.homeprovider.currencies;
    this.deliveryCharge = this.homeprovider.deliveryCharge;
   
    console.log('ionViewDidLoad OrderreceiptPage');
    this.api.get("GetOrdersP/"+this.navParams.get('purchaseid')).then((res)=>{
      console.log('Purchase orders',res)
      this.currency = res['currency']=='USD'? this.currencies[0]: this.currencies[1]; 
      this.orders=res["orders"];
      for(var p=0;p<res["arrayproduct"].length;p++){ 
        this.products.push(res["arrayproduct"][p][0])
      } 
      this.colors=res["arraycolor"];
      this.sizes=res["arraysize"];
      this.discount= (res['redeemuserId'] && res['redeemuserId']>0)? true: this.discount;
      this.CaluclateTotal();
      // console.log('history',this.navParams.get('history'))
      if(!this.navParams.get('history')){
        //--Update label
        this.api.getspecial("https://mouawadmbs.com/receiptapp?purchaseId="+this.navParams.get('purchaseid')).catch((er)=>{console.log(er)});
        this.api.getspecial("https://mouawadmbs.com/label?purchaseId="+this.navParams.get('purchaseid')).catch((er)=>{console.log(er)});
      }}).catch((er)=>{
      console.log('Error',er)
    })
    
  }
  public totalLL:any=0;
  public totaldollars:any=0;
  public username="";
  public totalinital=0;
  CaluclateTotal(){
    console.log('CaluclateTotal')
    this.totalLL=0;
    this.totalinital=0;
    this.totaldollars=0;
    for(var c=0;c<this.orders.length;c++){ 
      this.totalinital=this.totalinital+this.orders[c]['itemPrice'] * this.orders[c]["quantity"];
    }
    console.log('CaluclateTotal',this.totalinital) 
    var tax=0;
    if(this.totalinital<100){ 
      if(this.purchase && this.purchase['pending_decision']!=""){
        tax= parseInt(this.purchase['shipping_tax']);
        console.log(tax,typeof tax)
        this.totaldollars = (this.totalinital+ tax).toFixed(2);
      } else {
        this.totaldollars=(this.totalinital+this.deliveryCharge).toFixed(2);
      }
    }
    else{
      if(this.purchase){
        tax= parseInt(this.purchase['shipping_tax']);
        this.totaldollars = (this.totalinital+  tax).toFixed(2);
      } else {
        this.totaldollars=(this.totalinital).toFixed(2);
      }
    }
    if((this.navParams.get('code') && this.navParams.get('code')>0) || this.discount){
      this.totaldollars=(this.totaldollars - this.loyaltyProgramPoints.amount).toFixed(2);
    }
   
  }
  GoToHome(){
    this.event.publish('selecttab:menu', 0)
  }
  public showbutton=true;
  public hidback=false;
  ionViewDidEnter(){
    let nav = this.navCtrl.getPrevious();
    if(nav.id=="MyordersPage"){
      this.showbutton =false;
      // this.hidback=false;
    } else{
      this.showbutton = true;
      // this.hidback=true;
      //this.platform.registerBackButtonAction(() => { 
        // this.event.publish('selecttab:menu', 0);
      //})
    }
   
  }
  ionViewWillLeave(){ 
    this.homeprovider.currentPage = 'orderreceipt'; 
    let nav = this.navCtrl.getPrevious();
    if(nav.id!="MyordersPage"){
      this.event.publish('selecttab:menu', 3);
    }
  }
  PayNow(){
    let alert = this.alertCtrl.create({
      title: 'Pay',
      subTitle: 'Please check your email for payment link',
      buttons: ['Dismiss']
    });
    alert.present();
    // var x=this.totalinital.toFixed(2)
    // this.navCtrl.push("shipping",{"total":this.totaldollars,'totalintial':x,purchase:this.purchase})
  }
}
