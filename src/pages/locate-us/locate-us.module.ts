import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocateUsPage } from './locate-us';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    LocateUsPage,
  ],
  imports: [
    IonicPageModule.forChild(LocateUsPage),
    PipesModule
  ],
})
export class LocateUsPageModule {}
