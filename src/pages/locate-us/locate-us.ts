import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events, AlertController } from 'ionic-angular';
import { User, Settings } from '../../providers';
import { GiftPage } from '../gift/gift';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';

/**
 * Generated class for the LocateUsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@IonicPage()
@Component({
  selector: 'page-locate-us',
  templateUrl: 'locate-us.html',
})
export class LocateUsPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  public countcart:any=0;
  public username="";
  public currency:any={};
  public currencies:any=[];
  public currency_id=1;
  constructor(public homeprovider:HomeproviderProvider,public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public settings:Settings,public user:User,public event:Events,public alertCtrl:AlertController) {
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
    //--Get Cart
     this.settings.getcart().then((res)=>{
      console.log('GetCart',res)
      if(res && res.length>0)
        this.countcart=res.length;
    })
    if(this.user._user){
      this.username=this.user._user["fullName"]
    }
    this.event.unsubscribe('CartEdit');
    this.event.subscribe('CartEdit',() => {
        //--Get Cart
      this.settings.getcart().then((res)=>{
        console.log('GetCart',res)
        if(res && res.length>0)
          this.countcart=res.length;
      })
    });
  }
  ionViewWillLeave(){ 
    this.homeprovider.currentPage = 'home'; 
    }
    
  ionViewWillEnter(){ 
    this.homeprovider.currentPage = 'other';
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
    
    this.event.unsubscribe('CartEdit');
    this.event.subscribe('CartEdit',() => {
      console.log('CartEdit Home') 
      //--Get Cart
      this.settings.getcart().then((res)=>{
        console.log('GetCart',res)
        if(res && res.length>0)
          this.countcart=res.length;
      })
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LocateUsPage');
    this.loadMap();
  }
  loadMap(){
    let latLng = new google.maps.LatLng(33.906111, 35.576909);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker();

  }
  GotoCart(){ 
    this.event.publish('selecttab:menu', 3);  
  }
  Search($event){
    var val="";
    if($event.target) 
      val = $event.target.value; 
    this.navCtrl.push('products', {'src':'search','value':val});
    // if(this.user._user){ 
    //   var val="";
    //     if($event.target) 
    //       val = $event.target.value; 
    //   this.navCtrl.push('products', {'src':'search','value':val});
    // }
    // else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  addMarker(){

    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position:  new google.maps.LatLng(33.906111, 35.576909)
    });
  
    let content = "<h4>Mouawad Library Jal Ed Dib!</h4>";          
  
    this.addInfoWindow(marker, content);
  
  }
  addInfoWindow(marker, content){

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });

  }
  OpenModal(){
    let giftCardModal = this.modalCtrl.create(GiftPage);
    giftCardModal.present();
    // if(this.user._user){ 
    //   let giftCardModal = this.modalCtrl.create(GiftPage);
    //   giftCardModal.present();
    // }
    // else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  setCurrency(){
    this.homeprovider.currency = this.currencies.find(currency=>currency.id==this.currency_id);
    this.currency = this.homeprovider.currency;
  }

}
