 
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
/**
 * Generated class for the JoinourteamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-joinourteam',
  templateUrl: 'joinourteam.html',
})
export class JoinourteamPage { 
  public cv:any="";
  
  constructor(public navCtrl: NavController, public navParams: NavParams,public platform:Platform,public transfer: FileTransfer) {
  }
  public _READER : any  			=	new FileReader();
  ionViewDidLoad() {
    console.log('ionViewDidLoad JoinourteamPage');
  }
  selectFileToUpload(event) 
  {
    console.log(event)
    let file 		: any 		= event.target.files[0];
    console.log(file)
    this._READER.readAsDataURL(file);
    this._READER.onloadend = () =>
    {
      console.log(this._READER.result)
      let fileTransfer: FileTransferObject = this.transfer.create();
      let options: FileUploadOptions = {
        fileKey: 'file',
        fileName: 'cv.png',
        headers: {} 
     }
   
     fileTransfer.upload(this._READER.result, 'https://mouawadmbs.com/betaMobileApi/cvupload.php', options)
      .then((data) => {
        // success
        alert('succes'+data)
      }, (err) => {
        // error
        alert('err'+err)
      })
    
    } 
  } 
  SaveApplicant(){
    console.log(this.cv)
     
    
  }

}
