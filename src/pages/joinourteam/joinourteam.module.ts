import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JoinourteamPage } from './joinourteam';

@NgModule({
  declarations: [
    JoinourteamPage,
  ],
  imports: [
    IonicPageModule.forChild(JoinourteamPage),
  ],
})
export class JoinourteamPageModule {}
