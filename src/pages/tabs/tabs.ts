import { Component, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, Events, Tabs, MenuController } from 'ionic-angular';

import { Tab1Root, Tab2Root, Tab3Root,Tab4Root } from '../';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root: any = Tab1Root;
  tab2Root: any = Tab2Root;
  tab3Root: any = Tab3Root;
  tab4Root: any = Tab4Root; 
  @ViewChild('myTabs') tabRef: Tabs;
  constructor(public navCtrl: NavController, public translateService: TranslateService,public event:Events,public menu: MenuController) {
    this.event.unsubscribe('selecttab:menu');
    this.event.subscribe('selecttab:menu',(index,selectlatest) => { 
      console.log('selecttab',index,selectlatest)
      if(selectlatest)
      {
        this.SelectTabSpecial(index)
      }
      else {
        this.SelectTab(index)
      }
     
    });
   
  }
  SelectTab(index){
    console.log('SelectTab',index)
    this.tabRef.select(index);
  }
  SelectTabSpecial(index){
    this.tabRef.select(index).then(()=>{
      this.event.publish('selectlatest')
    })    
  }
  OpenMenu(){
    console.log('OpenMenu')
    this.menu.open();
  }
}
