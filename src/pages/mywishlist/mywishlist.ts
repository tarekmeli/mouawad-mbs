import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { User, Api, Settings } from '../../providers';

/**
 * Generated class for the MywishlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mywishlist',
  templateUrl: 'mywishlist.html',
})
export class MywishlistPage {
  public wishes:any=[];
  public username:any="";
  public countcart:any=0;
  public loggeduser:any;
  public bestsellers:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public settings:Settings,public user:User,public api:Api,public homeprovider:HomeproviderProvider,public alertCtrl:AlertController) {
    
    if(this.homeprovider.bestsellers){  
      this.bestsellers=this.homeprovider.bestsellers
    }
    
  }
  
  ionViewWillEnter(){   
    this.homeprovider.currentPage = 'other';
  }

ionViewWillLeave(){ 
    this.homeprovider.currentPage = 'home'; 
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MywishlistPage');
    this.user.loggedinuser.subscribe((data)=>{
      console.log('data login',data); 
      if(data && data.length>0){ 
        var user=data[0]; 
        this.loggeduser=user;
        
        this.GetWishes(user["userId"]);
      } else{
        this.loggeduser='';
      }
    })
    this.user.cart.subscribe((data)=>{
      if(data && data.length>0){
        this.countcart=data.length;
      } else{
        this.countcart=0;
      }
    })
  }
  GetWishes(userID){
    this.api.get("GetUserWish/"+userID).then((res)=>{
      this.wishes=[];
      console.log('bestsellers',this.bestsellers)
      console.log('Wish',res)
      if(res["wishlist"]){
        //loop through bestsellers
        for(var i=0;i<this.bestsellers.length;i++){
          for(var j=0;j<res["wishlist"].length;j++){
            console.log(this.bestsellers[i]["productId"]==res["wishlist"][j]["productId"])
            if(this.bestsellers[i]["productId"]==res["wishlist"][j]["productId"]){ 
              this.bestsellers[i]["wishlistid"]=res["wishlist"][j]["wishlistId"]
              this.wishes.push(this.bestsellers[i])  
            }
          }
        }
        console.log('wishes',this.wishes)
      }
    })
  }
  RemoveFromWishList(product){
    console.log(product)
    let alert = this.alertCtrl.create({ 
      subTitle: 'Remove product from wishlist?',
      buttons: [
       
        {
          text: 'Remove',
          handler: () => {
            console.log('Remove clicked',product,product["wishlistid"]);
            this.api.delete('deletewishlist/'+product["wishlistid"]).subscribe((res)=>{
              console.log('deletewishlist',res)
              this.refreshwishlist();
              //product["inwishlist"]=false;
            })
          }
        },
         {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }
  refreshwishlist() {
    
    //--Get Wish list
    if(this.loggeduser)
    { 

      this.api.get("GetUserWish/"+this.loggeduser["userId"]).then((res)=>{
        this.wishes=[];
        console.log('bestsellers',this.bestsellers)
        console.log('Wish',res)
        if(res["wishlist"]){
          //loop through bestsellers
          for(var i=0;i<this.bestsellers.length;i++){
            for(var j=0;j<res["wishlist"].length;j++){
              console.log(this.bestsellers[i]["productId"]==res["wishlist"][j]["productId"])
              if(this.bestsellers[i]["productId"]==res["wishlist"][j]["productId"]){ 
                this.bestsellers[i]["wishlistid"]=res["wishlist"][j]["wishlistId"]
                this.wishes.push(this.bestsellers[i])  
              }
            }
          }
          console.log('wishes',this.wishes)
        }
      })
    }  
    
  }
  GotoProductDetials(productid){
    this.navCtrl.push('productsdetials', {'productid':productid});
    // if(this.loggeduser)
    // { 
    //   this.navCtrl.push('productsdetials', {'productid':productid});
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }

}
