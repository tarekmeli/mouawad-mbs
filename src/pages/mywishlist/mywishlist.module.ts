import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MywishlistPage } from './mywishlist';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    MywishlistPage,
  ],
  imports: [
    IonicPageModule.forChild(MywishlistPage),
    PipesModule
  ],
})
export class MywishlistPageModule {}
