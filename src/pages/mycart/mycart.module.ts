import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MycartPage } from './mycart';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    MycartPage,
  ],
  imports: [
    IonicPageModule.forChild(MycartPage),
    PipesModule
  ],
})
export class MycartPageModule {}
