import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Api } from './../../providers/api/api';
import { User } from './../../providers/user/user';
import { Settings } from './../../providers/settings/settings';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular'; 
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';


/**
 * Generated class for the MycartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mycart',
  templateUrl: 'mycart.html',
})
export class MycartPage {
  public cart:any=[];
  public totalLL:any=0;
  public totaldollars:any=0;
  public username="";
  public loggeduser:any='';
  public countcart:any=0;
  public currency:any={};
  public currencies:any=[];
  public currency_id=1;
  public deliveryCharge=2;
  constructor(public homeprovider:HomeproviderProvider,public navCtrl: NavController, public navParams: NavParams,public setting:Settings,public user:User,public event:Events,public settings:Settings,public api:Api,public iab:InAppBrowser,public alertCtrl:AlertController) {
    this.currency = this.homeprovider.currency;  
    this.currencies = this.homeprovider.currencies;
    this.currency_id = this.currency.id;  
    this.deliveryCharge = this.homeprovider.deliveryCharge;
    // if(this.user._user){
    //   this.username=this.user._user["fullName"]
    // }
    console.log('user',this.user.loggedinuser)
    this.user.loggedinuser.subscribe((data)=>{ 
      if(data && data.length>0){ 
        var user=data[0];
        this.loggeduser=user;
       
      } else{
        this.loggeduser='';
      }
    })
    this.user.cart.subscribe((data)=>{
      console.log('this.user.cart',data)
      if(data && data.length>0){
        this.countcart=data.length;
      } else{
        this.countcart=0;
      }
    })
    this.selectallmodel=false;

  } 
  ionViewDidEnter(){ 
    if(this.loggeduser["userId"]){
      if(this.homeprovider.currentPage == 'orderreceipt'){ 
        this.event.publish('selecttab:menu', 0);
      }
      
      this.user.cart.subscribe((data)=>{
        if(data && data.length>0){
          this.countcart=data.length;
        } else{
          this.countcart=0;
        }
      })
      this.selectallmodel= false;
      //--Get Cart
      this.GetCartFromDB(this.loggeduser["userId"]);
    } else{ 
        let alert = this.alertCtrl.create({ 
          subTitle: 'Please login first',
          buttons: [{
            text: 'Go to Login',
            handler: () => { 
              this.event.publish('selecttab:menu', 2); 
            }
          }]
        });
        alert.present();
    }
   
   }
   public productTocart:any={};
   Search($event){
    var val="";
    if($event.target) 
     val = $event.target.value; 
    this.navCtrl.push('products', {'src':'search','value':val});
   }
   GetCartFromDB(userid){
    this.cart=[];
    // console.log('GetCartFromDB',userid)
    this.api.get('getUserCart/'+userid).then((res)=>{
      //console.log('GetCartFromDB',res);
      //--Update cart
      if(res["success"]){
        //--clear car local storage
        this.settings.clearcart();
        var productarray=[];
        //--Add items to cart local stoarge
        for(var c=0;c<res["cart"].length;c++){
          //console.log('cart',res["arraysize"][c][0])
          this.productTocart={
            'carttable':res["cart"][c],
            'productselecteddetials':res["arrayproduct"][c],
            // 'productselectedcolor':[res["cart"]["arraycolor"][c]],
            // 'productselectedsize':[res["cart"]["arraysize"][c]],
            'productselectedquantity':res["cart"][c]["quantity"],
            'productselectedpriceLL':res["cart"][c]["unitPrice"],
            'productselectedpricedollars':res["cart"][c]["unitPrice"]
          } 
          //--size
          if(res["arraysize"][c][0]){
            this.productTocart["productselectedsize"]=[res["arraysize"][c][0]];
          }
          else{
            this.productTocart["productselectedsize"]=[];
          }
          //--color
          if(res["arraycolor"][c][0]){
            this.productTocart["productselectedcolor"]=[res["arraycolor"][c][0]];
          }
          else{
            this.productTocart["productselectedcolor"]=[];
          }
          this.productTocart.select=false;
          productarray.push(this.productTocart) 
        }
        this.settings.setcart(productarray).then((res)=>{
          // console.log('AddToCart',res)
          this.event.publish('CartEditI') 
          //--Get Cart
          this.settings.getcart().then((ress)=>{
            this.user.cart.next(ress);
            if(ress && ress.length>0){
              this.cart=ress;
              if(this.selectlatest){
                this.cart[this.cart.length -1].select=true; 
                this.AddToCart();
              }
              this.CaluclateTotal();
            }
          })
        }) 
        
      }
    })
    
      

  }
  ionViewDidLoad() {
    // //--check database
    // var userid=this.settings.getusersession(); 
    // this.GetCartFromDB(userid);


  }
  
  public selectallmodel:boolean=false;
  public selectall:boolean=false;
  Select(index){  
    // this.selectallmodel=false;
    // if(index === -1){
    //   for(var i=0;i<this.cart.length;i++){
    //     this.cart[i].select=!this.cart[i].select;
    //   }
    //   this.CaluclateTotal();
    //   this.AddToCart();
    // }
    // else{
      this.cart[index].select=!this.cart[index].select;  
      // this.selectallmodel=false; 
      this.AddToCart();
      // this.CaluclateTotal();
    // }
  }
  SelectAll(event){
    // console.log('SelectAll',event.checked);
    for(var i=0;i<this.cart.length;i++){
      this.cart[i].select=event.checked;
    }
    this.CaluclateTotal();
    this.AddToCart();
  }
  public selectlatest=false;
  ionViewWillEnter(){   
   this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
   //--check database
  //  var userid=this.settings.getusersession(); 
  //  this.event.unsubscribe('selectlatest');
  //  this.event.subscribe('selectlatest',()=>{
  //    this.selectlatest=true;
  //  })
  //  this.GetCartFromDB(userid);
  
  }
  GoToHome(){
    this.event.publish('selecttab:menu', 0)
  }
  RemoveItemFromCart(i){
    let alert = this.alertCtrl.create({ 
      subTitle: 'Remove product from cart?',
      buttons: [
       
        {
          text: 'Remove',
          handler: () => { 
            //--Update Db
            this.api.postpromise("RemoveProduct",{
              "cartId":this.cart[i]["carttable"]["cartId"] 
            }) 
            this.cart.splice(i,1);
            this.CaluclateTotal();
            this.AddToCart();
          }
        },
         {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
   
  }
  Subtractquantity(i){
    // console.log('Subtractquantity',this.cart[i].productselectedquantity)
    var newquantity=this.cart[i].productselectedquantity-1;
    if(newquantity==0){
      let alert = this.alertCtrl.create({ 
        subTitle: 'Remove product from cart?',
        buttons: [
         
          {
            text: 'Remove',
            handler: () => { 
              //--Update Db
              this.api.postpromise("RemoveProduct",{
                "cartId":this.cart[i]["carttable"]["cartId"] 
              }) 
              this.cart.splice(i,1);
              this.CaluclateTotal();
              this.AddToCart();
            }
          },
           {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      alert.present();
    }
    else{
      this.cart[i].productselectedquantity=newquantity;
       //--Update Db
      this.api.postpromise("UpdateOrder",{
        "cartId":this.cart[i]["carttable"]["cartId"],   
        "quantity":this.cart[i].productselectedquantity
      }) 
      this.CaluclateTotal;
      this.AddToCart();
    }
  }
  Addquantity(i){ 
    this.cart[i].productselectedquantity++;
    //--Update Db
    this.api.postpromise("UpdateOrder",{
      "cartId":this.cart[i]["carttable"]["cartId"],   
      "quantity":this.cart[i].productselectedquantity
    }) 
    this.CaluclateTotal();
    this.AddToCart();
  }
  AddToCart(){
    // console.log('AddToCart',this.cart)
    this.setting.setcart(this.cart).then((res)=>{
      // console.log('AddToCart',res)
      this.event.publish('CartEdit');
      this.CaluclateTotal();
      this.settings.getcart().then((ress)=>{
        this.user.cart.next(ress);
         
      })
    })
  }
  public totalinital:any;
  public selectmoreone:boolean=false;
  CaluclateTotal(){
    this.totalLL=0;
    this.totalinital=0;
    this.totaldollars=0;
    for(var c=0;c<this.cart.length;c++){
      if(this.cart[c].select){
        this.totalLL=this.totalLL+this.cart[c]['productselectedpriceLL'];
        this.totalinital=(this.totalinital+(this.cart[c]['productselectedpricedollars'] * this.cart[c]['productselectedquantity'])); 
      }
    }
    // console.log('total',this.totalinital)
    if(this.totalinital == 0){
      this.totaldollars=0;
    }else{
      if(this.totalinital<100){ 
        this.totaldollars=(this.totalinital+this.deliveryCharge).toFixed(2);
      }
      else{
        this.totaldollars=(this.totalinital).toFixed(2);
      }
    }
    
  }
  ProccessedtoCheckout(){
    try{ 
      
      if(this.loggeduser ==''){
        //--User no logged In
        let alert = this.alertCtrl.create({
          title: 'Confirm purchase',
          message: 'Please login or register before proceeding',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Login',
              handler: () => {
                this.event.publish('selecttab:menu', 2);
              }
            }
          ]
        });
        alert.present();
      }
      else{ 
        // console.log('prod',this.totalinital)
        if(!this.totalinital || this.totalinital == 0){
          let alert = this.alertCtrl.create({
            title: 'Purchase Error',
            message: 'Please select an item before proceeding',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              },
              {
                text: 'Ok',
                handler: () => {
                  
                }
              }
            ]
          });
          alert.present();
        }
        else{
          var x=this.totalinital.toFixed(2)
          this.navCtrl.push("shipping",{"total":this.totaldollars,'totalintial':x})

        }
      }
    
    }catch(er){
      console.log('ProccessedtoCheckout()',er)
    }
  }
  setCurrency(){
    this.homeprovider.currency = this.currencies.find(currency=>currency.id==this.currency_id);
    this.currency = this.homeprovider.currency;
  }
}
