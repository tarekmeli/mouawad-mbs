import { Api } from './../../providers/api/api';
import { User } from './../../providers/user/user';
import { Settings } from './../../providers/settings/settings';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController, AlertController, ToastController } from 'ionic-angular';
import { GiftPage } from '../gift/gift'; 

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  public banners:any=[];
  public categories:any=[];
  public brands:any=[];
  public bestsellers:any=[];
  public countcart:any=0;
  public loggeduser:any='';
  public currencies:any=[];
  public currency:any={};  
  public currency_id=1;
  constructor(public navCtrl: NavController, public navParams: NavParams,public homeprovider:HomeproviderProvider,public event:Events,public settings:Settings, public modalCtrl: ModalController,public user:User,public api:Api,public alertCtrl:AlertController,public toastCtrl:ToastController) {
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
    //--check if user is saved
    var userJson=JSON.parse(localStorage.getItem('userid'))
    if(userJson){
      // --Login
      this.user.login({
        "userName":userJson['emailAddress'],   
        "password":userJson['password']
      }).subscribe((res)=>{
        if(res["success"]){
          this.user.loggedinuser.next(res['user']);
        }
      });
    }
   
    if(this.homeprovider.banners){ 
      this.banners=this.homeprovider.banners  
    } 
    if(this.homeprovider.categories){ 
      this.categories=this.homeprovider.categories  
    } 
    if(this.homeprovider.brands){ 
      this.brands=this.homeprovider.brands  
    }  
    if(this.homeprovider.bestsellers){ 
      this.bestsellers=this.homeprovider.bestsellers
    }
    this.event.unsubscribe('categoryname:menu');
    this.event.subscribe('categoryname:menu',(data) => {
      console.log(data)
      this.GoToSubCategories(data["category"])
    });
    
  }
  ionViewWillEnter(){ 
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
    this.homeprovider.currentPage = 'home';
  }
  ionViewWillLeave(){
    this.homeprovider.currentPage = 'other';
  }
  ionViewDidLoad() { 
    this.user.loggedinuser.subscribe((data)=>{
      console.log('data login',data); 
      if(data && data.length>0){ 
        var user=data[0];
        this.loggeduser=user;
        //--Get Cart
        this.GetCartFromDB(user["userId"]);
        //--Get Wish list
        this.GetWishList(user["userId"]);
      } else{
        this.loggeduser='';
      }
    })
    this.user.cart.subscribe((data)=>{
      if(data && data.length>0){
        this.countcart=data.length;
      } else{
        this.countcart=0;
      }
    })
  }
  public GetWishList(userid){
    if(this.homeprovider.bestsellers){
      this.api.get("GetUserWish/"+userid).then((res)=>{ 
        if(res["wishlist"]){
          //loop through bestsellers
          for(var i=0;i<this.bestsellers.length;i++){
            for(var j=0;j<res["wishlist"].length;j++){
              if(this.bestsellers[i]["productId"]==res["wishlist"][j]["productId"]){ 
                this.bestsellers[i]["wishlistid"]=res["wishlist"][j]["wishlistId"];
                this.bestsellers[i]["inwishlist"]=true;
              }
            }
          }
        }
        //console.log('bestsellers updated',this.bestsellers)
      })
    }
  }
  public productTocart:any={};
  GetCartFromDB(userid){ 
    // console.log('GetCartFromDB',userid)
    this.api.get('getUserCart/'+userid).then((res)=>{
      // console.log('GetCartFromDB',res);
      //--Update cart
      if(res["success"]){
        //--clear car local storage
        this.settings.clearcart();
        var productarray=[];
        //--Add items to cart local stoarge
        for(var c=0;c<res["cart"].length;c++){
          // console.log('cart',res["arraysize"][c][0])
          this.productTocart={
            'carttable':res["cart"][c],
            'productselecteddetials':res["arrayproduct"][c],
            'productselectedquantity':res["cart"][c]["quantity"],
            'productselectedpriceLL':res["cart"][c]["unitPrice"],
            'productselectedpricedollars':res["cart"][c]["unitPrice"]
          } 
          //--size
          if(res["arraysize"][c][0]){
            this.productTocart["productselectedsize"]=[res["arraysize"][c][0]];
          }
          else{
            this.productTocart["productselectedsize"]=[];
          }
          //--color
          if(res["arraycolor"][c][0]){
            this.productTocart["productselectedcolor"]=[res["arraycolor"][c][0]];
          }
          else{
            this.productTocart["productselectedcolor"]=[];
          }
          productarray.push(this.productTocart)
         
        }
        this.settings.setcart(productarray).then((res)=>{
          // console.log('AddToCart',res)
          this.event.publish('CartEditI') 
          //--Get Cart
          this.settings.getcart().then((ress)=>{
            this.user.cart.next(ress);
          })
        }) 
      }
    })
    
      

  }
  GoToSubCategories(category){ 
    console.log(category)
    // if(this.loggeduser!='')
    // { 
      if(category.isUnderConst=='1'){
        this.navCtrl.push('sub-category', { 'categoryname':category.categoryName,'categoryid': category.categoryId,'category':category });  
      } else{
        this.event.publish('categoryname', category.categoryName);
        this.api.get("categories/"+category.categoryId).then((res)=>{
          console.log('GoToSubCategoriesDetails',res["subcategories"])
          if(res["subcategories"].length>0){
            this.navCtrl.push('sub-category', { 'categoryname':category.categoryName,'categoryid': category.categoryId,'category':category }); 
          }
          else{
            this.navCtrl.push('products', {'src':'category','categoryid':category.categoryId}); 
          }
        });
      }
      
  //   }else{
  //   let alert = this.alertCtrl.create({ 
  //     subTitle: 'Please login first',
  //     buttons: [{
  //       text: 'Go to Login',
  //       handler: () => { 
  //         this.navCtrl.parent.select(2);
  //       }
  //     }]
  //   });
  //   alert.present();
  // }
  }
  GotoBrands(){
    this.navCtrl.push('brands');
    // if(this.loggeduser!='')
    // { 
    //   this.navCtrl.push('brands');
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  GotoCart(){ 
    this.event.publish('selecttab:menu', 3); 
    // if(this.loggeduser!='')
    // { 
    //   this.event.publish('selecttab:menu', 3);  
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  GotoProductnew(){
    this.navCtrl.push('products', {'src':'newarrival'});
    // if(this.loggeduser!='')
    // { 
     // this.navCtrl.push('products', {'src':'newarrival'});
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  GotoProductpro(){
    this.navCtrl.push('products', {'src':'promotions'});
    // if(this.loggeduser!='')
    // { 
    //   this.navCtrl.push('products', {'src':'promotions'});
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  GotoProductbest(){
    this.navCtrl.push('products', {'src':'bestsellers'});
    // if(this.loggeduser!='')
    // { 
    //   this.navCtrl.push('products', {'src':'bestsellers'});
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  OpenModal(){
    let giftCardModal = this.modalCtrl.create(GiftPage);
    giftCardModal.present();
    // if(this.loggeduser!='')
    // { 
    //   let giftCardModal = this.modalCtrl.create(GiftPage);
    //   //let giftCardModal = this.modalCtrl.create(WelcomeModalPage);
    //   giftCardModal.present();
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  Search($event){
    var val="";
    if($event.target) 
     val = $event.target.value; 
    this.navCtrl.push('products', {'src':'search','value':val});
    // if(this.loggeduser!='')
    // {
    //   var val="";
    //     if($event.target) 
    //       val = $event.target.value; 
    //   this.navCtrl.push('products', {'src':'search','value':val});
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  GotoProductDetials(productid){
    console.log('type of productid',typeof productid)
    this.navCtrl.push('productsdetials', {'productid':productid});
    // if(this.loggeduser!='')
    // { 
    //   this.navCtrl.push('productsdetials', {'productid':productid});
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  RemoveWishList(product){
    let alert = this.alertCtrl.create({ 
      subTitle: 'Remove product from wishlist?',
      buttons: [
       
        {
          text: 'Remove',
          handler: () => {
            console.log('Remove clicked',product,product["wishlistid"]);
            this.api.delete('deletewishlist/'+product["wishlistid"]).subscribe((res)=>{
              console.log('deletewishlist',res)
              product["inwishlist"]=false;
            })
          }
        },
         {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }
  AddToWishList(product){
    console.log(product,product["productId"])
    // if(this.loggeduser!=''){
      this.api.postpromise("AddWish",{
        "productId":product["productId"],   
        "userId":this.loggeduser["userId"]
      }).then((res)=>{
        console.log(res)
        product["inwishlist"]=true;
        product["wishlistid"]=res["wish"].id;
        let toast = this.toastCtrl.create({
          message:  res["message"], 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close',
          duration: 1500  
        });
        toast.present(); 

      })
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  GotoSelectedBrand(brandid){
    this.navCtrl.push('products', {'src':'brand','brandid':brandid});
    // if(this.loggeduser !='')
    // { 
    //   this.navCtrl.push('products', {'src':'brand','brandid':brandid});
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  setCurrency(){
    this.homeprovider.currency = this.currencies.find(currency=>currency.id==this.currency_id);
    this.currency = this.homeprovider.currency;
  }
}
