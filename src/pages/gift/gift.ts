import { User } from './../../providers/user/user';
import { Settings } from './../../providers/settings/settings';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, Events, App } from 'ionic-angular';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';

/**
 * Generated class for the GiftPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name:"gift"
})
@Component({
  selector: 'page-gift',
  templateUrl: 'gift.html',
})
export class GiftPage {
  public selected=50000;
  public currency:any={};
  constructor(public homeprovider:HomeproviderProvider, public appCtrl: App,public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,public setting:Settings,public user:User,public alertCtrl:AlertController,public event:Events) {
    this.currency = this.homeprovider.currency;
  }

  ionViewDidLoad() {
    this.currency = this.homeprovider.currency;
    console.log('ionViewDidLoad GiftPage');
  }
  SelectCoupon(value){
    this.selected=value;
  }
  closeModal() {
    this.viewCtrl.dismiss()
  }
  GoToShipping(){
     //get logged in user
     let user =this.setting.getuser();
     let loggedinuser=this.user._user;
     console.log("loggedinuser",loggedinuser)
     if(!user && !loggedinuser){
       //--User no logged In
       let alert = this.alertCtrl.create({
         title: 'Confirm purchase',
         message: 'Please login or register before proceeding',
         buttons: [
           {
             text: 'Cancel',
             role: 'cancel',
             handler: () => {
               console.log('Cancel clicked');
             }
           },
           {
             text: 'Login',
             handler: () => {
               this.event.publish('selecttab:menu', 2);
             }
           }
         ]
       });
       alert.present();
     }
     else{ 
       this.viewCtrl.dismiss()
       this.appCtrl.getRootNav().push("shippinggift",{"total":this.selected, "currency":this.currency}); 
     }
  }

}
