import { Settings } from './../../providers/settings/settings';
import { Component } from '@angular/core';
import { IonicPage, MenuController, NavController, Platform, LoadingController, ToastController, Events } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { HomeproviderProvider } from '../../providers/homeprovider/homeprovider';
import { User } from '../../providers';

export interface Slide {
  title: string;
  description: string;
  image: string;
}

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})
export class TutorialPage { 
  dir: string = 'ltr'; 
  public loading:any;
  constructor(public navCtrl: NavController, public menu: MenuController, translate: TranslateService, public platform: Platform,public homeprovider:HomeproviderProvider,public loadingCtrl:LoadingController,public toastCtrl:ToastController,public user:User,public setting:Settings,public event:Events) {
    this.dir = platform.dir(); 
    this.event.subscribe('deeplink');
    this.event.subscribe('deeplink',(productId)=>{
      console.log('deeplink',productId);
      this.homeprovider.GetHomeComponents().then((res)=>{  
        if(res["success"]) {
          
          this.homeprovider.banners=res["Home"]["banners"].sort(function(a, b){return a.categoryOrder - b.categoryOrder})
          this.homeprovider.categories=res["Home"]["categories"].sort(function(a, b){return a.categoryOrder - b.categoryOrder})
          this.homeprovider.brands=res["Home"]["brands"].sort(function(a, b){return a.brandOrder - b.brandOrder})
          this.homeprovider.bestsellers=res["Home"]["bestsellers"].sort(function(a, b){return a.productOrder - b.productOrder});
          console.log('type of productid',productId) 
          this.navCtrl.push('productsdetials', {'productid':productId.productId}, {
            animate: true,
            direction: 'forward'
          });
          // this.navCtrl.push('TabsPage',{}, {
          //   animate: true,
          //   direction: 'forward'
          // });
        }
        else
        {
          
           let toast = this.toastCtrl.create({
            message:  "Data not retrieved.Please try again later.", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
        }
      }, (err) => {
        console.log('error',err)
        
        // Unable to log in
        let toast = this.toastCtrl.create({
          message:  "Data not retrieved.Please try again later.", 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close'
        });
        toast.present();
      }); 
      
    })
  }

  startApp() {
    
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    }); 
    this.loading.present();
  
    this.homeprovider.GetHomeComponents().then((res)=>{  
      if(res["success"]) {
        
        this.homeprovider.banners=res["Home"]["banners"].sort(function(a, b){return a.categoryOrder - b.categoryOrder})
        this.homeprovider.categories=res["Home"]["categories"].sort(function(a, b){return a.categoryOrder - b.categoryOrder})
        this.homeprovider.brands=res["Home"]["brands"].sort(function(a, b){return a.brandOrder - b.brandOrder})
        this.homeprovider.bestsellers=res["Home"]["bestsellers"].sort(function(a, b){return a.productOrder - b.productOrder});
        this.navCtrl.push('TabsPage',{}, {
          animate: true,
          direction: 'forward'
        });
      }
      else
      {
        if(this.loading!=""){
          this.loading.dismiss();
        } 
         let toast = this.toastCtrl.create({
          message:  "Data not retrieved.Please try again later.", 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close'
        });
        toast.present();
      }
    }, (err) => {
      console.log('error',err)
      //this.navCtrl.push(MainPage);
      if(this.loading!=""){
        this.loading.dismiss();
      }
      // Unable to log in
      let toast = this.toastCtrl.create({
        message:  "Data not retrieved.Please try again later.", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
    }); 
    
  }
  
  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    //--check if user is logged in 
    var user=this.setting.getuser();
    console.log('t',user)
    if(user!= null && user!='undefined')
      this.user._user=JSON.parse(user)
    
    if(this.user._user)
    {
      this.menu.enable(true, 'loggedInMenu'); 
      this.event.publish('userloggedin',this.user._user) 
    }
    else{ 
      this.menu.enable(true,'loggedOutMenu');
    }
  }
  ionViewDidLeave(){
    console.log('ionViewDidLeave')
    if(this.loading && this.loading!=""){
      this.loading.dismiss();
    }
  } 

}
