import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular'; 
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Api, Settings } from '../../providers';

/**
 * Generated class for the PriceCheckingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 
@IonicPage()
@Component({
  selector: 'page-price-checking',
  templateUrl: 'price-checking.html',
})
export class PriceCheckingPage {
  constructor(public navCtrl: NavController, public navParams: NavParams, public barcodeScanner: BarcodeScanner,public alertCtrl : AlertController, public api:Api,public settings:Settings) {
 
  }
  public searchedCodes: any =[];
  public is_Supplier:any =false;
  public enteredcode:any ='';
  ionViewDidLoad() {
    console.log('ionViewDidLoad PriceCheckingPage');
    var getuser=this.settings.gettempuser();
    if(getuser){
      this.is_Supplier = (JSON.parse(getuser).is_Supplier == '0' ? false: true);
    }
    console.log('is_Supplier',this.is_Supplier);
    // this.barcodeScanner.scan().then(barcodeData => {
    //   // alert('Barcode data'+ JSON.stringify(barcodeData));
    //   if(barcodeData && barcodeData.text && barcodeData.text != '' ) {
        
    //     var code = barcodeData.text +"-";
    //     this.api.get('getbarcodes/'+code).then((res)=>{ 
    //       if(res && res["barcodes"] && res["barcodes"].length>0){
    //         for(var i=0;i<res["barcodes"].length;i++){
    //           var index = this.searchedCodes.findIndex(bcode=>{ 
    //             return bcode.barcodeId == res["barcodes"][i].barcodeId;
    //           });
    //           if(index == -1){
    //            this.searchedCodes.push(res["barcodes"][i])
    //           }
    //         }
    //       }
    //       else{
    //         let alert = this.alertCtrl.create({
    //           title: 'Product Not Found',
    //           message: '',
    //           buttons: [
    //             {
    //               text: 'Cancel',
    //               role: 'cancel',
    //               handler: () => {
                    
    //               }
    //             } 
    //           ]
    //         });
    //         alert.present();
    //       }
    //     })
    //   }
  
    //  }).catch(err => {
    //      console.log('Error', err);
    //      this.navCtrl.pop();
    //  });
  }

  ScanMore(){
    this.barcodeScanner.scan().then(barcodeData => {
      // alert('Barcode data'+ JSON.stringify(barcodeData));
      if(barcodeData && barcodeData.text && barcodeData.text != '' ) {
        
        var code = barcodeData.text +"-";
        this.api.get('getbarcodes/'+code).then((res)=>{ 
          if(res && res["barcodes"] && res["barcodes"].length>0){
            for(var i=0;i<res["barcodes"].length;i++){
              var index = this.searchedCodes.findIndex(bcode=>{  
                return bcode.barcodeId == res["barcodes"][i].barcodeId;
              });

              if(index == -1){
               this.searchedCodes.push(res["barcodes"][i])
              }
            }
          }
          else{
            let alert = this.alertCtrl.create({
              title: 'Product Not Found',
              message: '',
              buttons: [
                {
                  text: 'Cancel',
                  role: 'cancel',
                  handler: () => {
                    
                  }
                } 
              ]
            });
            alert.present();
          }
        })
      }
  
     }).catch(err => {
         console.log('Error', err);
         this.navCtrl.pop();
     });
  }
  SearchbyCode(){
    if(this.enteredcode != ''){
      this.api.get('getbarcodesByCode/'+this.enteredcode).then((res)=>{
        console.log(res)
        if(res && res["barcodes"] && res["barcodes"].length>0){
          for(var i=0;i<1;i++){
            var index = this.searchedCodes.findIndex(bcode=>{  
              return bcode.barcodeId == res["barcodes"][i].barcodeId;
            });

            if(index == -1){
             this.searchedCodes.push(res["barcodes"][i])
            }
          }
        }
        else{
          let alert = this.alertCtrl.create({
            title: 'Code  Not Found',
            message: '',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                  
                }
              } 
            ]
          });
          alert.present();
        }
      });
    } else{
      let alert = this.alertCtrl.create({
                  title: 'Please enter code',
                  message: '',
                  buttons: [
                    {
                      text: 'Cancel',
                      role: 'cancel',
                      handler: () => {
                        
                      }
                    } 
                  ]
       });
      alert.present();
    }
  }
}
