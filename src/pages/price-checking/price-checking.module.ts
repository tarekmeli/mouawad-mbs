import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PriceCheckingPage } from './price-checking';

@NgModule({
  declarations: [
    PriceCheckingPage,
  ],
  imports: [
    IonicPageModule.forChild(PriceCheckingPage),
  ],
})
export class PriceCheckingPageModule {}
