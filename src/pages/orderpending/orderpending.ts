import { Api } from './../../providers/api/api';
import { User } from './../../providers/user/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';

/**
 * Generated class for the OrderpendingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orderpending',
  templateUrl: 'orderpending.html',
})
export class OrderpendingPage {
  public loggeduser:any='';
  public countcart:any=0;
  public orders:any;
  public currency:any={};
  constructor(public homeprovider:HomeproviderProvider,public navCtrl: NavController, public navParams: NavParams,public user:User,public api:Api) {
    this.currency = this.homeprovider.currency;
  }
  ionViewWillEnter(){  
    this.currency = this.homeprovider.currency; 
    this.homeprovider.currentPage = 'other'; 
  }
  ionViewWillLeave(){ 
    this.homeprovider.currentPage = 'home'; 
  }
  ionViewDidLoad() {
    this.user.loggedinuser.subscribe((data)=>{
      console.log('data login',data); 
      if(data && data.length>0){ 
        var user=data[0];
        this.loggeduser=user;
        //--Get Cart
        this.GetOrders(user["userId"]);
       
      } else{
        this.loggeduser='';
      }
    })
    this.user.cart.subscribe((data)=>{
      if(data && data.length>0){
        this.countcart=data.length;
      } else{
        this.countcart=0;
      }
    })
  }
  GetOrders(userId){
    this.api.get("GetOrders/"+userId).then((res)=>{ 
      this.orders=[];
      if(res["purchase"]){
        
        for(var i=0;i<res["purchase"].length;i++){
          console.log("response",res["purchase"][i]['response'] === 'pending')
          if(res["purchase"][i]['paymentMethod'] === 'pending'){
            this.orders.push(res["purchase"][i])
          }
        }
        // this.orders=res["purchase"];
        // .sort((a, b) => b.purchaseDate - a.purchaseDate)
        this.orders=this.orders.sort((a, b) => b.purchaseId - a.purchaseId)
        console.log(this.orders)
      }
    })
  }
  GoToOrder(order){
    console.log('GoToOrder',order)
    this.navCtrl.push('orderreceipt', {'purchaseid':order["purchaseId"],'history':true,'purchase':order});

  }

}
