import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderpendingPage } from './orderpending';

@NgModule({
  declarations: [
    OrderpendingPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderpendingPage),
  ],
})
export class OrderpendingPageModule {}
