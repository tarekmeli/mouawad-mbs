import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ToastController, LoadingController, ModalController } from 'ionic-angular';
import { Api, Settings, User } from '../../providers';
import { GiftPage } from '../gift/gift';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';

/**
 * Generated class for the SubcategorydetialsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name:'subcategorydetials'
})
@Component({
  selector: 'page-subcategorydetials',
  templateUrl: 'subcategorydetials.html',
})
export class SubcategorydetialsPage {

  public subcateogriesdetials:any=[];
  public subcategoryname='';
  public loading:any;
  public countcart:any=0;
  public username="";
  public currency:any={};
  public currencies:any=[];
  public currency_id=1;
  constructor(public homeprovider:HomeproviderProvider,public navCtrl: NavController, public navParams: NavParams,public api:Api,public event:Events,public loadingCtrl:LoadingController,public toastCtrl:ToastController,public modalCtrl: ModalController,public settings:Settings,public user:User) { 
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    }); 
    this.loading.present();
    this.event.subscribe('subcategoryname', (categoryname) => {
      if(categoryname.length>8)
        this.subcategoryname=categoryname.substring(0,8)+"...";
      else
        this.subcategoryname=categoryname 
    });
    if(this.navParams.get('subcategoryname').length>8) 
      this.subcategoryname=this.navParams.get('subcategoryname').substring(0,8)+"...";
    else
      this.subcategoryname=this.navParams.get('subcategoryname');  
    this.api.get("categories/"+this.navParams.get('subcategoryid')).then((res)=>{
      console.log('SubcategorydetialsPage',res)
      if(res["success"])
      {
        if(this.loading!=""){
          this.loading.dismiss();
        } 
        this.subcateogriesdetials=res["subcategories"].sort(function(a, b){return a.categoryOrder - b.categoryOrder})
      } 
      else{
        if(this.loading!=""){
          this.loading.dismiss();
        } 
         let toast = this.toastCtrl.create({
          message:  "Data not retrieved.Please try again later.", 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close'
        });
        toast.present();
      }
    }).catch((er)=>{
      if(this.loading!=""){
        this.loading.dismiss();
      } 
       let toast = this.toastCtrl.create({
        message:  "Data not retrieved.Please try again later.", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
    })
    //--Get Cart
    this.settings.getcart().then((res)=>{
      console.log('GetCart',res)
      if(res && res.length>0)
        this.countcart=res.length;
    })
    if(this.user._user){
      this.username=this.user._user["fullName"]
    }
    this.event.unsubscribe('CartEdit');
    this.event.subscribe('CartEdit',() => {
        //--Get Cart
      this.settings.getcart().then((res)=>{
        console.log('GetCart',res)
        if(res && res.length>0)
          this.countcart=res.length;
      })
    });
  }
  ionViewWillEnter(){ 
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
    
    this.event.unsubscribe('CartEdit');
    this.event.subscribe('CartEdit',() => {
      console.log('CartEdit Home') 
      //--Get Cart
      this.settings.getcart().then((res)=>{
        console.log('GetCart',res)
        if(res && res.length>0)
          this.countcart=res.length;
      })
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SubcatagoriesPage');
  }
  ionViewDidLeave(){
    console.log('ionViewDidLeave')
    if(this.loading!=""){
      this.loading.dismiss();
    }
  } 
  GotoProduct(categoryid,categoryname){
    //this.navCtrl.push('products', {'src':'category','categoryid':categoryid});
    this.event.publish('subcategoryname', categoryname);
    this.api.get("categories/"+categoryid).then((res)=>{
      console.log('GoToSubCategoriesDetails',res["subcategories"])
      if(res["subcategories"].length>0){
        this.navCtrl.push('subcategorydetials', { 'subcategoryname':categoryname,'subcategoryid': categoryid }); 
      }
      else{
        this.navCtrl.push('products', {'src':'category','categoryid':categoryid}); 
      }
    });
  }
  GotoBrands(){
    this.navCtrl.push('brands');
  }
  GotoProductnew(){
    this.navCtrl.push('products', {'src':'newarrival'});
  }
  GotoProductpro(){
    this.navCtrl.push('products', {'src':'promotions'});
  }
  GotoProductbest(){
    this.navCtrl.push('products', {'src':'bestsellers'});
  }
  OpenModal(){
    let giftCardModal = this.modalCtrl.create(GiftPage);
    giftCardModal.present();
  }
  GotoCart(){ 
    this.event.publish('selecttab:menu', 3);  
  }
  Search($event){
    var val="";
      if($event.target) 
        val = $event.target.value; 
    this.navCtrl.push('products', {'src':'search','value':val});
  }
  setCurrency(){
    this.homeprovider.currency = this.currencies.find(currency=>currency.id==this.currency_id);
    this.currency = this.homeprovider.currency;
  }
}
