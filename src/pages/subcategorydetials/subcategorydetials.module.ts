import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubcategorydetialsPage } from './subcategorydetials';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    SubcategorydetialsPage,
  ],
  imports: [
    IonicPageModule.forChild(SubcategorydetialsPage),
    PipesModule
  ],
})
export class SubcategorydetialsPageModule {}
