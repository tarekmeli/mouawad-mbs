import { Api } from './../../providers/api/api';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, ModalController, Content } from 'ionic-angular';
import { Settings } from '../../providers/settings/settings';
import { User } from '../../providers/user/user';
import { SocialSharing } from '@ionic-native/social-sharing';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';

/**
 * Generated class for the LoyalityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-loyality',
  templateUrl: 'loyality.html',
})
export class LoyalityPage {
  @ViewChild(Content) content: Content;
  public username="";
  public userpoints=0;
  public countcart:any=0;
  public userpointslogs=[];
  public loggeduser:any;
  public currency:any={};
  public currencies:any=[];
  public currency_id=1;
  public loyaltyProgramPoints:any=[];
  constructor(public homeprovider:HomeproviderProvider,public navCtrl: NavController, public navParams: NavParams,public user:User,public event:Events,public settings:Settings,public modalCtrl: ModalController ,public alertCtrl:AlertController,public socialSharing: SocialSharing, public api:Api) {
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
    
    this.loyaltyProgramPoints = this.homeprovider.loyaltyProgramPoints;
  }
  
  ionViewDidLoad(){

    this.user.loggedinuser.subscribe((data)=>{
      console.log('data login',data); 
      if(data && data.length>0){ 
        var user=data[0]; 
        this.loggeduser=user; 
        this.userpoints = parseFloat(this.loggeduser['userPoints']) ;
        
        this.GetUserPoint(user["userId"]);
      } else{
        this.loggeduser='';
      }
    })
    this.user.cart.subscribe((data)=>{
      if(data && data.length>0){
        this.countcart=data.length;
      } else{
        this.countcart=0;
      }
    })
  }
  GetUserPoint(userId){
    this.api.get("getpointsuser/"+userId).then((res)=>
    {
      console.log('getpointsuser',res);
      this.userpointslogs=res["pointsuser"];
      for(var i=0;i<this.userpointslogs.length;i++){
        console.log('pointsuser',this.userpointslogs[i]); 

      } 
    })
    this.codes=[];
    //--Get User Codes
    this.api.get('getUserCodes/'+userId).then((codes)=>{
      if(codes["codes"].length>0){
        for(var c=0;c<codes["codes"].length;c++){
        if(codes["codes"][c]["isActive"]!=0)
          this.codes.push(codes["codes"][c])
        }
        // this.codes=codes["codes"];
      }
     
    })
  }
  ionViewWillEnter(){ 
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
  }
  Search($event){
    var val="";
    if($event.target) 
      val = $event.target.value; 
    this.navCtrl.push('products', {'src':'search','value':val});
    // if(this.loggeduser)
    // { 
    //   var val="";
    //     if($event.target) 
    //       val = $event.target.value; 
    //   this.navCtrl.push('products', {'src':'search','value':val});
    // }else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
  }
  GotoCart(){ 
    if(this.loggeduser)
    { 
      this.event.publish('selecttab:menu', 3);  
    }else{
      let alert = this.alertCtrl.create({ 
        subTitle: 'Please login first',
        buttons: [{
          text: 'Go to Login',
          handler: () => { 
            this.navCtrl.parent.select(2);
          }
        }]
      });
      alert.present();
    }
  }
  ShareApp(){
    if(this.loggeduser)
    { 
      this.socialSharing.share("Check Mouawad MBS App","Mouawad MBS","","https://play.google.com/store/apps/details?id=com.mouawad.mbs&hl=en").then((res)=>{
        console.log('ShareApp',res,this.loggeduser)
        if(res){
          
          this.api.postpromise('updateuser',
          { 
            "userid" :this.loggeduser["userId"],
            "fullName":this.loggeduser['fullName'],
            "gender":this.loggeduser['gender'],
            "dateBirthday":this.loggeduser['dateBirthday'], 
            "emailAddress":this.loggeduser['emailAddress'], 
            "mobile":this.loggeduser['mobile'], 
            "occupation":this.loggeduser['occupation'], 
            "financeNb":this.loggeduser['financeNb'],  
            "userPoints": parseInt(this.loggeduser['userPoints']) + 10
                      
          })
        }
      })
    }
  }
  public codes:any=[];
  RedeemPoint(){ 
    if(this.loggeduser)
    {
      this.api.get('getuser/'+this.loggeduser["userId"]).then((res)=>{
        console.log('Get',res)
        if(res["user"]["userPoints"]>=this.homeprovider.loyaltyProgramPoints.points){
          this.api.get('generateUserCode/'+this.loggeduser["userId"]).then((rescode)=>{
            console.log('generateUserCode',rescode)
            //--update user point (Decrease this.homeprovider.loyaltyProgramPoints.points)
            this.api.postpromise('updateuser',
            { 
              "userid" :this.loggeduser['userId'],
              "fullName":this.loggeduser['fullName'],
              "gender":this.loggeduser['gender'],
              "dateBirthday":this.loggeduser['dateBirthday'], 
              "emailAddress":this.loggeduser['emailAddress'], 
              "mobile":this.loggeduser['mobile'], 
              "occupation":this.loggeduser['occupation'], 
              "financeNb":this.loggeduser['financeNb'], 
              "userPoints": parseInt(this.loggeduser['userPoints']) - this.homeprovider.loyaltyProgramPoints.points
                        
            })
          })
          this.codes=[]
          //--Get User Codes
          this.api.get('getUserCodes/'+this.loggeduser["userId"]).then((codes)=>{
            if(codes["codes"].length>0){
              for(var c=0;c<codes["codes"].length;c++){
              if(codes["codes"][c]["isActive"]!=0)
                this.codes.push(codes["codes"][c])
              }
              // this.codes=codes["codes"];
            }
            else{
              let alert = this.alertCtrl.create({ 
                subTitle: 'Please collect more points to get discounts',
                buttons: ['Dismiss']
              });
              alert.present();
            }
          })
        
        }
        else{
          this.codes=[]
          //--Get User Codes
          this.api.get('getUserCodes/'+this.loggeduser["userId"]).then((codes)=>{
            console.log('getUserCodes',codes)
            if(codes["codes"].length>0){
              for(var c=0;c<codes["codes"].length;c++){
                if(codes["codes"][c]["isActive"]!=0)
                  this.codes.push(codes["codes"][c])
              }
              console.log(this.codes)
            }
            //else{
              let alert = this.alertCtrl.create({ 
                subTitle: 'Please collect more points to get discounts',
                buttons: ['Dismiss']
              });
              alert.present();
            //}
          }) 
        }
        
      })
    }
  }
  setCurrency(){
    this.homeprovider.currency = this.currencies.find(currency=>currency.id==this.currency_id);
    this.currency = this.homeprovider.currency;
  }

}
