import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoyalityPage } from './loyality';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    LoyalityPage,
  ],
  imports: [
    IonicPageModule.forChild(LoyalityPage),
    PipesModule
  ],
})
export class LoyalityPageModule {}
