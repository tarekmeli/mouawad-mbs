import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SendgiftPage } from './sendgift';

@NgModule({
  declarations: [
    SendgiftPage,
  ],
  imports: [
    IonicPageModule.forChild(SendgiftPage),
  ],
})
export class SendgiftPageModule {}
