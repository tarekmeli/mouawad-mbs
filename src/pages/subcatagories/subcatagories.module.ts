import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubcatagoriesPage } from './subcatagories';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    SubcatagoriesPage,
  ],
  imports: [
    IonicPageModule.forChild(SubcatagoriesPage),
    PipesModule
  ],
})
export class SubcatagoriesPageModule {}
