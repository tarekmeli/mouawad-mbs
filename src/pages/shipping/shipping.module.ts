import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShippingPage } from './shipping';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ShippingPage,
  ],
  imports: [
    IonicPageModule.forChild(ShippingPage),
    PipesModule
  ],
})
export class ShippingPageModule {}
