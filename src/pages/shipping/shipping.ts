import { Api } from './../../providers/api/api';
import { Settings } from './../../providers/settings/settings';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events, ToastController, LoadingController, Content } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { User } from './../../providers/user/user';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { Geolocation } from '@ionic-native/geolocation';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';
/**
 * Generated class for the ShippingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@IonicPage({
  name:"shipping"
})
@Component({
  selector: 'page-shipping',
  templateUrl: 'shipping.html',
})
export class ShippingPage {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild(Content) content: Content;
  map: any;
  public edditaddress:boolean=false;
  public addresses:any=[];
  public code:any="";
  public codeid:any="";
  public profilefullname:any="";
  public profileMobile:any="";
  public profilehome:any="";
  public profilehomeline:any="";
  public profileProvince:any="";
  public profileCity:any="";
  public username:any="";
  public profileStreet:any="";
  public profileBuilding:any="";
  public profileFloor:any="";
  public countcart=0;
  public total=0;
  public user:any;
  public loading:any;
  public paymentcard=true;
  public paymentcash=false;
  public lat:any;
  public lng:any;
  public loggeduser:any;
  public profileaddtype:any="";
  public profileaddMobile:any="";
  public profileaddProvince:any="";
  public profileaddCity:any="";
  public profileaddCountry:any="";
  public profileaddBuilding:any="";
  public profileaddpostalCode:any="";
  public profileaddaddressDetails:any="";
  public countriesall:any=[];
  public profileDefAddMobile:any="";
  public profileedittype:any="";
  public profileeditMobile:any="";
  public profileeditProvince:any="";
  public profileeditCity:any="";
  public profileeditCountry:any="";
  public profileeditBuilding:any="";
  public profileeditpostalCode:any="";
  public profileeditaddressDetails:any="";
  public profileeditadefault:any="";
  public totalintial=0;
  public purchase:any='';
  public currency:any={};
  public currency_id=1;
  public currencies:any=[];
  public deliveryCharge=2;
  public address_country=123;
  public loyaltyProgramPoints:any={};
  constructor(public homeprovider:HomeproviderProvider,public navCtrl: NavController, public navParams: NavParams,public setting:Settings,public alertCtrl:AlertController,public api:Api,public event:Events,public toastCtrl:ToastController,public loadingCtrl:LoadingController,public iab:InAppBrowser,public User:User,public geolocation: Geolocation,public openNativeSettings: OpenNativeSettings) { 
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;

    this.deliveryCharge = this.homeprovider.deliveryCharge;
    
    this.loyaltyProgramPoints = this.homeprovider.loyaltyProgramPoints; 
    
    this.total=this.navParams.get('total');
    this.totalintial=this.navParams.get('totalintial');
    if(this.currency.description !="USD"){ 
      this.total = this.currency.rate * this.total;
      this.totalintial = this.currency.rate * this.totalintial;
      this.deliveryCharge  = this.currency.rate * this.deliveryCharge;
    } else{ 
      this.total = this.total;
      this.totalintial =  this.totalintial;
    }
    console.log('totalintial',this.totalintial)
    // --Get Countries
    this.api.get('getCountries').then((res)=>{
      console.log('getCountries',res["countries"])
      this.countriesall=res["countries"];
    })  
    this.purchase=this.navParams.get('purchase');
  }
  ionViewWillEnter(){    
  
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id; 
    
    if(this.currency.id == 1){
        this.paymentcash = true;
        this.paymentchangecash();   
    }
    
    
    this.loyaltyProgramPoints = this.homeprovider.loyaltyProgramPoints; 
    
  }
  public profileaddadefault:boolean=false;
  ionViewDidLoad() {
    console.log(this.User)
    this.User.loggedinuser.subscribe((data)=>{
      console.log('data login',data); 
      if(data && data.length>0){ 
        var user=data[0];
        this.loggeduser=user;
        this.user=user;
        this.GetAddress(user["userId"]);
        this.RedeemPoint(user["userId"]);
      } else{
        this.loggeduser='';
      }
    })
    this.User.cart.subscribe((data)=>{
      if(data && data.length>0){
        this.countcart=data.length;
      } else{
        this.countcart=0;
      }
    })
  }
  GetAddress(userid){
    this.addresses=[];
    //--Get User Address
    this.api.get('getUserAddress/'+userid).then((res)=>{ 
        this.addresses=res["addresses"]; 
        if(this.purchase && this.purchase !==''){
          console.log('GetAddress',this.purchase)
          if(this.purchase['useraddressId']){
            for(var i=0;i<this.addresses.length;i++){
              console.log('add',this.addresses[i]['useraddressId'] == this.purchase['useraddressId']);
              if(this.addresses[i]['useraddressId'] == this.purchase['useraddressId']){
                this.selectedindex = i;
              }
            }
          }
        }else{
            for(var i=0;i<this.addresses.length;i++){
                if(this.addresses[i]['isDefault'] == '1'){
                    this.selectedindex = i;
                }
            }
        } console.log(this.selectedindex);
    })
  }
  public addresstoedit:any;
  EditAddress(addr){
    console.log('EditAddress',addr);
    this.addresstoedit=addr;
    this.edditaddress=true;
    this.RemoveMap();
    this.addnewaddress=false;
    this.setAddressToEdits(addr);
  }
  setAddressToEdits(address){
    console.log('setAddressToEdits',address);
    this.profileedittype=address["addressType"];
    this.profileeditMobile=address["mobile"];
    this.profileeditProvince=address["province"];
    this.profileeditCity=address["city"];
    this.profileeditCountry=address["countryId"];
    this.profileeditBuilding=address["building"];
    this.profileeditpostalCode=address["postalCode"];
    this.profileeditaddressDetails=address["addressDetails"];
    if(address["addressDetails"].isDefault === 1){
        this.profileeditadefault=true;
    }else{
      this.profileeditadefault=false;
    }
} 
loadMap(){ 
  if(!this.map){
      console.log('loadmap')
      let latLng = new google.maps.LatLng(33.8547, 35.8623); 
      let mapOptions = {
      center: latLng,
      zoom: 7,
      mapTypeId: google.maps.MapTypeId.ROADMAP
      } 
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions); 
      google.maps.event.addListener(this.map, 'click', (event) => { 
          this.placeMarker(event.latLng)
      })
      this.addmarker();
      this.content.scrollToTop();
  }
  else{
      this.addmarker();
      this.content.scrollToTop();
  }
}
public gmarkers = [];
placeMarker(location) {
  console.log('placeMarker',location.lat(),location.lng())
  this.lat=location.lat();
  this.lng=location.lng();
  for(var i=0; i<this.gmarkers.length; i++){
      this.gmarkers[i].setMap(null);
  }
  let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      draggable:true,
      position: location
    });
  
  //   let content = "<h4>Information!</h4>";          
  
  //   this.addInfoWindow(marker, content);
    google.maps.event.addListener(marker,'dragend', (event) => {
      this.lat=event.latLng.lat();
      this.lng=event.latLng.lng();
    });
    this.map.setZoom(17);
    this.map.panTo(marker.position);
    this.gmarkers.push(marker)
}
addmarker(){
  console.log('addmarker',this.lat,this.lng)
  for(var i=0; i<this.gmarkers.length; i++){
      this.gmarkers[i].setMap(null);
  }
  if(this.lat && this.lng && this.lat != 0 && this.lng != 0){
      let latLng = new google.maps.LatLng(this.lat, this.lng);  
      let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          draggable:true,
          position: latLng
        });
        google.maps.event.addListener(marker,'dragend', (event) => {
          this.lat=event.latLng.lat();
          this.lng=event.latLng.lng();
        });
        this.map.setZoom(17);
        this.map.panTo(marker.position);
        this.gmarkers.push(marker)
  } 
  else{
      this.geolocation.getCurrentPosition().then((position) => {
          let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); 
          this.lat=position.coords.latitude;
          this.lng= position.coords.longitude;
          let marker = new google.maps.Marker({
              map: this.map,
              animation: google.maps.Animation.DROP,
              draggable:true,
              position: latLng
            });
          
          //   let content = "<h4>Information!</h4>";   
          //   this.addInfoWindow(marker, content);
            google.maps.event.addListener(marker,'dragend', (event) => {
              this.lat=event.latLng.lat();
              this.lng=event.latLng.lng();
            });
            this.map.setZoom(17);
            this.map.panTo(marker.position);
            this.gmarkers.push(marker)
         }).catch((error) => {
          let alert = this.alertCtrl.create({ 
              subTitle: 'Please enable ur GPS to detect location',
              buttons: [
               
                {
                  text: 'Enable',
                  handler: () => { 
                     this.openNativeSettings.open('location')
                  }
                },
                 {
                  text: 'Click on Map',
                  role: 'cancel',
                  handler: () => {
                    console.log('Cancel clicked');
                  }
                }
              ]
            });
            alert.present();
           console.log('Error getting location', error);
         })
      
  }
}
addInfoWindow(marker, content){

  let infoWindow = new google.maps.InfoWindow({
    content: content
  });

  google.maps.event.addListener(marker, 'click', () => {
    infoWindow.open(this.map, marker);
  });

}
  public selectedindex:any=-1;
  SelectAddress(index, address=null){
    console.log('SelectAddress')
    this.selectedindex=index;
    if(address && address.countryId!=123){
        this.address_country = address.countryId;
        this.currency_id = 1;
        this.setCurrency();
        
        this.paymentcard = true;
        this.paymentchangecard();
    }else{
        this.address_country = 123;
    }
  }
  GotoCart(){ 
    this.event.publish('selecttab:menu', 3);  
  }
  Search($event){
    var val="";
      if($event.target) 
        val = $event.target.value; 
    this.navCtrl.push('products', {'src':'search','value':val});
  }
  public usecode:boolean=false;
  async UpdateProfile(){  
     this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    }); 
    // this.loading.present();
    //--check inputs all fields are required
    if(this.selectedindex==-1)
    {
      if(this.loading!=""){
        this.loading.dismiss();
      }  
     let toast = this.toastCtrl.create({
       message:  "Please select an address", 
       position: 'middle',
       showCloseButton:true,
       closeButtonText:'Close'
     });
     toast.present();
    } 
    else{ 
      if(this.usecode){
        console.log('codes',this.codes)
        this.api.postpromise('RemoveCode',{
          'redeemuserId':this.codes[0]["redeemuserId"]
        })
        this.codeid=this.codes[0]["redeemuserId"];
        if(this.currency.description !="USD"){ 
          this.total =  this.total - (this.loyaltyProgramPoints.amount * this.currency.rate); 
        } else{ 
          this.total=this.total - this.loyaltyProgramPoints.amount;
        }
        this.Payment();
      } else{ 
        this.Payment();  
      }
    }
  }
  Payment(){
    console.log('this.addresses[this.selectedindex]',this.addresses[this.selectedindex])
    if(this.addresses[this.selectedindex]['countryId'] == '123'){
      //--card payment
      if(this.paymentcard){
        var bill = this.total;
         
        const getcountry = this.countriesall.find(country=> country.countryId === this.addresses[this.selectedindex]["countryId"]);
        const browser=this.iab.create('https://mouawadmbs.com/MobileApi/redirect.php?userid='+this.user["userId"]+'&useraddressId='+this.addresses[this.selectedindex]["useraddressId"]+'&bill_to_email='+this.user["emailAddress"]+'&amount='+bill+'&currency='+this.currency.description+'&bill_to_phone='+this.user["mobile"]+'&bill_to_forename='+this.user["fullName"]+'&bill_to_address_state='+this.addresses[this.selectedindex]["province"]+'&bill_to_address_city='+this.addresses[this.selectedindex]["city"]+'&bill_to_address_line1='+this.addresses[this.selectedindex]["addressDetails"]+'&building='+this.addresses[this.selectedindex]["building"]+'&floor='+this.addresses[this.selectedindex]["floor"]+'&street='+this.addresses[this.selectedindex]["street"]+'&address-type='+this.addresses[this.selectedindex]["addressType"]+'&bill_to_address_country='+getcountry["country_code"],'_blank','');
        browser.on('loadstop').subscribe(event => { 
          console.log(event)
          
          if(event.url=="https://mouawadmbs.com/MobileApi/reciept.php"){ 
            console.log(true) 
            
            browser.executeScript({code:"returnreciept()"}).then((res)=>{
                console.log('returnreciept',res); 
                browser.close();
                this.ProccessPayment(res[0]); 
            }).catch((er)=>{
                browser.executeScript({code:"returnreciept()"}).then((res)=>{
                  console.log('returnreciept',res); 
                  browser.close();
                  this.ProccessPayment(res[0]); 
              })
            }); 
          }
          else{
            browser.executeScript({code:"clicksubmitt()"}).then((res)=>{
              console.log(res);
              
            });
          }
      }); 
        
      }//--end credit payment
      else{
      // --cash on delivery
      
      var msec = Math.round(new Date().getTime()/1000); 
       
      this.ProccessPayment({
        "isPaid": "0",
        "paymentMethod": "cash",
        "priceTotal": this.total,
        "purchaseDate":"",
        "purchaseNumber": msec.toString(),
        "response": "ACCEPT",
        
      }) 
    }
    } else{
      let alert = this.alertCtrl.create({
        title: 'Your order is well received',
        message: 'We will check the delivery charges and send you a notification in a short time.',
        buttons: [
          {
            text: 'Confirm',
            handler: () => {
              var msec = Math.round(new Date().getTime()/1000); 
              this.ProccessPayment({
                "isPaid": "0",
                "paymentMethod": "pending",
                "priceTotal": this.total,
                "purchaseDate":"",
                "purchaseNumber": msec.toString(),
                "response": "pending",
                
              }) 
            }
          }
        ]
      });
      alert.present();
    }
   
  }
  public addmap:boolean=false;
  public AddMap(){
    this.addmap=true;
    this.loadMap();
    
  }
  RemoveMap(){ 
    this.addmap=false;
    this.lat=0;
    this.lng=0;
  }
  EditMap(){
    this.addmap=true;
    console.log('addres to edit',this.addresstoedit);
    this.lat=this.addresstoedit['lat'];
    this.lng=this.addresstoedit['lng'];
    this.loadMap();
  }
  DeleteAddress(addr){
    console.log('DeleteAddress',addr);
    let jsonuser=this.loggeduser;
    let alert = this.alertCtrl.create({ 
      subTitle: 'Remove address?',
      buttons: [
       
        {
          text: 'Remove',
          handler: () => { 
              this.api.postpromise("removeAddress",{
                  "useraddressId":addr["useraddressId"] 
              }).then(()=>{
                  this.addresses=[];
                  //--Get User Address
                  this.api.get('getUserAddress/'+jsonuser['userId']).then((res)=>{
                      console.log('getUserAddress',res["addresses"]) 
                      this.addresses=res["addresses"];
                      this.edditaddress=false;
                  })
              }) 

          }
        },
         {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
}
  ProccessPayment(res) { 
    console.log('ProccessPayment',res) 
    if(res["response"]=="DECLINE" || res["response"]=="ERROR"){
      if(this.loading!=""){
        this.loading.dismiss();
      }  
      //--Add User ID
      let jsonuser=this.loggeduser;
      res["userId"]=jsonuser["userId"];
      if(!res["userId"] || res["userId"]=='')
        res["userId"]=this.setting.getusersession();
        res["redeemuserId"]="";
        
        if(this.addresses[this.selectedindex]["useraddressId"]!==""){
          res["useraddressId"]=this.addresses[this.selectedindex]["useraddressId"];
        }else{
          res["useraddressId"]="";
        }
        if(this.addresses[this.selectedindex]["mapLink"]!==""){
          res["mapLink"]=this.addresses[this.selectedindex]["mapLink"];
        }else{
          res["mapLink"]="";
        } 
      //--Add Orders to res
      var orderselected=[];
      var ordersnotselected=[];
      this.setting.getcart().then((orders)=>{
        orders.forEach(element => { 
          if(element.select){
            orderselected.push(element);
          }
          else{
            ordersnotselected.push(element);
          }
        });
        res["orders"]=orderselected;
        res['currency'] = this.currency.description;  
        
        this.api.postpromise("addPurchase",res).then((res)=>{
          console.log('res',res);
          if(this.loading!=""){
            this.loading.dismiss();
          }
          if(res["success"]){
            alert('Payment Failed, Please Try again')
            // this.setting.setcart(ordersnotselected).then(()=>{ 
            //   this.event.publish('CartEditI')
            //   //--Get Cart
            //   this.setting.getcart().then((resss)=>{ 
            //     this.countcart=0;
            //     let toast = this.toastCtrl.create({
            //         message:  "Order Submitted", 
            //         position: 'middle',
            //         duration: 1500
            //     });
            //     toast.present();
            //     this.navCtrl.push('orderreceipt', {'purchaseid':res["purchase"]["id"],'code':this.codeid});
            //   });
            // });
          }
        });
      })
      
    }
    else if(res["response"]=="ACCEPT"  || res["response"]=="REVIEW"){
      //--Add User ID
      let jsonuser=this.loggeduser;
      res["userId"]=jsonuser["userId"];
      if(!res["userId"] || res["userId"]=='')
        res["userId"]=this.setting.getusersession();
      if(this.codeid!==""){
        res["redeemuserId"]=this.codeid;
      }else{
        res["redeemuserId"]="";
      }
      if(this.addresses[this.selectedindex]["useraddressId"]!==""){
        res["useraddressId"]=this.addresses[this.selectedindex]["useraddressId"];
      }else{
        res["useraddressId"]="";
      }
      if(this.addresses[this.selectedindex]["mapLink"]!==""){
        res["mapLink"]=this.addresses[this.selectedindex]["mapLink"];
      }else{
        res["mapLink"]="";
      } 
      //--Add Orders to res
      var orderselected=[];
      var ordersnotselected=[];
      this.setting.getcart().then((orders)=>{
        orders.forEach(element => { 
          if(element.select){
            orderselected.push(element);
          }
          else{
            ordersnotselected.push(element);
          }
        });
        res["orders"]=orderselected;
        res['currency'] = this.currency.description;
        this.api.postpromise("addPurchase",res).then((res)=>{
          console.log('res',res);
          if(this.loading!=""){
            this.loading.dismiss();
          }
          if(res["success"]){
            this.setting.setcart(ordersnotselected).then(()=>{ 
              this.event.publish('CartEditI')
              //--Get Cart
              this.setting.getcart().then((resss)=>{ 
                this.countcart=0;
                let toast = this.toastCtrl.create({
                    message:  "Order Submitted", 
                    position: 'middle',
                    duration: 1500
                });
                toast.present();
                this.navCtrl.push('orderreceipt', {'purchaseid':res["purchase"]["id"],'code':this.codeid});
              });
            });
          }
        });
      })
      
     
    } else if(res["response"]=="pending"){
    //--Add User ID
    let jsonuser=this.loggeduser;
    res["userId"]=jsonuser["userId"];
    if(!res["userId"] || res["userId"]=='')
      res["userId"]=this.setting.getusersession();
    if(this.codeid!==""){
      res["redeemuserId"]=this.codeid;
    }else{
      res["redeemuserId"]="";
    }
    if(this.addresses[this.selectedindex]["useraddressId"]!==""){
      res["useraddressId"]=this.addresses[this.selectedindex]["useraddressId"];
    }else{
      res["useraddressId"]="";
    }
    if(this.addresses[this.selectedindex]["mapLink"]!==""){
      res["mapLink"]=this.addresses[this.selectedindex]["mapLink"];
    }else{
      res["mapLink"]="";
    } 
    //--Add Orders to res
    var orderselected=[];
    var ordersnotselected=[];
    this.setting.getcart().then((orders)=>{
      orders.forEach(element => { 
        if(element.select){
          orderselected.push(element);
        }
        else{
          ordersnotselected.push(element);
        }
      });
      res["orders"]=orderselected;
      res['currency'] = this.currency.description;
     
      this.api.postpromise("addPurchase",res).then((res)=>{
        console.log('res',res);
        if(this.loading!=""){
          this.loading.dismiss();
        }
        if(res["success"]){
          this.setting.setcart(ordersnotselected).then(()=>{ 
            this.event.publish('CartEditI')
            //--Get Cart
            this.setting.getcart().then((resss)=>{ 
              this.countcart=0;
              //this.navCtrl.push('orderreceipt', {'purchaseid':res["purchase"]["id"],'code':this.codeid});
            });
          });
          this.event.publish('selecttab:menu', 3);
        }
      });
    })
    }
  }
  ionViewDidLeave(){
    console.log('ionViewDidLeave')
    if(this.loading){
      this.loading.dismiss();
    }
  } 
  paymentchangecard(){ 
    if(this.paymentcard)
     this.paymentcash=false
    
    
  }
  paymentchangecash(){
    if(this.paymentcash)
     this.paymentcard=false
  }
  public addnewaddress:boolean=false;
  SaveNewAddress(){
    // -- check validation
    if(this.profileaddtype == '' || this.profileaddMobile == '' ||   this.profileaddProvince =='' || this.profileaddCity == '' || this.profileaddCountry == '' || this.profileaddBuilding =='' || this.profileaddaddressDetails ==''){
     let toast = this.toastCtrl.create({
         message:  "Please enter all required fields", 
         position: 'middle',
         showCloseButton:true,
         closeButtonText:'Close'
       });
       toast.present();
   }//--end if
   else{
    let jsonuser=this.loggeduser;
     let isDefault=0;
     let deafulttochange:any="";
     if(this.profileaddadefault){  
         isDefault=1; 
         for (var i=0; i < this.addresses.length; i++) {
             console.log('add')
             if (this.addresses[i].isDefault == 1) {
                 deafulttochange= this.addresses[i];
             }
         }
     }
     let latselected=0;
     let lngselected=0;
     let maplinkselected='';
     if(this.lat && this.lng && this.lat != 0 && this.lng != 0 && this.addmap)
     {
       latselected=this.lat;
       lngselected=this.lng;
       maplinkselected='https://www.google.com/maps/place/'+this.lat+','+this.lng;
     }
       this.api.postpromise('AddAddress',
       { 
         "userId" :parseInt(jsonuser['userId']),
         "addressType":this.profileaddtype,
         "mobile":this.profileaddMobile,
         "addressDetails":this.profileaddaddressDetails,
         "countryId":this.profileaddCountry,
         "province":this.profileaddProvince,
         "city":this.profileaddCity,
         "street":"",
         "building":this.profileaddBuilding,
         "floor":"",
         "postalCode":this.profileaddpostalCode,
         "lat":latselected,
         "lng":lngselected,
         "mapLink":maplinkselected,
         "isDefault":isDefault
       }
     ).then((res)=>{
         this.addnewaddress=false;
   this.addresses=[];
  this.RemoveMap();
 if(deafulttochange!=""){
     console.log('1')
     this.api.postpromise('UpdateAddress',
     { 
       "addressId":deafulttochange["useraddressId"],
       "userId" :parseInt(jsonuser['userId']),
       "addressType":deafulttochange["addressType"],
       "mobile":deafulttochange["mobile"],
       "addressDetails":deafulttochange["addressDetails"],
       "countryId":deafulttochange["countryId"],
       "province":deafulttochange["province"],
       "city":deafulttochange["city"],
       "street":"",
       "building":deafulttochange["building"],
       "floor":"",
       "postalCode":deafulttochange["postalCode"],
       "lat":deafulttochange["lat"],
       "lng":deafulttochange["lng"],
       "mapLink":deafulttochange["mapLink"],
       "isDefault":0
     }).then(()=>{
           //--Get User Address
         this.api.get('getUserAddress/'+jsonuser['userId']).then((res)=>{
             console.log('getUserAddress',res["addresses"]) 
             this.addresses=res["addresses"];
         })
     })
     }
     else{
         this.api.get('getUserAddress/'+jsonuser['userId']).then((res)=>{
             console.log('getUserAddress',res["addresses"]) 
             this.addresses=res["addresses"];
         })
     }
  
     }).catch((er)=>{
         console.error('AddAddress',er)
     })
   } 
}
UpdateAddress(){
  console.log('UpdateAddress',this.addresstoedit)
  let jsonuser=this.loggeduser;
  let isDefault=0;
  let deafulttochange:any="";
  if(this.profileeditadefault){  
      isDefault=1; 
      for (var i=0; i < this.addresses.length; i++) {
          console.log('add',this.addresses[i].isDefault == 1 , this.addresses[i].useraddressId , this.addresstoedit["useraddressId"])
          if (this.addresses[i].isDefault == 1 && this.addresses[i].useraddressId !== this.addresstoedit["useraddressId"]) {
              deafulttochange= this.addresses[i];
          }
      }
  }
  let latselected=0;
  let lngselected=0;
  let maplinkselected='';
  if(this.lat && this.lng && this.lat != 0 && this.lng != 0 && this.addmap)
  {
    latselected=this.lat;
    lngselected=this.lng;
    maplinkselected='https://www.google.com/maps/place/'+this.lat+','+this.lng;
  }
  this.api.postpromise('UpdateAddress',
  { 
    "addressId":this.addresstoedit["useraddressId"],
    "userId" :parseInt(jsonuser['userId']),
    "addressType":this.profileedittype,
    "mobile":this.profileeditMobile,
    "addressDetails":this.profileeditaddressDetails,
    "countryId":this.profileeditCountry,
    "province":this.profileeditProvince,
    "city":this.profileeditCity,
    "street":"",
    "building":this.profileeditBuilding,
    "floor":"",
    "postalCode":this.profileeditpostalCode,
    "lat":latselected,
    "lng":lngselected,
    "mapLink":maplinkselected,
    "isDefault":isDefault
  }
).then((res)=>{
    console.log('UpdateAddress',deafulttochange);
    this.edditaddress=false;
    this.addresses=[];
    this.RemoveMap();
  if(deafulttochange!=""){
      console.log('1')
      this.api.postpromise('UpdateAddress',
      { 
        "addressId":deafulttochange["useraddressId"],
        "userId" :parseInt(jsonuser['userId']),
        "addressType":deafulttochange["addressType"],
        "mobile":deafulttochange["mobile"],
        "addressDetails":deafulttochange["addressDetails"],
        "countryId":deafulttochange["countryId"],
        "province":deafulttochange["province"],
        "city":deafulttochange["city"],
        "street":"",
        "building":deafulttochange["building"],
        "floor":"",
        "postalCode":deafulttochange["postalCode"],
        "lat":deafulttochange["lat"],
        "lng":deafulttochange["lng"],
        "mapLink":deafulttochange["mapLink"],
        "isDefault":0
      }).then(()=>{
            //--Get User Address
          this.api.get('getUserAddress/'+jsonuser['userId']).then((res)=>{
              console.log('getUserAddress',res["addresses"]) 
              this.addresses=res["addresses"];
          })
      })
  }
  else{
      this.api.get('getUserAddress/'+jsonuser['userId']).then((res)=>{
          console.log('getUserAddress',res["addresses"]) 
          this.addresses=res["addresses"];
      })
  }
   
}).catch((er)=>{
    console.error('UpdateAddress',er)
})
}
EditAddressCancel(){
  this.edditaddress=false;
}
AddNewAddress(){
  this.addnewaddress=true;
  this.edditaddress=false;
  this.RemoveMap();
}
DontAddNewAddress(){
  this.addnewaddress=false; 
}
 
public codes:any=[];
RedeemPoint(userId){
  console.log('RedeemPoint',userId) 
    this.api.get('getuser/'+userId).then((res)=>{
      console.log('Get',res)
      if(res["user"]["userPoints"]>=this.loyaltyProgramPoints.points){
        this.api.get('generateUserCode/'+userId).then((rescode)=>{
          console.log('generateUserCode',rescode)
          //--update user point (Decrease 1000)
          this.api.postpromise('updateuser',
          {  
            "userid" :res["user"]['userId'],
            "fullName":res["user"]['fullName'],
            "gender":res["user"]['gender'],
            "dateBirthday":res["user"]['dateBirthday'], 
            "emailAddress":res["user"]['emailAddress'], 
            "mobile":res["user"]['mobile'], 
            "occupation":res["user"]['occupation'],
            "financeNb":res["user"]['financeNb'],
            "userPoints": parseInt(res["user"]['userPoints']) - this.loyaltyProgramPoints.points
          }).then((res)=>{
            if(res["success"]){ 
              var arrayuser=[];
              arrayuser.push(res["user"])
              this.User.loggedinuser.next(arrayuser);
              if(localStorage.getItem('userid')){
                localStorage.setItem('userid',JSON.stringify(res["user"]))
              }
            }
          })
        })
        this.codes=[]
        //--Get User Codes
        this.api.get('getUserCodes/'+userId).then((codes)=>{
          if(codes["codes"].length>0){
            for(var c=0;c<codes["codes"].length;c++){
              this.codes.push(codes["codes"][c])
            }
            console.log('codes',this.codes)
          }
        
        })
      
      }
      else{
        this.codes=[]
        //--Get User Codes
        this.api.get('getUserCodes/'+userId).then((codes)=>{
          console.log('getUserCodes',codes)
          if(codes["codes"].length>0){
            for(var c=0;c<codes["codes"].length;c++){
              if(codes["codes"][c]["isActive"]!=0)
                this.codes.push(codes["codes"][c])
            }
            console.log('codes',this.codes)
          }
         
        }) 
      }
      
})
}
  setCurrency(){
    this.homeprovider.currency = this.currencies.find(currency=>currency.id==this.currency_id);
    var oldrate = this.currency.rate ;
    this.currency = this.homeprovider.currency; 
    console.log(this.currency)
   // Recalculate
   if(this.currency.description != "USD"){ 
    this.total = this.currency.rate * this.total;
    this.totalintial = this.currency.rate * this.totalintial;
    this.deliveryCharge  = this.currency.rate * this.deliveryCharge;
  } else { 
    this.total = this.total / oldrate ;
    this.totalintial =  this.totalintial /  oldrate;
    this.deliveryCharge  = this.deliveryCharge  /  oldrate;

  }   
    if(this.currency.id==1){
        this.paymentcash = true;
        this.paymentchangecash();
    }else{
        this.paymentcard = true;
        this.paymentchangecard();
    }
    
  }
}
