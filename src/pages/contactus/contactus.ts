import { User } from './../../providers/user/user';
import { Api } from './../../providers/api/api';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, Events, ModalController, AlertController } from 'ionic-angular';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Settings } from '../../providers';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';

/**
 * Generated class for the ContactusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contactus',
  templateUrl: 'contactus.html',
})
export class ContactusPage {
  public fullName:any="";
  public emailAddress:any="";
  public mobile:any="";
  public request:any="";
  public loading:any="";
  public countcart:any=0;
  public username="";
  public currency:any={};
  public currencies:any=[];
  public currency_id=1;
  constructor(public homeprovider:HomeproviderProvider,public navCtrl: NavController, public navParams: NavParams,public api:Api,public loadingCtrl:LoadingController,public toastCtrl:ToastController,public modalCtrl: ModalController,public settings:Settings,public user:User,public event:Events,public alertCtrl:AlertController) {
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
    //--Get Cart
      this.settings.getcart().then((res)=>{
        console.log('GetCart',res)
        if(res && res.length>0)
          this.countcart=res.length;
      })
      if(this.user._user){
        this.username=this.user._user["fullName"]
      }
      this.event.unsubscribe('CartEdit');
      this.event.subscribe('CartEdit',() => {
          //--Get Cart
        this.settings.getcart().then((res)=>{
          console.log('GetCart',res)
          if(res && res.length>0)
            this.countcart=res.length;
        })
      });
  }
  Gotolocateus(){
    this.navCtrl.push('LocateUsPage')
  }
  ionViewWillLeave(){ 
    this.homeprovider.currentPage = 'home'; 
    }
  ionViewWillEnter(){ 
  this.homeprovider.currentPage = 'other'; 
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
    
    this.event.unsubscribe('CartEdit');
    this.event.subscribe('CartEdit',() => {
      console.log('CartEdit Home') 
      //--Get Cart
      this.settings.getcart().then((res)=>{
        console.log('GetCart',res)
        if(res && res.length>0)
          this.countcart=res.length;
      })
    });
  }
  GotoCart(){ 
    this.event.publish('selecttab:menu', 3);  
  }
  Search($event){
    var val="";
    if($event.target) 
      val = $event.target.value; 
    this.navCtrl.push('products', {'src':'search','value':val});
    // if(this.user._user){
    //   var val="";
    //   if($event.target) 
    //     val = $event.target.value; 
    // this.navCtrl.push('products', {'src':'search','value':val});
    // } else{
    //   let alert = this.alertCtrl.create({ 
    //     subTitle: 'Please login first',
    //     buttons: [{
    //       text: 'Go to Login',
    //       handler: () => { 
    //         this.navCtrl.parent.select(2);
    //       }
    //     }]
    //   });
    //   alert.present();
    // }
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactusPage');
  }
  SendRequest(){ 
    console.log(this.fullName,this.emailAddress,this.mobile,this.request)
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    }); 
    this.loading.present();
    if(this.fullName!="" && this.emailAddress!="" && this.mobile!="" && this.request!=""){ 
      let header=new HttpHeaders(); 
      let reqOpts;
      reqOpts = {
        headers:header,
        params: new HttpParams()
       };
      this.api.post('sendmsg',{
        "fullName":this.fullName,
        "emailAddress":this.emailAddress,
        "mobile":this.mobile,
        "request":this.request
      },reqOpts).share().subscribe((res: any) => { 
         if(res["success"]){
          if(this.loading!=""){
            this.loading.dismiss();
          } 
           let toast = this.toastCtrl.create({
            message:  res["message"], 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
         }
         else{
          if(this.loading!=""){
            this.loading.dismiss();
          } 
           let toast = this.toastCtrl.create({
            message:  res["message"],
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
         }
      }, err => {
        if(this.loading!=""){
          this.loading.dismiss();
        } 
         let toast = this.toastCtrl.create({
          message:  "Error", 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close'
        });
        toast.present();
      }); 
    }
    else{
      if(this.loading!=""){
        this.loading.dismiss();
      } 
       let toast = this.toastCtrl.create({
        message:  "All fields are mandatory", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
    }
  }
  ionViewDidLeave(){
    console.log('ionViewDidLeave')
    if(this.loading!=""){
      this.loading.dismiss();
    }
  } 
  setCurrency(){
    this.homeprovider.currency = this.currencies.find(currency=>currency.id==this.currency_id);
    this.currency = this.homeprovider.currency;
  }

}
