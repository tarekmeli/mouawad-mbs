import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShippinggiftPage } from './shippinggift';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ShippinggiftPage,
  ],
  imports: [
    IonicPageModule.forChild(ShippinggiftPage),
    PipesModule
  ],
})
export class ShippinggiftPageModule {}
