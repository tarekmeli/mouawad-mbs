import { Api } from './../../providers/api/api';
import { Settings } from './../../providers/settings/settings';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events, ToastController, LoadingController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';

/**
 * Generated class for the ShippinggiftPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name:"shippinggift"
})
@Component({
  selector: 'page-shippinggift',
  templateUrl: 'shippinggift.html',
})
export class ShippinggiftPage {
  public friname:any="";
  public friemail:any="";
  public profilefullname:any="";
  public profileMobile:any="";
  public profilehome:any="";
  public profilehomeline:any="";
  public profileProvince:any="";
  public profileCity:any="";
  public username:any="";
  public countcart=0;
  public total=0;
  public user:any;
  public loading:any;
  public paymentcard=true;
  public paymentcash=false;
  public currency:any={};
  constructor(public homeprovider:HomeproviderProvider, public navCtrl: NavController, public navParams: NavParams,public setting:Settings,public alertCtrl:AlertController,public api:Api,public event:Events,public toastCtrl:ToastController,public loadingCtrl:LoadingController,public iab:InAppBrowser) {
    //get logged in user
    this.user =this.setting.getuser();
    //get total
    this.total=this.navParams.get('total')
    this.currency = this.homeprovider.currencies[1];
    console.log('user',this.user)
    if(this.user){
      //--Get Cart
      this.setting.getcart().then((res)=>{
        console.log('GetCart',res)
        if(res && res.length>0)
          this.countcart=res.length;
      })
      this.event.unsubscribe('CartEdit');
      this.event.subscribe('CartEdit',() => {
          //--Get Cart
        this.setting.getcart().then((res)=>{
          console.log('GetCart',res)
          if(res && res.length>0)
            this.countcart=res.length;
        })
      });
      let jsonuser=JSON.parse(this.user);
      this.username=jsonuser["fullName"];
      this.api.get('getuser/'+jsonuser["userId"]).then((res)=>{
        console.log('Get',res)
        if(res["success"]){
          this.profilefullname=res["user"]["fullName"];
          this.profileMobile=res["user"]["mobile"];
          this.profilehome=res["user"]["homeAddress"];
          this.profilehomeline=res["user"]["homeLandline"];
          this.profileProvince=res["user"]["province"];
          this.profileCity=res["user"]["city"];
        }
      })
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShippingPage'); 
  }
  GotoCart(){ 
    this.event.publish('selecttab:menu', 3);  
  }
  Search($event){
    var val="";
      if($event.target) 
        val = $event.target.value; 
    this.navCtrl.push('products', {'src':'search','value':val});
  }
  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.trim());
  }
  UpdateProfile(){  
    console.log('UpdateProfile',this.reciver)
    // if(this.friname=="" || this.friemail==""){
    //   let toast = this.toastCtrl.create({
    //     message:  "Please enter your friend detials", 
    //     position: 'middle',
    //     showCloseButton:true,
    //     closeButtonText:'Close'
    //   });
    //   toast.present();
    // }
    // else if(!this.validateEmail(this.friemail)){
    //   let toast = this.toastCtrl.create({
    //     message:  "Please enter an valid email", 
    //     position: 'middle',
    //     showCloseButton:true,
    //     closeButtonText:'Close'
    //   });
    //   toast.present();
    // }
    // else{
    //   let jsonuser=JSON.parse(this.user);
    //   this.loading = this.loadingCtrl.create({
    //     content: 'Please wait...'
    //   }); 
    //   this.loading.present();
    //   this.api.postpromise('updateuser',
    //   { 
    //     "userid" :jsonuser['userId'],
    //     "fullName":this.profilefullname,
    //     "gender":jsonuser["gender"], 
    //     "emailAddress":jsonuser["emailAddress"], 
    //     "mobile":this.profileMobile, 
    //     "occupation":jsonuser["occupation"],
    //     "homeAddress":this.profilehome,
    //     "homeLandline":this.profilehomeline,
    //     "workAddress":jsonuser["workAddress"],
    //     "workLandline":jsonuser["workLandline"],
    //     "additionalAddress":jsonuser["additionalAddress"],
    //     "additionalLandline":jsonuser["additionalLandline"],
    //     "province":this.profileProvince,
    //     "city":this.profileCity 
                  
    //   }
    // ).then((res)=>{
    //   if(this.paymentcard){
    //     if(this.loading!=""){
    //       this.loading.dismiss();
    //     }
    //     this.ProccessPayment({
    //       "isPaid": "0",
    //       "paymentMethod": "card",
    //       "priceTotal": "25",
    //       "purchaseDate": "2019-09-15 11:34:57",
    //       "purchaseNumber": "6641923100688",
    //       "response": "REVIEW"
    //     }) 
    //   if(res["success"]){ 
    //     console.log(res)
    //     const browser=this.iab.create('https://mouawadmbs.com/MobileApi/redirectgift.php?userid='+res["user"]["userId"]+'&bill_to_email='+res["user"]["emailAddress"]+'&bill_to_forename='+res["user"]["fullName"]+'&bill_to_phone='+res["user"]["mobile"]+'&bill_to_address_line1='+res["user"]["homeLandline"]+'&bill_to_address_state='+res["user"]["homeAddress"]+'&bill_to_address_city='+res["user"]["homeAddress"]+'&landline-home='+res["user"]["homeLandline"]+'&amount='+this.total+'&email='+res["user"]["emailAddress"]+'','_blank','');
    //     browser.on('loadstop').subscribe(event => { 
    //       console.log(event)
    //       //browser.executeScript({ code: 'loadpayment(\''+jsonstring+ '\')' });
          
    //       if(event.url=="https://mouawadmbs.com/MobileApi/recieptgift.php"){ 
    //         console.log(true)
    //         browser.executeScript({code:"returnreciept()"}).then((res)=>{
    //             console.log('returnreciept',res);
    //             this.ProccessPayment(res[0]); 
               
    //         }); 
    //       }
    //       else{
    //         browser.executeScript({code:"clicksubmitt()"}).then((res)=>{
    //           console.log(res);
              
    //         });
    //       }
    //   }); 
    //   } 
      
        
    //   }
    //   else{
    //     if(this.loading!=""){
    //       this.loading.dismiss();
    //     } 
    //     this.ProccessPayment({
    //       "isPaid": "0",
    //       "paymentMethod": "cash",
    //       "priceTotal": this.total,
    //       "purchaseDate":"",
    //       "purchaseNumber": new Date().toUTCString(),
    //       "response": "REVIEW",
    //     }) 
      
    //   }
    // }).catch((er)=>{
    //   if(this.loading!=""){
    //     this.loading.dismiss();
    //   }
    //   let toast = this.toastCtrl.create({
    //     message:  "Something went wrong,please try again later", 
    //     position: 'middle',
    //     showCloseButton:true,
    //     closeButtonText:'Close'
    //   });
    //   toast.present();
    // })
    // }
  }
  ProccessPayment(res) { 
    if(res["response"]=="DECLINE" || res["response"]=="ERROR"){
      let toast = this.toastCtrl.create({
        message:  "Payment was rejected,Please try again", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
    }
    else if(res["response"]=="ACCEPT" || res["response"]=="REVIEW"){
       //--Add User ID
       let jsonuser=JSON.parse(this.user);
       res["userId"]=jsonuser["userId"];
      
      if(this.reciver !='friend'){
        res["friendEmail"]=jsonuser["emailAddress"];
        res["friendName"]=jsonuser["fullName"];
      }
      else{
        res["friendEmail"]=this.friemail;
        res["friendName"]=this.friname;
      }
      res["amountUsd"] = this.total;
      //Save Purchase and Cart
      this.api.postpromise("AddAnGift",res).then((res)=>{
        let toast = this.toastCtrl.create({
          message:  "Your gift card is sent to your friend", 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close'
        });
        toast.present();
       
      })
    }
  }
  ionViewDidLeave(){
    console.log('ionViewDidLeave')
    if(this.loading!=""){
      this.loading.dismiss();
    }
  } 
  SelectAddress(value){
    console.log('SelectAddress',value)
  }
  public reciver:any;

  paymentchangecard(){ 
    if(this.paymentcard)
     this.paymentcash=false
    
    
  }
  paymentchangecash(){
    if(this.paymentcash)
     this.paymentcard=false
  }

  Proceed(){
    if(this.reciver){ 
      console.log('Proceed()',this.reciver,this.total);
      var addres="";
      var userjson;
       if(this.reciver =='friend'){
        if(this.friname=="" || this.friemail==""){
          let toast = this.toastCtrl.create({
            message:  "Please enter your friend detials", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
          return;
        }
        else if(!this.validateEmail(this.friemail)){
          let toast = this.toastCtrl.create({
            message:  "Please enter an valid email", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
        }
        else{
          addres="";
          userjson=JSON.parse(this.user)

          console.log('userjson',userjson)
          const browser=this.iab.create('https://mouawadmbs.com/MobileApi/redirect.php?userid='+userjson["userId"]+'&useraddressId=&bill_to_email='+userjson["emailAddress"]+'&amount='+this.total+'&currency='+this.currency.description+'&bill_to_phone=&bill_to_forename='+userjson["fullName"]+'&bill_to_address_state=&bill_to_address_city=&bill_to_address_line1=&building=&floor=&street=&address-type=&bill_to_address_country=','_blank','');
          browser.on('loadstop').subscribe(event => { 
            console.log(event)
            //browser.executeScript({ code: 'loadpayment(\''+jsonstring+ '\')' });
            
            if(event.url=="https://mouawadmbs.com/MobileApi/reciept.php"){ 
              console.log(true)
              browser.executeScript({code:"returnreciept()"}).then((res)=>{
                  console.log('returnreciept',res); 
                  browser.close();
                  this.ProccessPayment(res[0]); 
              }); 
            }
            else{
              browser.executeScript({code:"clicksubmitt()"}).then((res)=>{
                console.log(res);
                
              });
            }
        }); 
        }
       }
       else{
        addres="";
        userjson=JSON.parse(this.user)

        console.log('userjson',userjson,this.total)
        const browser=this.iab.create('https://mouawadmbs.com/MobileApi/redirect.php?userid='+userjson["userId"]+'&useraddressId=&bill_to_email='+userjson["emailAddress"]+'&amount='+this.total+'&currency='+this.currency.description+'&bill_to_phone=&bill_to_forename='+userjson["fullName"]+'&bill_to_address_state=&bill_to_address_city=&bill_to_address_line1=&building=&floor=&street=&address-type=&bill_to_address_country=','_blank','');
        browser.on('loadstop').subscribe(event => { 
          console.log(event)
          //browser.executeScript({ code: 'loadpayment(\''+jsonstring+ '\')' });
          
          if(event.url=="https://mouawadmbs.com/MobileApi/reciept.php"){ 
            console.log(true)
            browser.executeScript({code:"returnreciept()"}).then((res)=>{
                console.log('returnreciept',res); 
                browser.close();
                this.ProccessPayment(res[0]); 
            }); 
          }
          else{
            browser.executeScript({code:"clicksubmitt()"}).then((res)=>{
              console.log(res);
              
            });
          }
      }); 
       }
    }
  }

}
