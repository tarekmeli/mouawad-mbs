import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductdetialsPage } from './productdetials';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ProductdetialsPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductdetialsPage),
    PipesModule

  ],
})
export class ProductdetialsPageModule {}
