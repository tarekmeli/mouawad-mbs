import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Events, ModalController, Slides, AlertController } from 'ionic-angular';
import { Api, Settings, User } from '../../providers';
import { GiftPage } from '../gift/gift';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the ProductdetialsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name:'productsdetials'
})
@Component({
  selector: 'page-productdetials',
  templateUrl: 'productdetials.html',
})
export class ProductdetialsPage {
  public product:any;
  public loading:any;
  public selectedsize=0;
  public selectedcolor=0;
  public selectedquantity=1;
  public countcart:any=0;
  public username="";
  public colors:any=[];
  public slidesimages=[];
  public productsizemerg:any=[];
  public currency:any={};
  public currencies:any=[];
  public currency_id=1;
  public users:any=''
  @ViewChild(Slides) slides: Slides;
  constructor(public homeprovider:HomeproviderProvider,public navCtrl: NavController, public navParams: NavParams,public api:Api,public event:Events,public loadingCtrl:LoadingController,public toastCtrl:ToastController,public modalCtrl: ModalController,public settings:Settings,public user:User, public alertCtrl:AlertController,public socialSharing: SocialSharing){
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    }); 
    this.loading.present();
    this.api.get('products/'+this.navParams.get('productid')).then((res)=>{
      console.log('Search',res)
      if(res["success"])
      {
        if(this.loading!=""){
          this.loading.dismiss();
        } 
        var getuser=this.settings.gettempuser();
        if(getuser){
          this.users = JSON.parse(getuser);
          console.log('user',this.user)
        }
        if(res["product"]){
          this.product=res["product"];
          
          if(this.product["productsizes"].length>0 && this.product["productsizesdetials"].length>0)
          {
            for(var i=0;i<this.product["productsizesdetials"].length;i++){
              for(var j=0;j<this.product["productsizes"].length;j++)
              {
                if(this.product["productsizesdetials"][i].sizeId == this.product["productsizes"][j].sizeId)
                {  
                  var price=Number(this.product["productsizes"][j].productSizePrice).toFixed(2); 
                  var sprice=Number(this.product["productsizes"][j].productSizePriceSupplier).toFixed(2); 
                  this.productsizemerg.push({
                    'sizeId':this.product["productsizesdetials"][i].sizeId,
                    'sizeName':this.product["productsizesdetials"][i].sizeName,
                    'productSizeId':this.product["productsizes"][j].productSizeId,
                    'productId':this.product["productsizes"][j].productId,
                    'productSizePrice':price,
                    'productSizeReference':this.product["productsizes"][j].productSizeReference,
                    'productSizePriceSupplier':sprice
                  })
                }
              }
            }
            
          }
          this.colors=[];
          // console.log('color condition',this.product["colors"],this.productsizemerg)
          if(this.product["colors"] && this.product["colors"].length>0 && this.productsizemerg.length>0){
            for(var c=0;c<this.product["colors"].length;c++){
              // console.log('color',c,this.product["colors"][c]["sizeId"]==this.productsizemerg[this.selectedsize]["sizeId"])
              if(this.product["colors"][c]["sizeId"]==this.productsizemerg[this.selectedsize]["sizeId"]){
                this.colors.push(this.product["colors"][c]);
              }
            }
          } 
          if(this.product["colors"] && this.product["colors"].length>0 && this.productsizemerg.length==0){
            for(var c=0;c<this.product["colors"].length;c++){
              //console.log('color',c,this.product["colors"][c]["sizeId"]==this.productsizemerg[this.selectedsize]["sizeId"])
              //if(this.product["colors"][c]["sizeId"]==this.productsizemerg[this.selectedsize]["sizeId"]){
                this.colors.push(this.product["colors"][c]);
              //}
            }
          } 
          //--get slide images
          this.slidesimages=[];
          console.log('this.product',this.product['productdetials'])
          if(this.product && this.product['productdetials']){
            this.slidesimages.push(this.product['productdetials'].productMainImage);
            if(this.product['images'])
              for(var i=0;i<this.product['images'].length;i++){
                this.slidesimages.push(this.product['images'][i]);
              }
          }
          if(this.product['productsizes']){
            for(var i=0;i<this.product['productsizes'].length;i++){
              this.slidesimages.push(this.product['productsizes'][i]);
            }
          }
         
         
          this.CalculatePrice();
        }
        else
        {
          let toast = this.toastCtrl.create({
            message:  "No product matching you search were found", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
        }
      } 
      else{
        if(this.loading!=""){
          this.loading.dismiss();
        } 
         let toast = this.toastCtrl.create({
          message:  "No product matching you search were found", 
          position: 'middle',
          showCloseButton:true,
          closeButtonText:'Close'
        });
        toast.present();
      }
    }).catch((er)=>{
      if(this.loading!=""){
        this.loading.dismiss();
      } 
       let toast = this.toastCtrl.create({
        message:  "Data not retrieved.Please try again later.", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
    })
    //--Get Cart
   this.settings.getcart().then((res)=>{
    console.log('GetCart',res)
    if(res && res.length>0)
      this.countcart=res.length;
  })
  this.event.unsubscribe('CartEdit');
  this.event.subscribe('CartEdit',() => {
      //--Get Cart
    this.settings.getcart().then((res)=>{
      console.log('GetCart',res)
      if(res && res.length>0)
        this.countcart=res.length;
    })
  });
 
  }
  ionViewWillEnter(){  
    this.homeprovider.currentPage = 'other'; 
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
    var getuser=this.settings.gettempuser();
    if(getuser){
      this.users = JSON.parse(getuser);
      console.log('user',this.user)
    }
  }
 GotoBrands(){
    this.navCtrl.push('brands');
  }
  GotoProductnew(){
    this.navCtrl.push('products', {'src':'newarrival'});
  }
  GotoProductpro(){
    this.navCtrl.push('products', {'src':'promotions'});
  }
  GotoProductbest(){
    this.navCtrl.push('products', {'src':'bestsellers'});
  }
  OpenModal(){
    let giftCardModal = this.modalCtrl.create(GiftPage);
    giftCardModal.present();
  }
  GotoCart(){ 
    this.event.publish('selecttab:menu', 3);  
  }
  PayNow(){ 
    this.event.publish('selecttab:menu', 3); 
    // var userl=this.settings.getusersession();
    // if(this.colors.length>0 && this.productsizemerg.length>0 )
    // {
    //     console.log(1,this.colors);
    //     var price=0;
    //     //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
    //     if(this.product['productdetials'].productDiscount>0){
    //       price=this.productsizemerg[this.selectedsize]["productSizePrice"]-(this.productsizemerg[this.selectedsize]["productSizePrice"]*(this.product['productdetials'].productDiscount/100))
    //     }
    //     else{
    //       price=this.productsizemerg[this.selectedsize]["productSizePrice"]
    //     }
    //     //--Add Product to cart DB
    //     this.api.postpromise('AddProductToCart',
    //     {
    //       "userId": userl,
    //       "productId": this.product["productdetials"]["productId"],
    //       "productsizeId": this.productsizemerg[this.selectedsize]["productSizeId"],
    //       "productcolorId": this.colors[this.selectedcolor]["productColorId"],
    //       "quantity": this.selectedquantity,
    //       "unitPrice": price
    //    }).then((res)=>{
    //     this.productTocart={
    //       'productselecteddetials':this.product["productdetials"],
    //       'productselectedcolor':[this.colors[this.selectedcolor]],
    //       'productselectedsize':[this.productsizemerg[this.selectedsize]],
    //       'productselectedquantity':this.selectedquantity,
    //       'productselectedpriceLL':this.priceLL,
    //       'productselectedpricedollars':this.pricedollars
    //     } 
    //    })
    // }
    // else if(this.colors.length>0 && this.productsizemerg.length==0)
    // {
    //   console.log(2)
    //   var price=0;
    //   //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
    //   if(this.product['productdetials'].productDiscount>0){
    //     price=this.product["productdetials"]["productPrice"]-(this.product["productdetials"]["productPrice"]*(this.product['productdetials'].productDiscount/100))
    //   }
    //   else{
    //     price=this.product["productdetials"]["productPrice"]
    //   }
    //      //--Add Product to cart DB
    //      this.api.postpromise('AddProductToCart',
    //      {
    //        "userId": userl,
    //        "productId": this.product["productdetials"]["productId"],
    //        "productcolorId": this.colors[this.selectedcolor]["productColorId"],
    //        "quantity": this.selectedquantity,
    //        "unitPrice": price
    //     }).then((res)=>{
    //       this.productTocart={
    //         'productselecteddetials':this.product["productdetials"],
    //         'productselectedcolor':[this.colors[this.selectedcolor]],
    //         'productselectedsize':[],
    //         'productselectedquantity':this.selectedquantity,
    //         'productselectedpriceLL':this.priceLL,
    //         'productselectedpricedollars':this.pricedollars
    //       }
    //     })
    // }
    // else if(this.productsizemerg.length>0 && this.colors.length==0)
    // {
    //   console.log(3)
    //   var price=0;
    //   //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
    //   if(this.product['productdetials'].productDiscount>0){
    //     price=this.productsizemerg[this.selectedsize]["productSizePrice"]-(this.productsizemerg[this.selectedsize]["productSizePrice"]*(this.product['productdetials'].productDiscount/100))
    //   }
    //   else{
    //     price=this.productsizemerg[this.selectedsize]["productSizePrice"]
    //   }
    //     //--Add Product to cart DB
    //     this.api.postpromise('AddProductToCart',
    //     {
    //       "userId": userl,
    //       "productId": this.product["productdetials"]["productId"],
    //       "productsizeId": this.productsizemerg[this.selectedsize]["productSizeId"], 
    //       "quantity": this.selectedquantity,
    //       "unitPrice": price
    //    }).then((res)=>{
    //     this.productTocart={
    //       'productselecteddetials':this.product["productdetials"],
    //       'productselectedcolor':[],
    //       'productselectedsize':[this.productsizemerg[this.selectedsize]],
    //       'productselectedquantity':this.selectedquantity,
    //       'productselectedpriceLL':this.priceLL,
    //       'productselectedpricedollars':this.pricedollars
    //     }
    //    })
    // }
    // else{
    //   console.log(4,this.product["productdetials"])
    //   var price=0;
    //   //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
    //   if(this.product['productdetials'].productDiscount>0){
    //     price=this.product["productdetials"]["productPrice"]-(this.product["productdetials"]["productPrice"]*(this.product['productdetials'].productDiscount/100))
    //   }
    //   else{
    //     price=this.product["productdetials"]["productPrice"]
    //   }
    //    //--Add Product to cart DB
    //    this.api.postpromise('AddProductToCart',
    //    {
    //      "userId": userl,
    //      "productId": this.product["productdetials"]["productId"], 
    //      "quantity": this.selectedquantity,
    //      "unitPrice": price
    //   }).then((res)=>{
    //     this.productTocart={
    //       'productselecteddetials':this.product["productdetials"],
    //       'productselectedcolor':[],
    //       'productselectedsize':[],
    //       'productselectedquantity':this.selectedquantity,
    //       'productselectedpriceLL':this.priceLL,
    //       'productselectedpricedollars':this.pricedollars
    //     } 
    //   })
    // }
   
  
    // this.settings.getcart().then((res)=>{
    //   console.log(res)
    //   if(!res || (res && res.length==0)){
    //     this.settings.setcart([this.productTocart]).then((res)=>{
    //       console.log('AddToCart',res)
    //       this.event.publish('CartEditI')
    //       //--Get Cart
    //       this.settings.getcart().then((res)=>{
    //         console.log('GetCart',res)
    //         if(res && res.length>0)
    //           this.countcart=res.length;
    //       })
    //       let toast = this.toastCtrl.create({
    //         message:  "Product Added to Cart", 
    //         position: 'middle',
    //         duration: 1500
    //       });
    //       toast.present();
    //       toast.onWillDismiss(() => {
    //         console.log('Dismissed toast');
    //         this.selectedquantity=1;
    //         this.selectedsize=0;
    //         this.selectedcolor=0;
    //         this.CalculatePrice();
    //         this.event.publish('selecttab:menu', 3,'selectlatest'); 
    //       }); 
    //     })
    //   }
    //   else{
    //     res.push(this.productTocart);
    //     this.settings.setcart(res).then((res)=>{
    //       console.log('AddToCart',res)
    //       this.event.publish('CartEditI')
    //       //--Get Cart
    //       this.settings.getcart().then((res)=>{
    //         console.log('GetCart',res)
    //         if(res && res.length>0)
    //           this.countcart=res.length;
    //       })
    //       let toast = this.toastCtrl.create({
    //         message:  "Product Added to Cart", 
    //         position: 'middle',
    //         duration: 1500
    //       });
    //       toast.present();
    //       toast.onWillDismiss(() => {
    //         console.log('Dismissed toast');
    //         this.selectedquantity=1;
    //         this.selectedsize=0;
    //         this.selectedcolor=0;
    //         this.CalculatePrice();
    //         this.event.publish('selecttab:menu', 3,'selectlatest'); 
    //       }); 
    //     })
    //   }
    // }) 
  }
  Search($event){
    var val="";
      if($event.target) 
        val = $event.target.value; 
    this.navCtrl.push('products', {'src':'search','value':val});
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductdetialsPage');
  
  }
  ionViewDidLeave(){
    console.log('ionViewDidLeave')
    if(this.loading!=""){
      this.loading.dismiss();
    }
    this.event.publish('CartEdit')
  } 
  SelectSize(index,id){
    //console.log(index,id);
    this.selectedsize=index;
    this.CalculatePrice();
    this.colors =[];
    // console.log('color',this.product["colors"],this.productsizemerg)
    if(this.product["colors"] && this.product["colors"].length>0 && this.productsizemerg.length>0){
      for(var c=0;c<this.product["colors"].length;c++){
        //console.log('color',this.product["colors"][c],this.productsizemerg[this.selectedsize]["sizeId"])
        if(this.product["colors"][c]["sizeId"]==this.productsizemerg[this.selectedsize]["sizeId"]){
          this.colors.push(this.product["colors"][c]);
        }
      }
    }
    //--search images 
    console.log('size',this.productsizemerg[this.selectedsize])
    index = this.slidesimages.findIndex(x => x.productSizeId === this.productsizemerg[this.selectedsize]["productSizeId"]);
    console.log('index',index);
    if(index>-1){
      this.slides.slideTo(index, 500); 
    }
  }
  getslideindex(productSizeId) { 
    if(this.slidesimages["productSizeId"])
      return this.slidesimages["productSizeId"] === productSizeId;
  }
  SelectColor(index,id){
    console.log(index,id);
    this.selectedcolor=index;
  }
  Subtractquantity(){
    console.log('Sub')
    if(this.selectedquantity>1){
      this.selectedquantity--;
    }
    this.CalculatePrice();
  }
  Addquantity(){ 
    console.log('Add')
    this.selectedquantity++; 
    this.CalculatePrice();
  }
  public productTocart:any={};
  public price:any=0;
  public priceLL:any=0;
  public pricedollars:any=0;
  CalculatePrice(){
     //-- Get user if suplier
     var getuser=this.settings.gettempuser();
     if(getuser){
       var user = JSON.parse(getuser); 
       if(user.is_Supplier == '1'){
        if(this.product && this.product["productdetials"])
        {
          if(this.product["productsizesdetials"].length>0)
          {
            this.price=this.productsizemerg[this.selectedsize].productSizePriceSupplier * this.selectedquantity; 
          }
          else{
            this.price=this.product["productdetials"].productPriceSupplier * this.selectedquantity;
          } 
          if(this.product["productdetials"].productDiscount>0){
            this.price= this.price-(this.price * (this.product["productdetials"].productDiscount/100))
          }
          this.priceLL=this.price*1500;
          this.pricedollars=(this.price).toFixed(2); 
        }
       } else{
        if(this.product && this.product["productdetials"])
        {
          if(this.product["productsizesdetials"].length>0)
          {
            this.price=this.productsizemerg[this.selectedsize].productSizePrice * this.selectedquantity; 
          }
          else{
            this.price=this.product["productdetials"].productPrice * this.selectedquantity;
          } 
          if(this.product["productdetials"].productDiscount>0){
            this.price= this.price-(this.price * (this.product["productdetials"].productDiscount/100))
          }
          this.priceLL=this.price*1500;
          this.pricedollars=(this.price).toFixed(2);
          console.log('Price',this.price,this.priceLL,this.pricedollars)
        }
       }
     } else{
      if(this.product && this.product["productdetials"])
      {
        if(this.product["productsizesdetials"].length>0)
        {
          this.price=this.productsizemerg[this.selectedsize].productSizePrice * this.selectedquantity; 
        }
        else{
          this.price=this.product["productdetials"].productPrice * this.selectedquantity;
        } 
        if(this.product["productdetials"].productDiscount>0){
          this.price= this.price-(this.price * (this.product["productdetials"].productDiscount/100))
        }
        this.priceLL=this.price*1500;
        this.pricedollars=(this.price).toFixed(2);
        console.log('Price',this.price,this.priceLL,this.pricedollars)
      }
     }
  
  }
  AddToCart(){ 
    var userl=this.settings.getusersession();
    console.log(typeof userl,userl)
    if(userl == null){
      let alert = this.alertCtrl.create({ 
        subTitle: 'Please login first',
        buttons: [{
          text: 'Go to Login',
          handler: () => { 
            this.event.publish('selecttab:menu', 2); 
          }
        }]
      });
      alert.present();
    } else{
      if(this.colors.length>0 && this.productsizemerg.length>0 )
      {
          console.log(1,this.colors);
          var price=0;
          var getuser=this.settings.gettempuser();
          if(getuser){
            var user = JSON.parse(getuser); 
            if(user.is_Supplier == '1'){
              if(this.product['productdetials'].productDiscount>0){
                price=this.productsizemerg[this.selectedsize]["productSizePriceSupplier"]-(this.productsizemerg[this.selectedsize]["productSizePriceSupplier"]*(this.product['productdetials'].productDiscount/100))
              }
              else{ 
                price=this.productsizemerg[this.selectedsize]["productSizePriceSupplier"]
              }
            } else{
              if(this.product['productdetials'].productDiscount>0){
                price=this.productsizemerg[this.selectedsize]["productSizePrice"]-(this.productsizemerg[this.selectedsize]["productSizePrice"]*(this.product['productdetials'].productDiscount/100))
              }
              else{
                price=this.productsizemerg[this.selectedsize]["productSizePrice"]
              }
            }
          } else{
          //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
          if(this.product['productdetials'].productDiscount>0){
            price=this.productsizemerg[this.selectedsize]["productSizePrice"]-(this.productsizemerg[this.selectedsize]["productSizePrice"]*(this.product['productdetials'].productDiscount/100))
          }
          else{
            price=this.productsizemerg[this.selectedsize]["productSizePrice"]
          }
          }
          console.log('price 1',price)
          //--Add Product to cart DB
          this.api.postpromise('AddProductToCart',
          {
            "userId": userl,
            "productId": this.product["productdetials"]["productId"],
            "productsizeId": this.productsizemerg[this.selectedsize]["productSizeId"],
            "productcolorId": this.colors[this.selectedcolor]["productColorId"],
            "quantity": this.selectedquantity,
            "unitPrice": price
         }).then((res)=>{
          this.productTocart={
            'productselecteddetials':this.product["productdetials"],
            'productselectedcolor':[this.colors[this.selectedcolor]],
            'productselectedsize':[this.productsizemerg[this.selectedsize]],
            'productselectedquantity':this.selectedquantity,
            'productselectedpriceLL':this.priceLL,
            'productselectedpricedollars':this.pricedollars
          } 
         })
      }
      else if(this.colors.length>0 && this.productsizemerg.length==0)
      {
        var price=0;
        var getuser=this.settings.gettempuser();
          if(getuser){
            var user = JSON.parse(getuser); 
            if(user.is_Supplier == '1'){
              //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
              if(this.product['productdetials'].productDiscount>0){
                price=this.product["productdetials"]["productPriceSupplier"]-(this.product["productdetials"]["productPriceSupplier"]*(this.product['productdetials'].productDiscount/100))
              }
              else{
                price=this.product["productdetials"]["productPriceSupplier"]
              }
            } else{
              //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
        if(this.product['productdetials'].productDiscount>0){
          price=this.product["productdetials"]["productPrice"]-(this.product["productdetials"]["productPrice"]*(this.product['productdetials'].productDiscount/100))
        }
        else{
          price=this.product["productdetials"]["productPrice"]
        }
            }
          } else{
            //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
            if(this.product['productdetials'].productDiscount>0){
              price=this.product["productdetials"]["productPrice"]-(this.product["productdetials"]["productPrice"]*(this.product['productdetials'].productDiscount/100))
            }
            else{
              price=this.product["productdetials"]["productPrice"]
            }
          }
          console.log('price 2',price)
           //--Add Product to cart DB
           this.api.postpromise('AddProductToCart',
           {
             "userId": userl,
             "productId": this.product["productdetials"]["productId"],
             "productcolorId": this.colors[this.selectedcolor]["productColorId"],
             "quantity": this.selectedquantity,
             "unitPrice": price
          }).then((res)=>{
            this.productTocart={
              'productselecteddetials':this.product["productdetials"],
              'productselectedcolor':[this.colors[this.selectedcolor]],
              'productselectedsize':[],
              'productselectedquantity':this.selectedquantity,
              'productselectedpriceLL':this.priceLL,
              'productselectedpricedollars':this.pricedollars
            }
          })
      }
      else if(this.productsizemerg.length>0 && this.colors.length==0)
      {
        console.log(3)
        var price=0;
        var getuser=this.settings.gettempuser();
          if(getuser){
            var user = JSON.parse(getuser); 
            if(user.is_Supplier == '1'){
               //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
        if(this.product['productdetials'].productDiscount>0){
          price=this.productsizemerg[this.selectedsize]["productSizePriceSupplier"]-(this.productsizemerg[this.selectedsize]["productSizePriceSupplier"]*(this.product['productdetials'].productDiscount/100))
        }
        else{
          price=this.productsizemerg[this.selectedsize]["productSizePriceSupplier"]
        }
            } else{
               //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
        if(this.product['productdetials'].productDiscount>0){
          price=this.productsizemerg[this.selectedsize]["productSizePrice"]-(this.productsizemerg[this.selectedsize]["productSizePrice"]*(this.product['productdetials'].productDiscount/100))
        }
        else{
          price=this.productsizemerg[this.selectedsize]["productSizePrice"]
        }
            }
          } else{
 //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
 if(this.product['productdetials'].productDiscount>0){
  price=this.productsizemerg[this.selectedsize]["productSizePrice"]-(this.productsizemerg[this.selectedsize]["productSizePrice"]*(this.product['productdetials'].productDiscount/100))
}
else{
  price=this.productsizemerg[this.selectedsize]["productSizePrice"]
}
          }
       
        console.log('price 3',price)
          //--Add Product to cart DB
          this.api.postpromise('AddProductToCart',
          {
            "userId": userl,
            "productId": this.product["productdetials"]["productId"],
            "productsizeId": this.productsizemerg[this.selectedsize]["productSizeId"], 
            "quantity": this.selectedquantity,
            "unitPrice": price
         }).then((res)=>{
          this.productTocart={
            'productselecteddetials':this.product["productdetials"],
            'productselectedcolor':[],
            'productselectedsize':[this.productsizemerg[this.selectedsize]],
            'productselectedquantity':this.selectedquantity,
            'productselectedpriceLL':this.priceLL,
            'productselectedpricedollars':this.pricedollars
          }
         })
      }
      else{
        console.log(4,this.product["productdetials"])
        var price=0;
        var getuser=this.settings.gettempuser();
        if(getuser){
          var user = JSON.parse(getuser); 
          if(user.is_Supplier == '1'){
             //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
        if(this.product['productdetials'].productDiscount>0){
          price=this.product["productdetials"]["productPriceSupplier"]-(this.product["productdetials"]["productPriceSupplier"]*(this.product['productdetials'].productDiscount/100))
        }
        else{
          price=this.product["productdetials"]["productPriceSupplier"]
        }
          } else{
             //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
        if(this.product['productdetials'].productDiscount>0){
          price=this.product["productdetials"]["productPrice"]-(this.product["productdetials"]["productPrice"]*(this.product['productdetials'].productDiscount/100))
        }
        else{
          price=this.product["productdetials"]["productPrice"]
        }
          }
        } else{
        //(size.productSizePrice-(size.productSizePrice*(product['productdetials'].productDiscount/100)))
        if(this.product['productdetials'].productDiscount>0){
          price=this.product["productdetials"]["productPrice"]-(this.product["productdetials"]["productPrice"]*(this.product['productdetials'].productDiscount/100))
        }
        else{
          price=this.product["productdetials"]["productPrice"]
        }
        }
        console.log('price 4',price)
         //--Add Product to cart DB
         this.api.postpromise('AddProductToCart',
         {
           "userId": userl,
           "productId": this.product["productdetials"]["productId"], 
           "quantity": this.selectedquantity,
           "unitPrice": price
        }).then((res)=>{
          this.productTocart={
            'productselecteddetials':this.product["productdetials"],
            'productselectedcolor':[],
            'productselectedsize':[],
            'productselectedquantity':this.selectedquantity,
            'productselectedpriceLL':this.priceLL,
            'productselectedpricedollars':this.pricedollars
          } 
        })
      }
    
      this.settings.getcart().then((res)=>{
        console.log(res)
        if(!res || (res && res.length==0)){
          this.settings.setcart([this.productTocart]).then((res)=>{
            console.log('AddToCart',res)
            this.event.publish('CartEditI')
            //--Get Cart
            this.settings.getcart().then((res)=>{
              console.log('GetCart',res)
              if(res && res.length>0)
                this.countcart=res.length;
            })
            let toast = this.toastCtrl.create({
              message:  "Product Added to Cart", 
              position: 'middle',
              duration: 1500
            });
            toast.present();
            toast.onWillDismiss(() => {
              console.log('Dismissed toast');
              this.selectedquantity=1;
              this.selectedsize=0;
              this.selectedcolor=0;
              this.CalculatePrice();
              this.settings.getcart().then((ress)=>{
                this.user.cart.next(ress);
              })
            }); 
          })
        }
        else{
          res.push(this.productTocart);
          this.settings.setcart(res).then((res)=>{
            console.log('AddToCart',res)
            this.event.publish('CartEditI')
            //--Get Cart
            this.settings.getcart().then((res)=>{
              console.log('GetCart',res)
              if(res && res.length>0)
                this.countcart=res.length;
            })
            let toast = this.toastCtrl.create({
              message:  "Product Added to Cart", 
              position: 'middle',
              duration: 1500
            });
            toast.present();
            toast.onWillDismiss(() => {
              console.log('Dismissed toast');
              this.selectedquantity=1;
              this.selectedsize=0;
              this.selectedcolor=0;
              this.CalculatePrice();
              this.settings.getcart().then((ress)=>{
                this.user.cart.next(ress);
              })
            }); 
          })
        }
      })
    
    }
       
  }
  GotoProductDetials(productid){
    this.navCtrl.push('productsdetials', {'productid':productid});
  }
  GoToHome(){
    this.event.publish('selecttab:menu', 0)
  }
  setCurrency(){
    this.homeprovider.currency = this.currencies.find(currency=>currency.id==this.currency_id);
    this.currency = this.homeprovider.currency;
  }
  
  adjustTextColor(bgColor) {  
    if(this.lightOrDark(bgColor) == 'dark'){
        return true;
    }
    return false;
  }
   
  lightOrDark(color) {
    var r, g, b, hsp;
    if (color.match(/^rgb/)) {
        color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/); 
        r = color[1];
        g = color[2];
        b = color[3];
    }else {
        color = +("0x" + color.slice(1).replace( color.length < 5 && /./g, '$&$&' ) ); 
        r = color >> 16;
        g = color >> 8 & 255;
        b = color & 255;
    }
    hsp = Math.sqrt(
        0.299 * (r * r) +
        0.587 * (g * g) +
        0.114 * (b * b)
    ); 
    if (hsp>127.5) {
        return 'light';
    }else { 
        return 'dark';
    }
  }
  GoToShare(product){
    // console.log('GoToShare',product);
    this.socialSharing.share('Please check '+product['productdetials'].productName+' on MBS App','',"https://mouawadmbs.com/uploads/images/"+product['productdetials'].productMainImage,"https://mouawadmbs.com/share/share.php?productId="+product['productdetials'].productId+' or on our website https://mouawadmbs.com/item/'+product['productdetials'].productId)
  }
}
