import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyaccountPage } from './myaccount';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    MyaccountPage,
  ],
  imports: [
    IonicPageModule.forChild(MyaccountPage),
    PipesModule
  ],
})
export class MyaccountPageModule {}
