import { WelcomeModalPage } from './../welcome-modal/welcome-modal';
import { Settings } from './../../providers/settings/settings';
import { User } from './../../providers/user/user';
import { Component, ElementRef ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, Events, AlertController, ModalController } from 'ionic-angular';
import { Api } from '../../providers/api/api';
import { Md5 } from 'ts-md5/dist/md5';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Geolocation } from '@ionic-native/geolocation'; 
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { GooglePlus } from '@ionic-native/google-plus';
import { HomeproviderProvider } from './../../providers/homeprovider/homeprovider';

/**
 * Generated class for the MyaccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@IonicPage()
@Component({
  selector: 'page-myaccount',
  templateUrl: 'myaccount.html',
})
export class MyaccountPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  public title="Sign In"
  public _user: any="";
  public mode:any="signin";
  public password:any="";
  public userName:any="";
  public loading:any="";
  public fullName:any="";  
  public mobile:any="";
  public emailAddress:any="";
  public verfiypassword:any="";
  public regpassword:any="";
  public reguserName:any="";
  public regcountry:any="+961";
  public profilefullname:any="";
  public genderfemale=false;
  public gendermale=false;
  public dob;
  public profileemail:any="";
  public profileCity:any="";
  public profileProvince:any="";
  public profileMobile:any="";
  public profileOccupation:any="";
  public profilehome:any="";
  public profilehomeline:any="";
  public profileStreet:any="";
  public profileBuilding:any="";
  public profileFloor:any="";
  public profileMOF:any="";
  public workhome:any="";
  public addaddressline:any="";
  public workhomeline:any="";
  public addaddress:any="";
  public saveuserid:any=false;
  public isCompany:any=false;
  public aggre:any=false;
  public countcart:any=0;
  public countries = [ {
    name: "United States",
    dial_code: "+1",
    code: "US"
  },  {
      name: "Afghanistan",
      dial_code: "+93",
      code: "AF"
  }, {
      name: "Albania",
      dial_code: "+355",
      code: "AL"
  }, {
      name: "Algeria",
      dial_code: "+213",
      code: "DZ"
  }, {
      name: "AmericanSamoa",
      dial_code: "+1 684",
      code: "AS"
  }, {
      name: "Andorra",
      dial_code: "+376",
      code: "AD"
  }, {
      name: "Angola",
      dial_code: "+244",
      code: "AO"
  }, {
      name: "Anguilla",
      dial_code: "+1 264",
      code: "AI"
  }, {
      name: "Antigua and Barbuda",
      dial_code: "+1268",
      code: "AG"
  }, {
      name: "Argentina",
      dial_code: "+54",
      code: "AR"
  }, {
      name: "Armenia",
      dial_code: "+374",
      code: "AM"
  }, {
      name: "Aruba",
      dial_code: "+297",
      code: "AW"
  }, {
      name: "Australia",
      dial_code: "+61",
      code: "AU"
  }, {
      name: "Austria",
      dial_code: "+43",
      code: "AT"
  }, {
      name: "Azerbaijan",
      dial_code: "+994",
      code: "AZ"
  }, {
      name: "Bahamas",
      dial_code: "+1 242",
      code: "BS"
  }, {
      name: "Bahrain",
      dial_code: "+973",
      code: "BH"
  }, {
      name: "Bangladesh",
      dial_code: "+880",
      code: "BD"
  }, {
      name: "Barbados",
      dial_code: "+1 246",
      code: "BB"
  }, {
      name: "Belarus",
      dial_code: "+375",
      code: "BY"
  }, {
      name: "Belgium",
      dial_code: "+32",
      code: "BE"
  }, {
      name: "Belize",
      dial_code: "+501",
      code: "BZ"
  }, {
      name: "Benin",
      dial_code: "+229",
      code: "BJ"
  }, {
      name: "Bermuda",
      dial_code: "+1 441",
      code: "BM"
  }, {
      name: "Bhutan",
      dial_code: "+975",
      code: "BT"
  }, {
      name: "Bosnia and Herzegovina",
      dial_code: "+387",
      code: "BA"
  }, {
      name: "Botswana",
      dial_code: "+267",
      code: "BW"
  }, {
      name: "Brazil",
      dial_code: "+55",
      code: "BR"
  }, {
      name: "British Indian Ocean Territory",
      dial_code: "+246",
      code: "IO"
  }, {
      name: "Bulgaria",
      dial_code: "+359",
      code: "BG"
  }, {
      name: "Burkina Faso",
      dial_code: "+226",
      code: "BF"
  }, {
      name: "Burundi",
      dial_code: "+257",
      code: "BI"
  }, {
      name: "Cambodia",
      dial_code: "+855",
      code: "KH"
  }, {
      name: "Cameroon",
      dial_code: "+237",
      code: "CM"
  }, {
      name: "Cape Verde",
      dial_code: "+238",
      code: "CV"
  }, {
      name: "Cayman Islands",
      dial_code: "+ 345",
      code: "KY"
  }, {
      name: "Central African Republic",
      dial_code: "+236",
      code: "CF"
  }, {
      name: "Chad",
      dial_code: "+235",
      code: "TD"
  }, {
      name: "Chile",
      dial_code: "+56",
      code: "CL"
  }, {
      name: "China",
      dial_code: "+86",
      code: "CN"
  }, {
      name: "Christmas Island",
      dial_code: "+61",
      code: "CX"
  }, {
      name: "Colombia",
      dial_code: "+57",
      code: "CO"
  }, {
      name: "Comoros",
      dial_code: "+269",
      code: "KM"
  }, {
      name: "Congo",
      dial_code: "+242",
      code: "CG"
  }, {
      name: "Cook Islands",
      dial_code: "+682",
      code: "CK"
  }, {
      name: "Costa Rica",
      dial_code: "+506",
      code: "CR"
  }, {
      name: "Croatia",
      dial_code: "+385",
      code: "HR"
  }, {
      name: "Cuba",
      dial_code: "+53",
      code: "CU"
  }, {
      name: "Cyprus",
      dial_code: "+537",
      code: "CY"
  }, {
      name: "Czech Republic",
      dial_code: "+420",
      code: "CZ"
  }, {
      name: "Denmark",
      dial_code: "+45",
      code: "DK"
  }, {
      name: "Djibouti",
      dial_code: "+253",
      code: "DJ"
  }, {
      name: "Dominica",
      dial_code: "+1 767",
      code: "DM"
  }, {
      name: "Dominican Republic",
      dial_code: "+1 849",
      code: "DO"
  }, {
      name: "Ecuador",
      dial_code: "+593",
      code: "EC"
  }, {
      name: "Egypt",
      dial_code: "+20",
      code: "EG"
  }, {
      name: "El Salvador",
      dial_code: "+503",
      code: "SV"
  }, {
      name: "Equatorial Guinea",
      dial_code: "+240",
      code: "GQ"
  }, {
      name: "Eritrea",
      dial_code: "+291",
      code: "ER"
  }, {
      name: "Estonia",
      dial_code: "+372",
      code: "EE"
  }, {
      name: "Ethiopia",
      dial_code: "+251",
      code: "ET"
  }, {
      name: "Faroe Islands",
      dial_code: "+298",
      code: "FO"
  }, {
      name: "Fiji",
      dial_code: "+679",
      code: "FJ"
  }, {
      name: "Finland",
      dial_code: "+358",
      code: "FI"
  }, {
      name: "France",
      dial_code: "+33",
      code: "FR"
  }, {
      name: "French Guiana",
      dial_code: "+594",
      code: "GF"
  }, {
      name: "French Polynesia",
      dial_code: "+689",
      code: "PF"
  }, {
      name: "Gabon",
      dial_code: "+241",
      code: "GA"
  }, {
      name: "Gambia",
      dial_code: "+220",
      code: "GM"
  }, {
      name: "Georgia",
      dial_code: "+995",
      code: "GE"
  }, {
      name: "Germany",
      dial_code: "+49",
      code: "DE"
  }, {
      name: "Ghana",
      dial_code: "+233",
      code: "GH"
  }, {
      name: "Gibraltar",
      dial_code: "+350",
      code: "GI"
  }, {
      name: "Greece",
      dial_code: "+30",
      code: "GR"
  }, {
      name: "Greenland",
      dial_code: "+299",
      code: "GL"
  }, {
      name: "Grenada",
      dial_code: "+1 473",
      code: "GD"
  }, {
      name: "Guadeloupe",
      dial_code: "+590",
      code: "GP"
  }, {
      name: "Guam",
      dial_code: "+1 671",
      code: "GU"
  }, {
      name: "Guatemala",
      dial_code: "+502",
      code: "GT"
  }, {
      name: "Guinea",
      dial_code: "+224",
      code: "GN"
  }, {
      name: "Guinea-Bissau",
      dial_code: "+245",
      code: "GW"
  }, {
      name: "Guyana",
      dial_code: "+595",
      code: "GY"
  }, {
      name: "Haiti",
      dial_code: "+509",
      code: "HT"
  }, {
      name: "Honduras",
      dial_code: "+504",
      code: "HN"
  }, {
      name: "Hungary",
      dial_code: "+36",
      code: "HU"
  }, {
      name: "Iceland",
      dial_code: "+354",
      code: "IS"
  }, {
      name: "India",
      dial_code: "+91",
      code: "IN"
  }, {
      name: "Indonesia",
      dial_code: "+62",
      code: "ID"
  }, {
      name: "Iraq",
      dial_code: "+964",
      code: "IQ"
  }, {
      name: "Ireland",
      dial_code: "+353",
      code: "IE"
  }, {
      name: "Italy",
      dial_code: "+39",
      code: "IT"
  }, {
      name: "Jamaica",
      dial_code: "+1 876",
      code: "JM"
  }, {
      name: "Japan",
      dial_code: "+81",
      code: "JP"
  }, {
      name: "Jordan",
      dial_code: "+962",
      code: "JO"
  }, {
      name: "Kazakhstan",
      dial_code: "+7 7",
      code: "KZ"
  }, {
      name: "Kenya",
      dial_code: "+254",
      code: "KE"
  }, {
      name: "Kiribati",
      dial_code: "+686",
      code: "KI"
  }, {
      name: "Kuwait",
      dial_code: "+965",
      code: "KW"
  }, {
      name: "Kyrgyzstan",
      dial_code: "+996",
      code: "KG"
  }, {
      name: "Latvia",
      dial_code: "+371",
      code: "LV"
  }, {
      name: "Lebanon",
      dial_code: "+961",
      code: "LB"
  }, {
      name: "Lesotho",
      dial_code: "+266",
      code: "LS"
  }, {
      name: "Liberia",
      dial_code: "+231",
      code: "LR"
  }, {
      name: "Liechtenstein",
      dial_code: "+423",
      code: "LI"
  }, {
      name: "Lithuania",
      dial_code: "+370",
      code: "LT"
  }, {
      name: "Luxembourg",
      dial_code: "+352",
      code: "LU"
  }, {
      name: "Madagascar",
      dial_code: "+261",
      code: "MG"
  }, {
      name: "Malawi",
      dial_code: "+265",
      code: "MW"
  }, {
      name: "Malaysia",
      dial_code: "+60",
      code: "MY"
  }, {
      name: "Maldives",
      dial_code: "+960",
      code: "MV"
  }, {
      name: "Mali",
      dial_code: "+223",
      code: "ML"
  }, {
      name: "Malta",
      dial_code: "+356",
      code: "MT"
  }, {
      name: "Marshall Islands",
      dial_code: "+692",
      code: "MH"
  }, {
      name: "Martinique",
      dial_code: "+596",
      code: "MQ"
  }, {
      name: "Mauritania",
      dial_code: "+222",
      code: "MR"
  }, {
      name: "Mauritius",
      dial_code: "+230",
      code: "MU"
  }, {
      name: "Mayotte",
      dial_code: "+262",
      code: "YT"
  }, {
      name: "Mexico",
      dial_code: "+52",
      code: "MX"
  }, {
      name: "Monaco",
      dial_code: "+377",
      code: "MC"
  }, {
      name: "Mongolia",
      dial_code: "+976",
      code: "MN"
  }, {
      name: "Montenegro",
      dial_code: "+382",
      code: "ME"
  }, {
      name: "Montserrat",
      dial_code: "+1664",
      code: "MS"
  }, {
      name: "Morocco",
      dial_code: "+212",
      code: "MA"
  }, {
      name: "Myanmar",
      dial_code: "+95",
      code: "MM"
  }, {
      name: "Namibia",
      dial_code: "+264",
      code: "NA"
  }, {
      name: "Nauru",
      dial_code: "+674",
      code: "NR"
  }, {
      name: "Nepal",
      dial_code: "+977",
      code: "NP"
  }, {
      name: "Netherlands",
      dial_code: "+31",
      code: "NL"
  }, {
      name: "Netherlands Antilles",
      dial_code: "+599",
      code: "AN"
  }, {
      name: "New Caledonia",
      dial_code: "+687",
      code: "NC"
  }, {
      name: "New Zealand",
      dial_code: "+64",
      code: "NZ"
  }, {
      name: "Nicaragua",
      dial_code: "+505",
      code: "NI"
  }, {
      name: "Niger",
      dial_code: "+227",
      code: "NE"
  }, {
      name: "Nigeria",
      dial_code: "+234",
      code: "NG"
  }, {
      name: "Niue",
      dial_code: "+683",
      code: "NU"
  }, {
      name: "Norfolk Island",
      dial_code: "+672",
      code: "NF"
  }, {
      name: "Northern Mariana Islands",
      dial_code: "+1 670",
      code: "MP"
  }, {
      name: "Norway",
      dial_code: "+47",
      code: "NO"
  }, {
      name: "Oman",
      dial_code: "+968",
      code: "OM"
  }, {
      name: "Pakistan",
      dial_code: "+92",
      code: "PK"
  }, {
      name: "Palau",
      dial_code: "+680",
      code: "PW"
  }, {
      name: "Panama",
      dial_code: "+507",
      code: "PA"
  }, {
      name: "Papua New Guinea",
      dial_code: "+675",
      code: "PG"
  }, {
      name: "Paraguay",
      dial_code: "+595",
      code: "PY"
  }, {
      name: "Peru",
      dial_code: "+51",
      code: "PE"
  }, {
      name: "Philippines",
      dial_code: "+63",
      code: "PH"
  }, {
      name: "Poland",
      dial_code: "+48",
      code: "PL"
  }, {
      name: "Portugal",
      dial_code: "+351",
      code: "PT"
  }, {
      name: "Puerto Rico",
      dial_code: "+1 939",
      code: "PR"
  }, {
      name: "Qatar",
      dial_code: "+974",
      code: "QA"
  }, {
      name: "Romania",
      dial_code: "+40",
      code: "RO"
  }, {
      name: "Rwanda",
      dial_code: "+250",
      code: "RW"
  }, {
      name: "Samoa",
      dial_code: "+685",
      code: "WS"
  }, {
      name: "San Marino",
      dial_code: "+378",
      code: "SM"
  }, {
      name: "Saudi Arabia",
      dial_code: "+966",
      code: "SA"
  }, {
      name: "Senegal",
      dial_code: "+221",
      code: "SN"
  }, {
      name: "Serbia",
      dial_code: "+381",
      code: "RS"
  }, {
      name: "Seychelles",
      dial_code: "+248",
      code: "SC"
  }, {
      name: "Sierra Leone",
      dial_code: "+232",
      code: "SL"
  }, {
      name: "Singapore",
      dial_code: "+65",
      code: "SG"
  }, {
      name: "Slovakia",
      dial_code: "+421",
      code: "SK"
  }, {
      name: "Slovenia",
      dial_code: "+386",
      code: "SI"
  }, {
      name: "Solomon Islands",
      dial_code: "+677",
      code: "SB"
  }, {
      name: "South Africa",
      dial_code: "+27",
      code: "ZA"
  }, {
      name: "South Georgia and the South Sandwich Islands",
      dial_code: "+500",
      code: "GS"
  }, {
      name: "Spain",
      dial_code: "+34",
      code: "ES"
  }, {
      name: "Sri Lanka",
      dial_code: "+94",
      code: "LK"
  }, {
      name: "Sudan",
      dial_code: "+249",
      code: "SD"
  }, {
      name: "Suriname",
      dial_code: "+597",
      code: "SR"
  }, {
      name: "Swaziland",
      dial_code: "+268",
      code: "SZ"
  }, {
      name: "Sweden",
      dial_code: "+46",
      code: "SE"
  }, {
      name: "Switzerland",
      dial_code: "+41",
      code: "CH"
  }, {
      name: "Tajikistan",
      dial_code: "+992",
      code: "TJ"
  }, {
      name: "Thailand",
      dial_code: "+66",
      code: "TH"
  }, {
      name: "Togo",
      dial_code: "+228",
      code: "TG"
  }, {
      name: "Tokelau",
      dial_code: "+690",
      code: "TK"
  }, {
      name: "Tonga",
      dial_code: "+676",
      code: "TO"
  }, {
      name: "Trinidad and Tobago",
      dial_code: "+1 868",
      code: "TT"
  }, {
      name: "Tunisia",
      dial_code: "+216",
      code: "TN"
  }, {
      name: "Turkey",
      dial_code: "+90",
      code: "TR"
  }, {
      name: "Turkmenistan",
      dial_code: "+993",
      code: "TM"
  }, {
      name: "Turks and Caicos Islands",
      dial_code: "+1 649",
      code: "TC"
  }, {
      name: "Tuvalu",
      dial_code: "+688",
      code: "TV"
  }, {
      name: "Uganda",
      dial_code: "+256",
      code: "UG"
  }, {
      name: "Ukraine",
      dial_code: "+380",
      code: "UA"
  }, {
      name: "United Arab Emirates",
      dial_code: "+971",
      code: "AE"
  }, {
      name: "United Kingdom",
      dial_code: "+44",
      code: "GB"
  }, {
      name: "Uruguay",
      dial_code: "+598",
      code: "UY"
  }, {
      name: "Uzbekistan",
      dial_code: "+998",
      code: "UZ"
  }, {
      name: "Vanuatu",
      dial_code: "+678",
      code: "VU"
  }, {
      name: "Wallis and Futuna",
      dial_code: "+681",
      code: "WF"
  }, {
      name: "Yemen",
      dial_code: "+967",
      code: "YE"
  }, {
      name: "Zambia",
      dial_code: "+260",
      code: "ZM"
  }, {
      name: "Zimbabwe",
      dial_code: "+263",
      code: "ZW"
  }, {
      name: "land Islands",
      dial_code: "",
      code: "AX"
  }, {
      name: "Antarctica",
      dial_code: null,
      code: "AQ"
  }, {
      name: "Bolivia, Plurinational State of",
      dial_code: "+591",
      code: "BO"
  }, {
      name: "Brunei Darussalam",
      dial_code: "+673",
      code: "BN"
  }, {
      name: "Cocos (Keeling) Islands",
      dial_code: "+61",
      code: "CC"
  }, {
      name: "Congo, The Democratic Republic of the",
      dial_code: "+243",
      code: "CD"
  }, {
      name: "Cote d'Ivoire",
      dial_code: "+225",
      code: "CI"
  }, {
      name: "Falkland Islands (Malvinas)",
      dial_code: "+500",
      code: "FK"
  }, {
      name: "Guernsey",
      dial_code: "+44",
      code: "GG"
  }, {
      name: "Holy See (Vatican City State)",
      dial_code: "+379",
      code: "VA"
  }, {
      name: "Hong Kong",
      dial_code: "+852",
      code: "HK"
  }, {
      name: "Iran, Islamic Republic of",
      dial_code: "+98",
      code: "IR"
  }, {
      name: "Isle of Man",
      dial_code: "+44",
      code: "IM"
  }, {
      name: "Jersey",
      dial_code: "+44",
      code: "JE"
  }, {
      name: "Korea, Democratic People's Republic of",
      dial_code: "+850",
      code: "KP"
  }, {
      name: "Korea, Republic of",
      dial_code: "+82",
      code: "KR"
  }, {
      name: "Lao People's Democratic Republic",
      dial_code: "+856",
      code: "LA"
  }, {
      name: "Libyan Arab Jamahiriya",
      dial_code: "+218",
      code: "LY"
  }, {
      name: "Macao",
      dial_code: "+853",
      code: "MO"
  }, {
      name: "Macedonia, The Former Yugoslav Republic of",
      dial_code: "+389",
      code: "MK"
  }, {
      name: "Micronesia, Federated States of",
      dial_code: "+691",
      code: "FM"
  }, {
      name: "Moldova, Republic of",
      dial_code: "+373",
      code: "MD"
  }, {
      name: "Mozambique",
      dial_code: "+258",
      code: "MZ"
  }, {
      name: "Palestinian Territory, Occupied",
      dial_code: "+970",
      code: "PS"
  }, {
      name: "Pitcairn",
      dial_code: "+872",
      code: "PN"
  }, {
      name: "Réunion",
      dial_code: "+262",
      code: "RE"
  }, {
      name: "Russia",
      dial_code: "+7",
      code: "RU"
  }, {
      name: "Saint Barthélemy",
      dial_code: "+590",
      code: "BL"
  }, {
      name: "Saint Helena, Ascension and Tristan Da Cunha",
      dial_code: "+290",
      code: "SH"
  }, {
      name: "Saint Kitts and Nevis",
      dial_code: "+1 869",
      code: "KN"
  }, {
      name: "Saint Lucia",
      dial_code: "+1 758",
      code: "LC"
  }, {
      name: "Saint Martin",
      dial_code: "+590",
      code: "MF"
  }, {
      name: "Saint Pierre and Miquelon",
      dial_code: "+508",
      code: "PM"
  }, {
      name: "Saint Vincent and the Grenadines",
      dial_code: "+1 784",
      code: "VC"
  }, {
      name: "Sao Tome and Principe",
      dial_code: "+239",
      code: "ST"
  }, {
      name: "Somalia",
      dial_code: "+252",
      code: "SO"
  }, {
      name: "Svalbard and Jan Mayen",
      dial_code: "+47",
      code: "SJ"
  }, {
      name: "Syrian Arab Republic",
      dial_code: "+963",
      code: "SY"
  }, {
      name: "Taiwan, Province of China",
      dial_code: "+886",
      code: "TW"
  }, {
      name: "Tanzania, United Republic of",
      dial_code: "+255",
      code: "TZ"
  }, {
      name: "Timor-Leste",
      dial_code: "+670",
      code: "TL"
  }, {
      name: "Venezuela, Bolivarian Republic of",
      dial_code: "+58",
      code: "VE"
  }, {
      name: "Viet Nam",
      dial_code: "+84",
      code: "VN"
  }, {
      name: "Virgin Islands, British",
      dial_code: "+1 284",
      code: "VG"
  }, {
      name: "Virgin Islands, U.S.",
      dial_code: "+1 340",
      code: "VI"
  }];
  public profiletype:any="";
  public profileAddMobile:any="";
  public profileCountry:any="";
  public profileaddressDetails:any="";
  public profilepostalCode:any="";
  public addnewaddress:boolean=false;
  public profileaddtype:any="";
  public profileaddMobile:any="";
  public profileaddProvince:any="";
  public profileaddCity:any="";
  public profileaddCountry:any="";
  public profileaddBuilding:any="";
  public profileaddpostalCode:any="";
  public profileaddaddressDetails:any="";
  public countriesall:any=[];
  public profileDefAddMobile:any="";
  public currency:any={};
  public currencies:any=[];
  public currency_id=1;
  constructor(public homeprovider:HomeproviderProvider,public navCtrl: NavController, public navParams: NavParams,public User:User,public user:User,public loadingCtrl:LoadingController,public toastCtrl:ToastController,public setting:Settings,public api:Api,public event:Events,public settings:Settings,private fb: Facebook,public geolocation: Geolocation,public alert:AlertController,public openNativeSettings: OpenNativeSettings,public modalCtrl: ModalController,public alertCtrl:AlertController,public googlePlus:GooglePlus) {
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
// --Get Countries
    this.api.get('getCountries').then((res)=>{
        
      this.countriesall=res["countries"];
    })

  }
public addresses:any=[];
ionViewDidLoad() {
  
  this.user.loggedinuser.subscribe((data)=>{
    // console.log('data login',data); 
    if(data && data.length>0){ 
      var user=data[0]; 
      this._user=user;
      this.SetValues();
      this.mode="profile";
      this.GetAddress(user["userId"]);
    } else{
      this._user='';
      this.mode="signin"
    }
  })
  this.user.cart.subscribe((data)=>{
    if(data && data.length>0){
      this.countcart=data.length;
    } else{
      this.countcart=0;
    }
  })
}
GetAddress(userid){
  this.addresses=[];
  //--Get User Address
  this.api.get('getUserAddress/'+userid).then((res)=>{ 
      this.addresses=res["addresses"];
  })
}
  public lat:any;
  public lng:any;
  loadMap(){ 
    if(!this.map){
        console.log('loadmap')
        let latLng = new google.maps.LatLng(33.8547, 35.8623); 
        let mapOptions = {
        center: latLng,
        zoom: 7,
        mapTypeId: google.maps.MapTypeId.ROADMAP
        } 
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions); 
        google.maps.event.addListener(this.map, 'click', (event) => { 
            this.placeMarker(event.latLng)
        })
        this.addmarker();
    }
    else{
        this.addmarker();
    }
  }
  public gmarkers = [];
  placeMarker(location) {
    console.log('placeMarker',location.lat(),location.lng())
    this.lat=location.lat();
    this.lng=location.lng();
    for(var i=0; i<this.gmarkers.length; i++){
        this.gmarkers[i].setMap(null);
    }
    let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        draggable:true,
        position: location
      });
    
    //   let content = "<h4>Information!</h4>";          
    
    //   this.addInfoWindow(marker, content);
      google.maps.event.addListener(marker,'dragend', (event) => {
        this.lat=event.latLng.lat();
        this.lng=event.latLng.lng();
      });
      this.map.setZoom(17);
      this.map.panTo(marker.position);
      this.gmarkers.push(marker)
  }
  addmarker(){
    console.log('addmarker',this.lat,this.lng,this.mode)
    for(var i=0; i<this.gmarkers.length; i++){
        this.gmarkers[i].setMap(null);
    }
    if(this.lat && this.lng && this.lat != 0 && this.lng != 0){
        let latLng = new google.maps.LatLng(this.lat, this.lng);  
        let marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            draggable:true,
            position: latLng
          });
          google.maps.event.addListener(marker,'dragend', (event) => {
            this.lat=event.latLng.lat();
            this.lng=event.latLng.lng();
          });
          this.map.setZoom(17);
          this.map.panTo(marker.position);
          this.gmarkers.push(marker)
    } 
    else{
        this.geolocation.getCurrentPosition().then((position) => {
            let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); 
            this.lat=position.coords.latitude;
            this.lng= position.coords.longitude;
            let marker = new google.maps.Marker({
                map: this.map,
                animation: google.maps.Animation.DROP,
                draggable:true,
                position: latLng
              });
            
            //   let content = "<h4>Information!</h4>";   
            //   this.addInfoWindow(marker, content);
              google.maps.event.addListener(marker,'dragend', (event) => {
                this.lat=event.latLng.lat();
                this.lng=event.latLng.lng();
              });
              this.map.setZoom(17);
              this.map.panTo(marker.position);
              this.gmarkers.push(marker)
           }).catch((error) => {
            let alert = this.alert.create({ 
                subTitle: 'Please enable ur GPS to detect location',
                buttons: [
                 
                  {
                    text: 'Enable',
                    handler: () => { 
                       this.openNativeSettings.open('location')
                    }
                  },
                   {
                    text: 'Click on Map',
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                    }
                  }
                ]
              });
              alert.present();
             console.log('Error getting location', error);
           })
        
    }
  }
  addInfoWindow(marker, content){

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });

  }
  ionViewDidEnter(){
     
  }
  ionViewWillEnter(){ 
    this.currencies = this.homeprovider.currencies;
    this.currency = this.homeprovider.currency;
    this.currency_id = this.currency.id;
    
  }
  GotoSignUp(){  
    this.mode="signup"; 
  }
  GotoSignIn(){  
    this.mode="signin"; 
  }
  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.trim());
  }
  validateLebanonphone(phone){
    var re = /^(961(3|70|71)|(03|70|71|76|78|79|80|81))\d{6}$/;
    return re.test(phone);
  }
  
  SignUp(){
    //--check required fields
    if(this.regpassword=="" || this.fullName=="" || this.mobile=="" || this.emailAddress=="" || this.verfiypassword==""){
      let toast = this.toastCtrl.create({
        message:  "All fields are required", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
    }
    else if(this.regpassword.length<6 || this.regpassword.length>30 ){
      let toast = this.toastCtrl.create({
        message:  "The password must be more than 6 and less than 30 characters long", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
      
    }
    else if(this.verfiypassword != this.regpassword){
      let toast = this.toastCtrl.create({
        message:  "Passwords dont match", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
    }
    else if(!this.validateEmail(this.emailAddress)) {
      let toast = this.toastCtrl.create({
        message:  "Please enter an valid email address", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
    }
    else if(!this.aggre){
      let toast = this.toastCtrl.create({
        message:  "Please accept our terms and conditions", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
    }
    else{
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
      }); 
      this.loading.present();
      this.user.signup({
        // "userName":this.reguserName,
        "fullName":this.fullName,
        "mobile":this.regcountry +this.mobile,
        "emailAddress":this.emailAddress,
        "password":(Md5.hashStr(this.regpassword) as string)
      }).subscribe((res)=>{
        if(res["success"]){
          if(this.loading!=""){
            this.loading.dismiss();
          } 
          this.userName=this.emailAddress;
          this.password=(Md5.hashStr(this.regpassword) as string);
          let giftCardModal = this.modalCtrl.create(WelcomeModalPage);
          giftCardModal.present(); 
          giftCardModal.onDidDismiss(() => {
            this.Login();
         });
         
        }
        else{
          if(this.loading!=""){
            this.loading.dismiss();
          } 
           let toast = this.toastCtrl.create({
            message:  res["message"], 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
        }
      })
    }
   
  }
  public fbtoken="";
  Login(){
    if(this.userName=="" && this.password==""){
      let toast = this.toastCtrl.create({
        message:  "Email and password required", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
    } 
    else if(!this.validateEmail(this.userName) && this.fbtoken == '') {
      let toast = this.toastCtrl.create({
        message:  "Please enter an valid email address", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
    }
    else{
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
      }); 
      this.loading.present();
      this.user.login({
        "userName":this.userName,   
        "password":(Md5.hashStr(this.password) as string)
      }).subscribe((res)=>{
        if(res["success"]){
          if(this.loading!=""){
            this.loading.dismiss();
          } 
          localStorage.removeItem('tempuser');
          localStorage.setItem('tempuser',JSON.stringify(res["user"][0]))
          if(this.saveuserid){
           localStorage.setItem('userid',JSON.stringify(res["user"][0]))
          } 
          else{
            localStorage.removeItem('userid');
          }
          console.log('es["user"][0]',res["user"][0])
          this.user.loggedinuser.next(res["user"]);
          this.event.publish('selecttab:menu', 0);
        }
        else{
          if(this.loading!=""){
            this.loading.dismiss();
          } 
           let toast = this.toastCtrl.create({
            message:  "Username and password incorrect", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
        }
      })
    }
   
  }
  ionViewDidLeave(){
    console.log('ionViewDidLeave')
    // if(this.loading!=""){
    //   this.loading.dismiss();
    // }

  } 
  genderchangefemale(){ 
        if(this.genderfemale)
            this.gendermale=false; 
  }
  genderchangemale(){
        if(this.gendermale)
            this.genderfemale=false;
  }
  UpdateProfile(){
    console.log('update',this.profilehome,this.dob)
     if(this.profilefullname=="" || this.profileemail=="" || this.profileMobile=="" || this.profileOccupation=="")
     {
      let toast = this.toastCtrl.create({
        message:  "Please enter all required fields", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
     } 
     else if(this.dob=="0000-00-00"){
      let toast = this.toastCtrl.create({
        message:  "Please enter your birth date", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
     }
     else if(!this.validateEmail(this.profileemail)) {
      let toast = this.toastCtrl.create({
        message:  "Please enter an valid email address", 
        position: 'middle',
        showCloseButton:true,
        closeButtonText:'Close'
      });
      toast.present();
    }
     else{
       var gender="";
       if(this.genderfemale)
          gender="female";
        if(this.gendermale)
          gender="male"; 
        if(this.isCompany)
        {
            gender="company";
        }
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        }); 
        this.loading.present();
        this.api.postpromise('updateuser',
          { 
            "userid" :this._user['userId'],
            "fullName":this.profilefullname,
            "gender":gender,
            "dateBirthday":this.dob, 
            "emailAddress":this.profileemail, 
            "mobile":this.profileMobile, 
            "occupation":this.profileOccupation,
            "financeNb":this.profileMOF,
            "userPoints":this._user['userPoints']
          }
        ).then((res)=>{
          console.log('1',res["user"])
          if(this.loading!=""){
            this.loading.dismiss();
          }
          if(res["success"]){ 
            var arrayuser=[];
            arrayuser.push(res["user"])
            this.user.loggedinuser.next(arrayuser);
            localStorage.removeItem('tempuser');
            localStorage.setItem('tempuser',JSON.stringify(res["user"]))
            if(localStorage.getItem('userid')){
              localStorage.setItem('userid',JSON.stringify(res["user"]))
            }
          }
          else{
            let toast = this.toastCtrl.create({
              message:  "Something went wrong,please try again later", 
              position: 'middle',
              showCloseButton:true,
              closeButtonText:'Close'
            });
            toast.present();
          }
         
          console.log('update',res)
        }).catch((er)=>{
          if(this.loading!=""){
            this.loading.dismiss();
          }
          let toast = this.toastCtrl.create({
            message:  "Something went wrong,please try again later", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
        })
     }
  }
  
  SetValues(){
    console.log('set values',this._user)
    if(this._user){
      this.profilefullname=this._user["fullName"];
      if(this._user["gender"]=="female")
        this.genderfemale=true;
      if(this._user["gender"]=="male")
        this.gendermale=true;
      if(this._user["gender"]=="company")
        this.isCompany=true;
      else
        this.isCompany=false;
        
      this.profileMOF=this._user["financeNb"];
      this.dob=this._user["dateBirthday"];
      this.profileemail=this._user["emailAddress"]; 
      this.profileMobile=this._user["mobile"]; 
      this.profileOccupation=this._user["occupation"]; 
    //   this.profilehome=this._user["homeAddress"];
    //   this.profilehomeline=this._user["homeLandline"]; 
    //   this.workhome=this._user["workAddress"]; 
    //   this.workhomeline=this._user["workLandline"]; 
    //   this.addaddress=this._user["additionalAddress"];
    //   this.addaddressline=this._user["additionalLandline"];
    //   this.profileProvince=this._user["province"];
    //   this.profileCity=this._user["city"];
    //   this.profileBuilding=this._user["building"];
    //   this.profileStreet=this._user["street"];
    //   this.profileFloor=this._user["floor"];
    //   if(this._user["lat"] != 0 && this._user["lng"] != 0){
    //       console.log('coordinates found')
    //       this.lat=this._user["lat"];
    //       this.lng=this._user["lng"];
    //       if(this.map)
    //         this.addmarker(); 
          
    //   } 
    //   else{
    //       console.log('get current location')
    //       this.lat=0;
    //       this.lng=0;
    //       if(this.map)
    //         this.addmarker(); 
           
    //   }
      //--Set 
      this.setting.savesessionuserid(this._user["userId"]);
      //--Get cart
      this.GetCartFromDB(this._user["userId"]);
    }
  }
  public productTocart:any={};
  GetCartFromDB(userid){ 
    console.log('GetCartFromDB',userid)
    this.api.get('getUserCart/'+userid).then((res)=>{
      console.log('GetCartFromDB',res);
      //--Update cart
      if(res["success"]){
        //--clear car local storage
        this.settings.clearcart();
        var productarray=[];
        //--Add items to cart local stoarge
        for(var c=0;c<res["cart"].length;c++){
          console.log('cart',res["arraysize"][c][0])
          this.productTocart={
            'carttable':res["cart"][c],
            'productselecteddetials':res["arrayproduct"][c],
            // 'productselectedcolor':[res["cart"]["arraycolor"][c]],
            // 'productselectedsize':[res["cart"]["arraysize"][c]],
            'productselectedquantity':res["cart"][c]["quantity"],
            'productselectedpriceLL':res["cart"][c]["unitPrice"],
            'productselectedpricedollars':res["cart"][c]["unitPrice"]
          } 
          //--size
          if(res["arraysize"][c][0]){
            this.productTocart["productselectedsize"]=[res["arraysize"][c][0]];
          }
          else{
            this.productTocart["productselectedsize"]=[];
          }
          //--color
          if(res["arraycolor"][c][0]){
            this.productTocart["productselectedcolor"]=[res["arraycolor"][c][0]];
          }
          else{
            this.productTocart["productselectedcolor"]=[];
          }
          productarray.push(this.productTocart)
         
        }
        this.settings.setcart(productarray).then((res)=>{
          console.log('AddToCart',res)
          this.event.publish('CartEditI') 
          //--Get Cart
          this.settings.getcart().then((ress)=>{
            if(ress && ress.length>0){
              this.countcart=ress.length;
            }
            else{
              this.countcart=0;
            }
          })
        }) 
      }
    })
    
      

  }
  GotoCart(){ 
    this.event.publish('selecttab:menu', 3);  
  }
  EditAddressCancel(){
      this.edditaddress=false;
  }
  AddNewAddress(){
      this.addnewaddress=true;
      this.edditaddress=false;
      this.RemoveMap();
  }
  DontAddNewAddress(){
    this.addnewaddress=false; 
  }
  Search($event){
    var val="";
      if($event.target) 
        val = $event.target.value; 
    this.navCtrl.push('products', {'src':'search','value':val});
  }
  LoginWithFacebook(){
    // Login with permissions
    this.fb.login(['public_profile', 'user_photos', 'email', 'user_birthday'])
    .then( (res: FacebookLoginResponse) => {

        // The connection was successful
        if(res.status == "connected") {

            // Get user ID and Token
            var fb_id = res.authResponse.userID;
            var fb_token = res.authResponse.accessToken;

            // Get user infos from the API
            this.fb.api("/me?fields=name,gender,birthday,email", []).then((user) => {

                // Get the connected user details
                var gender    = user.gender;
                var birthday  = user.birthday;
                var name      = user.name;
                var email     = user.email;

                console.log("=== USER INFOS ===");
                console.log("Gender : " + gender);
                console.log("Birthday : " + birthday);
                console.log("Name : " + name);
                console.log("Email : " + email);
                if(email == ''){
                  email= fb_token;
                }
                // => Open user session and redirect to the next page
                this.loading = this.loadingCtrl.create({
                  content: 'Please wait...'
                }); 
                this.loading.present();
                this.user.fblogin({
                  // "userName":name,
                  "fullName":name,
                  "mobile":"",
                  "emailAddress":email,
                  "password":(Md5.hashStr(fb_id) as string),
                  "facebookId":fb_id
                }).subscribe((res)=>{
                  console.log('res fb',res)
                  if(this.loading!=""){
                    this.loading.dismiss();
                  }
                  this.userName=email;
                  this.password=(Md5.hashStr(fb_id) as string);
                  this.saveuserid=true;
                  if(res["message"]=='User was Found')
                  {
                    this.Login();
                  } else{
                    let giftCardModal = this.modalCtrl.create(WelcomeModalPage);
                    giftCardModal.present(); 
                    giftCardModal.onDidDismiss(() => {
                      this.Login();
                  });
                  }
                
                  // if(this.saveuserid){
                  //   this.setting.saveuserid(res["user"]);
                  // } 
                  // else{
                  //   this.setting.removeValues('userid')
                  // }
                  
                  // res["user"][0]["userId"]= res["user"][0]["id"]
                  // this.user._user=res["user"][0];
                  // console.log('loggedin',this.user._user)
                  // this._user=res["user"][0];
                  // this.SetValues();
                  // this.mode="profile";
                  //  this.event.publish('userloggedin',this._user) 
                  // console.log('fb',res)
                })
            });

        } 
        // An error occurred while loging-in
        else {

            console.log("An error occurred...");

        }

    })
    .catch((e) => {
        console.log('Error logging into Facebook', e);
    });

  }
  CreateUpdateDefaultAddress(){
      // -- check validation
      if(this.profiletype == '' || this.profileMobile == '' || this.profileDefAddMobile == '' || this.profileProvince =='' || this.profileCity == '' || this.profileCountry == '' || this.profileBuilding =='' || this.profileaddressDetails ==''){
        let toast = this.toastCtrl.create({
            message:  "Please enter all required fields", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
      }//--end if
      else{
          let latselected=0;
          let lngselected=0;
          let maplinkselected='';
          if(this.lat && this.lng && this.lat != 0 && this.lng != 0 && this.addmap)
          {
            latselected=this.lat;
            lngselected=this.lng; 
            maplinkselected='https://www.google.com/maps/place/'+this.lat+','+this.lng;
          }
          this.api.postpromise('AddAddress',
          { 
            "userId" :parseInt(this._user['userId']),
            "addressType":this.profiletype,
            "mobile":this.profileMobile,
            "addressDetails":this.profileaddressDetails,
            "countryId":this.profileCountry,
            "province":this.profileProvince,
            "city":this.profileCity,
            "street":"",
            "building":this.profileBuilding,
            "floor":"",
            "postalCode":this.profilepostalCode,
            "lat":latselected,
            "lng":lngselected,
            "mapLink":maplinkselected,
            "isDefault":1 
          }
        ).then((res)=>{
            console.log('AddAddress',res);
            this.RemoveMap();
            this.api.get('getUserAddress/'+this._user['userId']).then((res)=>{
                console.log('getUserAddress',res["addresses"]) 
                this.addresses=res["addresses"];
            })
        }).catch((er)=>{
            console.error('AddAddress',er)
        })
      } 
  }
  public edditaddress:boolean=false;
  public addresstoedit:any;
  EditAddress(addr){
      console.log('EditAddress',addr);
      this.addresstoedit=addr;
      this.edditaddress=true; 
      this.addnewaddress=false;
      this.RemoveMap();
      this.setAddressToEdits(addr);
  }
  public profileedittype:any="";
  public profileeditMobile:any="";
  public profileeditProvince:any="";
  public profileeditCity:any="";
  public profileeditCountry:any="";
  public profileeditBuilding:any="";
  public profileeditpostalCode:any="";
  public profileeditaddressDetails:any="";
  public profileeditadefault:any="";
  setAddressToEdits(address){
      console.log('setAddressToEdits',address);
      this.profileedittype=address["addressType"];
      this.profileeditMobile=address["mobile"];
      this.profileeditProvince=address["province"];
      this.profileeditCity=address["city"];
      this.profileeditCountry=address["countryId"];
      this.profileeditBuilding=address["building"];
      this.profileeditpostalCode=address["postalCode"];
      this.profileeditaddressDetails=address["addressDetails"];
      if(address["addressDetails"].isDefault === 1){
          this.profileeditadefault=true;
      }else{
        this.profileeditadefault=false;
      }
  }
  UpdateAddress(){
    console.log('UpdateAddress',this.addresstoedit)
    let isDefault=0;
    let deafulttochange:any="";
    if(this.profileeditadefault){  
        isDefault=1; 
        for (var i=0; i < this.addresses.length; i++) { 
            if (this.addresses[i].isDefault == 1 && this.addresses[i].useraddressId !== this.addresstoedit["useraddressId"]) {
                deafulttochange= this.addresses[i];
            }
        }
    }
    let latselected=0;
    let lngselected=0;
    let maplinkselected='';
    if(this.lat && this.lng && this.lat != 0 && this.lng != 0 && this.addmap)
    {
      latselected=this.lat;
      lngselected=this.lng;
      maplinkselected='https://www.google.com/maps/place/'+this.lat+','+this.lng;
    }
    else{
        latselected=this.addresstoedit["lat"];
        lngselected=this.addresstoedit["lng"];
        maplinkselected=this.addresstoedit["mapLink"];
    }
    this.api.postpromise('UpdateAddress',
    { 
      "addressId":this.addresstoedit["useraddressId"],
      "userId" :parseInt(this._user['userId']),
      "addressType":this.profileedittype,
      "mobile":this.profileeditMobile,
      "addressDetails":this.profileeditaddressDetails,
      "countryId":this.profileeditCountry,
      "province":this.profileeditProvince,
      "city":this.profileeditCity,
      "street":"",
      "building":this.profileeditBuilding,
      "floor":"",
      "postalCode":this.profileeditpostalCode,
      "lat":latselected,
      "lng":lngselected,
      "mapLink":maplinkselected,
      "isDefault":isDefault
    }
  ).then((res)=>{
      console.log('UpdateAddress',deafulttochange);
      this.edditaddress=false;
      this.addresses=[];
      this.RemoveMap();  
    if(deafulttochange!=""){
        console.log('1')
        this.api.postpromise('UpdateAddress',
        { 
          "addressId":deafulttochange["useraddressId"],
          "userId" :parseInt(this._user['userId']),
          "addressType":deafulttochange["addressType"],
          "mobile":deafulttochange["mobile"],
          "addressDetails":deafulttochange["addressDetails"],
          "countryId":deafulttochange["countryId"],
          "province":deafulttochange["province"],
          "city":deafulttochange["city"],
          "street":"",
          "building":deafulttochange["building"],
          "floor":"",
          "postalCode":deafulttochange["postalCode"],
          "lat":deafulttochange['lat'],
          "lng":deafulttochange['lng'],
          "mapLink":deafulttochange['mapLink'],
          "isDefault":0
        }).then(()=>{
              //--Get User Address
            this.api.get('getUserAddress/'+this._user['userId']).then((res)=>{
                console.log('getUserAddress',res["addresses"]) 
                this.addresses=res["addresses"];
            })
        })
    }
    else{
        this.api.get('getUserAddress/'+this._user['userId']).then((res)=>{
            console.log('getUserAddress',res["addresses"]) 
            this.addresses=res["addresses"];
        })
    }
     
  }).catch((er)=>{
      console.error('UpdateAddress',er)
  })
  }
  public profileaddadefault:boolean=false;
  SaveNewAddress(){
       // -- check validation
       if(this.profileaddtype == '' || this.profileaddMobile == '' ||   this.profileaddProvince =='' || this.profileaddCity == '' || this.profileaddCountry == '' || this.profileaddBuilding =='' || this.profileaddaddressDetails ==''){
        let toast = this.toastCtrl.create({
            message:  "Please enter all required fields", 
            position: 'middle',
            showCloseButton:true,
            closeButtonText:'Close'
          });
          toast.present();
      }//--end if
      else{
        let isDefault=0;
        let deafulttochange:any="";
        if(this.profileaddadefault){  
            isDefault=1; 
            for (var i=0; i < this.addresses.length; i++) {
                console.log('add')
                if (this.addresses[i].isDefault == 1) {
                    deafulttochange= this.addresses[i];
                }
            }
        }
        let latselected=0;
        let lngselected=0;
        let maplinkselected='';
        if(this.lat && this.lng && this.lat != 0 && this.lng != 0 && this.addmap)
        {
          latselected=this.lat;
          lngselected=this.lng;
          maplinkselected='https://www.google.com/maps/place/'+this.lat+','+this.lng;
        }
          this.api.postpromise('AddAddress',
          { 
            "userId" :parseInt(this._user['userId']),
            "addressType":this.profileaddtype,
            "mobile":this.profileaddMobile,
            "addressDetails":this.profileaddaddressDetails,
            "countryId":this.profileaddCountry,
            "province":this.profileaddProvince,
            "city":this.profileaddCity,
            "street":"",
            "building":this.profileaddBuilding,
            "floor":"",
            "postalCode":this.profileaddpostalCode,
            "lat":latselected,
            "lng":lngselected,
            "mapLink":maplinkselected,
            "isDefault":isDefault
          }
        ).then((res)=>{
            this.addnewaddress=false;
      this.addresses=[];
        this.RemoveMap();
    if(deafulttochange!=""){
        console.log('1')
        this.api.postpromise('UpdateAddress',
        { 
          "addressId":deafulttochange["useraddressId"],
          "userId" :parseInt(this._user['userId']),
          "addressType":deafulttochange["addressType"],
          "mobile":deafulttochange["mobile"],
          "addressDetails":deafulttochange["addressDetails"],
          "countryId":deafulttochange["countryId"],
          "province":deafulttochange["province"],
          "city":deafulttochange["city"],
          "street":"",
          "building":deafulttochange["building"],
          "floor":"",
          "postalCode":deafulttochange["postalCode"],
          "lat":deafulttochange["lat"],
          "lng":deafulttochange["lng"],
          "mapLink":deafulttochange["mapLink"],
          "isDefault":0
        }).then(()=>{
              //--Get User Address
            this.api.get('getUserAddress/'+this._user['userId']).then((res)=>{
                console.log('getUserAddress',res["addresses"]) 
                this.addresses=res["addresses"];
            })
        })
        }
        else{
            this.api.get('getUserAddress/'+this._user['userId']).then((res)=>{
                console.log('getUserAddress',res["addresses"]) 
                this.addresses=res["addresses"];
            })
        }
     
        }).catch((er)=>{
            console.error('AddAddress',er)
        })
      } 
  }
  DeleteAddress(addr){
      console.log('DeleteAddress',addr);
      let alert = this.alertCtrl.create({ 
        subTitle: 'Remove address?',
        buttons: [
         
          {
            text: 'Remove',
            handler: () => { 
                this.api.postpromise("removeAddress",{
                    "useraddressId":addr["useraddressId"] 
                }).then(()=>{
                    this.addresses=[];
                    //--Get User Address
                    this.api.get('getUserAddress/'+this._user['userId']).then((res)=>{
                        console.log('getUserAddress',res["addresses"]) 
                        this.addresses=res["addresses"];
                        this.edditaddress=false;
                    })
                }) 

            }
          },
           {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      alert.present();
  }
  public addmap:boolean=false;
  public AddMap(){
    this.addmap=true;
    this.loadMap();
  }
  RemoveMap(){ 
    this.addmap=false;
    this.lat=0;
    this.lng=0;
  }
  EditMap(){
    this.addmap=true;
    console.log('addres to edit',this.addresstoedit);
    this.lat=this.addresstoedit['lat'];
    this.lng=this.addresstoedit['lng'];
    this.loadMap();
  }

  googlePlusLogin(){
    this.googlePlus.login({
    }).then(res => {
      console.log('googlePlusLogin',res);
      if(res["email"]){
        var fb_id=res["userId"];
        var email =res["email"];
        this.user.GoogleLogin({
          // "userName":name,
          "fullName":res["displayName"],
          "mobile":"",
          "emailAddress":res["email"],
          "password":(Md5.hashStr(res["userId"]) as string),
          "facebookId":res["userId"]
        }).subscribe((res)=>{
          console.log('res fb',res)
          if(this.loading!=""){
            this.loading.dismiss();
          }
          this.userName=email;
          this.password=(Md5.hashStr(fb_id) as string);
          this.saveuserid=true;
          if(res["message"]=='User was Found')
          {
            this.Login();
          } else{
            let giftCardModal = this.modalCtrl.create(WelcomeModalPage);
            giftCardModal.present(); 
            giftCardModal.onDidDismiss(() => {
              this.Login();
          });
          } 
        })
      }
      
    })
  }
  setCurrency(){
    this.homeprovider.currency = this.currencies.find(currency=>currency.id==this.currency_id);
    this.currency = this.homeprovider.currency;
  }
  
}
