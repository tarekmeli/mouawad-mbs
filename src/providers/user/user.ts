import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';
import { Events } from 'ionic-angular';
import { BehaviorSubject } from 'rxjs';
 
@Injectable()
export class User { 
  public _user:any;
  public loggedinuser: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  public cart: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  public loggedout=false;
  constructor(public api: Api,public event:Events) { 
    this.event.unsubscribe('loggout');
    this.event.subscribe('loggout',()=>{
      console.log('loggout')
      this.loggedout=true;
    })
  }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) {
    let seq = this.api.post('login', accountInfo).share();

    seq.subscribe((res: any) => {
      console.log('login',res)
      // If the API returned a successful response, mark the user as logged in
      if (res["success"] == 'success') {
        // this._loggedIn(res);
        this.loggedinuser.next(res.user);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) {
    let seq = this.api.post('signup', accountInfo).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res["success"] == 'success') {
        this._loggedIn(res);
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  fblogin(accountInfo: any){
    let seq = this.api.post('FbLogin', accountInfo).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res["success"] == 'success') {
        this._loggedIn(res);
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  GoogleLogin(accountInfo: any){
    let seq = this.api.post('GoogleLogin', accountInfo).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res["success"] == 'success') {
        this._loggedIn(res);
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this.loggedinuser.next(null);
    this.cart.next(null);
    localStorage.removeItem('sessionuserid');
    
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(resp) {
    // this._user = resp.user;
    this.loggedinuser.next(resp.user);
    localStorage.removeItem('tempuser');
  }

  
}
