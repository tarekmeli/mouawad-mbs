import { Api } from './../api/api';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the HomeproviderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HomeproviderProvider {
  public categories:any=[];
  public banners:any=[];
  public bestsellers:any=[];
  public brands:any=[];
  public currencies:any=[];
  public currency:any={};
  public deliveryCharge:any=2;
  public currentPage='other';
  public loyaltyProgramPoints:any={};
  
  constructor(public http: HttpClient,public api:Api) {
    console.log('Hello HomeproviderProvider Provider');
  }

  GetHomeComponents(){
    try{
       return this.api.get('IndexController') 
    }catch(er){console.error('GetHomeComponents',er)}
  }
  _SetHomeComponents(res: any) { 
     //--Set values
     this.categories=res["Home"]["categories"].sort(function(a, b){return a.categoryOrder - b.categoryOrder})
     this.banners=res["Home"]["banners"].sort(function(a, b){return a.categoryOrder - b.categoryOrder})
     this.bestsellers=res["Home"]["bestsellers"].sort(function(a, b){return a.productOrder - b.productOrder})
     this.brands=res["Home"]["brands"].sort(function(a, b){return a.brandOrder - b.brandOrder})   
  }

}
