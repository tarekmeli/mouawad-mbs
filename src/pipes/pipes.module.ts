import { NgModule } from '@angular/core';
import { TruncatePipe } from './truncate/truncate';
import { EllipsisPipe } from './ellipsis/ellipsis';
@NgModule({
	declarations: [TruncatePipe,
    EllipsisPipe],
	imports: [],
	exports: [TruncatePipe,
    EllipsisPipe]
})
export class PipesModule {}
