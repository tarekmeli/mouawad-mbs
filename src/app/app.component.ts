import { User } from './../providers/user/user';
import { Api } from './../providers/api/api';
import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform, Events, MenuController, AlertController, App, NavController } from 'ionic-angular'; 
import { OneSignal } from '@ionic-native/onesignal';
import { FirstRunPage } from '../pages';
import { Settings } from '../providers';
import { HomeproviderProvider } from './../providers/homeprovider/homeprovider'; 
import { Deeplinks } from '@ionic-native/deeplinks';
import { ProductdetialsPage } from '../pages/productdetials/productdetials';

@Component({
  templateUrl: 'app.template.html'
})
export class MyApp {
  rootPage = FirstRunPage;
  loggeduser: any='';
  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Tutorial', component: 'TutorialPage' },
    { title: 'Welcome', component: 'WelcomePage' },
    { title: 'Tabs', component: 'TabsPage' },
    { title: 'Cards', component: 'CardsPage' },
    { title: 'Content', component: 'ContentPage' },
    { title: 'Login', component: 'LoginPage' },
    { title: 'Signup', component: 'SignupPage' },
    { title: 'Master Detail', component: 'ListMasterPage' },
    { title: 'Menu', component: 'MenuPage' },
    { title: 'Settings', component: 'SettingsPage' },
    { title: 'Search', component: 'SearchPage' }
  ]
  public categories:any[]=[]; 
  public closeCam= false;
  constructor(public homeprovider:HomeproviderProvider,private translate: TranslateService, public platform: Platform, public settings: Settings, private config: Config, private statusBar: StatusBar, private splashScreen: SplashScreen,public api:Api,public event:Events, public menu: MenuController,public oneSignal: OneSignal,public user:User,public alertCtrl:AlertController,public app: App,public deeplinks: Deeplinks) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.deeplinks.route({ 
        '/products/:productId': ProductdetialsPage,
        '/share/share.php': ProductdetialsPage
      }).subscribe((match) => {
        // match.$route - the route we matched, which is the matched entry from the arguments to route()
        // match.$args - the args passed in the link
        // match.$link - the full link data
        this.event.publish('deeplink',match.$args);
        console.log('Successfully matched route', match.$args);
      },
      (nomatch) => {
        // nomatch.$link - the full link data
        console.error('Got a deeplink that didn\'t match', nomatch);
      });
      //Get Categories
      this.api.get('MenuCategories').then((res)=>{ 
        if(res && res["categories"])
          this.categories=res["categories"];
      })
      
      this.api.get('Currencies').then((res)=>{ 
        if(res && res["currencies"])
          this.homeprovider.currencies=res["currencies"];
        if(!this.homeprovider.currency || !this.homeprovider.currency.id){
            this.homeprovider.currency = this.homeprovider.currencies[1];
        }
        this.homeprovider.deliveryCharge = res["deliveryCharge"];
      })
      
      this.api.get('LoyaltyProgram').then((res)=>{  
        if(res["points"] && res["amount"]){
            this.homeprovider.loyaltyProgramPoints = {
                points: parseInt(res["points"]),
                amount: parseInt(res["amount"])
            };
        }
      })
       
      this.user.loggedinuser.subscribe((data)=>{ 
        console.log('loggedinuser app',data)
        if(data && data.length>0){
          this.loggeduser=data[0]; 
          console.log('loggedinuser',this.loggeduser)
          this.menu.enable(true, 'loggedInMenu');
        } else { 
          this.loggeduser='';
          this.menu.close();
          this.menu.enable(true, 'loggedOutMenu');
          this.settings.clearcart();
          this.event.publish('selecttab:menu', 0); 
        }
      }) 
      this.initTranslate();
    });
  
     
  } 
  
initTranslate() {  
    this.platform.registerBackButtonAction(() => { 
      if( this.closeCam){
        this.app.navPop();
        this.closeCam = false;
        if(this.menu.isOpen()){
          this.menu.close();
        }
      } else{
        let nav = this.app.getActiveNavs()[0];
        let activeView = nav.getActive();
        console.log('registerBackButtonAction',activeView); 
        
          if(activeView.id!="HomePage"){ 
            if(activeView.id=="AboutusPage" || activeView.id=="MyaccountPage" || activeView.id=="MycartPage" )
            {
              if(this.menu.isOpen()){
                this.menu.close();
              } else{ 
                this.event.publish('selecttab:menu', 0);
              }
            } else{
              if(this.menu.isOpen()){
                this.menu.close();
              } else{ 
                this.app.navPop();
              }
            }
           
          }
          else if(this.homeprovider.currentPage!='home' ){
            this.app.navPop();
          }else{
            if(this.menu.isOpen()){
              this.menu.close();
            } 
            else{
              let alert = this.alertCtrl.create({ 
                subTitle: 'Exit App?',
                buttons: [{
                  text: 'Exit',
                  handler: () => { 
                     this.platform.exitApp(); 
                  }
                },{
                  text: 'Cancel',
                  handler: () => { 
                    //this.event.publish('selecttab:menu', 2); 
                  }
                }]
              });
              alert.present();
            }
            
          } 
      }
    
    
    }); 
    if(this.platform.is('android')){
      //Initate OneSignal
      this.oneSignal.startInit('0606a862-f757-4926-9f9f-6ba6b43049bf', '1090654050795'); 
    }
    if(this.platform.is('ios')){
      //Initate OneSignal
      this.oneSignal.startInit('0606a862-f757-4926-9f9f-6ba6b43049bf',''); 
    }
      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert); 
      this.oneSignal.handleNotificationReceived().subscribe(() => {
      // do something when notification is received
      }); 
      this.oneSignal.handleNotificationOpened().subscribe(() => {
        // do something when a notification is opened
      }); 
      this.oneSignal.endInit();
      this.oneSignal.getIds().then((res)=>
      {console.log('onesignal',res)})
    // }
    this.user.loggedinuser.subscribe((data)=>{ 
      console.log('loggedinuser app',data)
      if(data && data.length>0){
        this.loggeduser=data[0]; 
        console.log('loggedinuser',this.loggeduser)
        this.menu.enable(true, 'loggedInMenu');
      } else { 
        this.menu.close();
        this.menu.enable(true, 'loggedOutMenu');
        this.settings.clearcart();
        this.event.publish('selecttab:menu', 0); 
      }
    })  
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === 'zh') {
        const browserCultureLang = this.translate.getBrowserCultureLang();

        if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
          this.translate.use('zh-cmn-Hans');
        } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
          this.translate.use('zh-cmn-Hant');
        }
      } else {
        this.translate.use(this.translate.getBrowserLang());
      }
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
    
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario 
    if(page == 'PriceCheckingPage'){
      this.closeCam = true;
    }
    this.nav.push(page);  
    
  }
  GoToSubCategories(category){  
    this.event.publish('categoryname:menu', {'category':category});
    // if(this.loggeduser !='')
    //   this.event.publish('categoryname:menu', {'category':category});
    // else
    //   {
    //     let alert = this.alertCtrl.create({ 
    //       subTitle: 'Please login first',
    //       buttons: [{
    //         text: 'Go to Login',
    //         handler: () => { 
    //           this.event.publish('selecttab:menu', 2); 
    //         }
    //       }]
    //     });
    //     alert.present();
    //   }
  }
  Gototab(index){
    this.event.publish('selecttab:menu', index); 
  }
  Loggout(){
    // this.menu.enable(true,'loggedOutMenu');
    // this.event.publish('loggout');
    // this.settings.removeValues('userid') 
    // this.settings.removeValues('sessionuserid');
    this.user.logout(); 
    
  }
   
}
